package bean;

import static org.junit.Assert.*;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.joda.time.DateTime;
import org.joda.time.base.BaseDateTime;
import org.joda.time.field.AbstractPartialFieldProperty;

import ejb.DateEJB;
import ejb.DateEvaluationEJB;
import ejb.DatelistEJB;
import ejb.MailEJB;
import ejb.MeetingEJB;
import ejb.MessageEJB;
import ejb.SettingsEJB;
import ejb.UrlEJB;
import ejb.UserEJB;
import ejb.WaitlistEJB;
import entity.Meeting;
import entity.TDate;
import entity.User;
import model.EditBeanDateWrapper;
import model.UserWrapper;
/**
 * 
 * This class tests EventBean and parts of SchedulingBean
 * @author JS
 *
 */


@Transactional(TransactionMode.ROLLBACK)
@RunWith(Arquillian.class)
public class EditEventBeanTest {

	// @Inject
	// EditEventBean testBean;
	@Inject
	private DateEJB dateEJB;
	@Inject
	MeetingEJB testmEJB;
	@Inject
	UserEJB testuEJB;

	Meeting testMeet = null;
	List<TDate> testList = null;
	User theUser = null;
	TDate testDate = null;

	@PersistenceContext
	private EntityManager em;

	@Deployment
	public static WebArchive createDeployment() {

		File[] libs = Maven.resolver().loadPomFromFile("pom.xml")
				.resolve("mysql:mysql-connector-java").withTransitivity()
				.asFile();

		return ShrinkWrap
				.create(WebArchive.class, "test.war")
				.addPackage(TDate.class.getPackage())
				.addPackage(AbstractPartialFieldProperty.class.getPackage())
				.addPackage(DateTime.class.getPackage())
				.addPackage(BaseDateTime.class.getPackage())
				.addClasses(DateEJB.class, DateEvaluationEJB.class,
						MailEJB.class, MeetingEJB.class, UserEJB.class,
						DatelistEJB.class, MessageEJB.class, UrlEJB.class,
						EditBeanDateWrapper.class, UserWrapper.class,
						SettingsEJB.class, EditEventBean.class,
						WaitlistEJB.class)
				.addAsResource("test-persistence.xml",
						"META-INF/persistence.xml").addAsLibraries(libs)
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Before
	public void init() throws Exception {
		// User part
		theUser = new User();
		theUser.setUserName("test1");
		theUser.setEmail("test@test.de");
		theUser.setPassword("test");
		theUser.setRole("user");
		theUser.setRegistered("yes");
		testuEJB.saveUser(theUser);
		// Meeting part
		testMeet = new Meeting();
		testMeet.setCreator(theUser.getUserId());
		testMeet.setDescription("testMessage");
		testMeet.setMaxParticipants(5);
		testMeet.setMinParticipants(2);
		testMeet.setAdminLink("www.flexplan.de/test1");
		testMeet.setUserLink("www.flexplan.de/usertest1");
		testMeet.setTitle("Testmeeting");
		testMeet.setLocation("Testloc");
		testMeet.setInvitemessage("Testers you're invited");
		testmEJB.saveMeeting(testMeet);
		// Date part
		testDate = new TDate();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, 9);
		cal.set(Calendar.DATE, 9);
		cal.set(Calendar.YEAR, 2015);
		cal.set(Calendar.HOUR, 15);
		cal.set(Calendar.MINUTE, 30);
		cal.set(Calendar.SECOND, 0);
		testDate.setDateTime(cal.getTime());
		testDate.setMeeting(testMeet);
		testDate.setStatus("invalid");
		em.persist(testDate);

	}

	@Test
	public void testCreateTimeString() {
		String timeString;
		Calendar c = Calendar.getInstance();
		String localeString = "en";
		SimpleDateFormat dateFormat;
		if (localeString.contains("de")) {
			dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		} else {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		}
		timeString = dateFormat.format(c.getTime());
		System.out.println(timeString);
		assertNotNull(timeString);
	}

	@Test
	public void testCheckTDateStatus() {
		assertTrue(testDate.getStatus().equals("invalid"));
	}

	@Test
	public void testConvertTDateToDateTime() {
		assertNotNull(testDate.getDateTime());

	}

	@Test
	public void testCreateConfirmedDateString() {
		String result = "";
		String localeString = "de";
		SimpleDateFormat dateFormat;
		if (localeString.contains("de")) {
			dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		} else {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		}
	
		result = dateFormat.format(testDate.getDateTime());
		assertEquals(result, "09.10.2015 15:30");
	//	DateTime t = new DateTime(testDate);
	//	t = t.minusMinutes(5);
	//	result = dateFormat.format(t.toDate());
		assertTrue(localeString.contains("de"));
	}
	
	@Test 
	public void testCreateDateString() {
		String res = "";
		SimpleDateFormat dateFormat;
		dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");

		res = dateFormat.format(testDate.getDateTime());
		assertEquals(res, "09.10.2015 15:30");
	}
}

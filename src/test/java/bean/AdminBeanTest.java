package bean;
import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.UserWrapper;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.joda.time.DateTime;
import org.primefaces.model.UploadedFile;
import org.joda.time.ReadableInstant;



import org.joda.time.base.BaseDateTime;
import org.joda.time.field.AbstractPartialFieldProperty;

import util.UserNameComparator;
import ejb.*;
import entity.Grouping;
import entity.GroupingId;
import entity.Groups;
import entity.TDate;
import entity.User;
import bean.AdminBean;

/**
 * 
 * This class tests the AdminBean
 * @author JS
 *
 */

@Transactional(TransactionMode.ROLLBACK)
@RunWith(Arquillian.class)
public class AdminBeanTest {
	

	@PersistenceContext
	private EntityManager em;
	
	@Inject
 	AdminBean testBean;
	@Inject 
	GroupsEJB testgEJB;
	@Inject
	UserEJB testuEJB;
	@Inject
	private GroupingEJB testgrEJB;
	User theUser =null;
	Groups testGroup = null;
	Grouping testGrouping = null;
	@Deployment
    public static WebArchive createDeployment() {

        File[] libs = Maven.resolver().loadPomFromFile("pom.xml").resolve("mysql:mysql-connector-java").withTransitivity().asFile();

        return ShrinkWrap.create(WebArchive.class,"test.war")
        		.addPackage(TDate.class.getPackage())
        		.addPackage(AbstractPartialFieldProperty.class.getPackage())
        		.addPackage(DateTime.class.getPackage())
        		.addPackage(BaseDateTime.class.getPackage())
        		.addPackage(DateEJB.class.getPackage())
        		.addClasses(AdminBean.class, UserWrapper.class, UploadedFile.class,
        				UserNameComparator.class)
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsLibraries(libs)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }
	@Before
	public void testInit() throws Exception{
		theUser = new User();
		theUser.setUserName("test1");
		theUser.setEmail("test@test.de");
		theUser.setPassword("test");
		theUser.setRole("user");
		theUser.setRegistered("yes");
		testuEJB.saveUser(theUser);
		testGroup = new Groups();
		testGroup.setGroupsAdmin(theUser.getUserId());
		testGroup.setGroupsDescription("The TestGroup");
		testGroup.setGroupsName("TestGroup");
		testgEJB.saveGroup(testGroup);
		testGrouping = new Grouping();
		testGrouping.setId(new GroupingId(testGroup.getGroupsId(), theUser.getUserId()));
		testGrouping.setGroups(testGroup);
		testGrouping.setUser(theUser);
		
		testgrEJB.saveGrouping(testGrouping);
		
	}

	@Test
	public void testGetUserList() {
		List<User> u = testuEJB.getUsers(true);
		int testInt = u.size();
		User testU = null;
		List<User> admins = new ArrayList<User>();
		for (int i = 0; i < u.size(); i++) {
			if ("admin".equals(u.get(i).getRole())) {
				admins.add(u.get(i));
				testU = u.get(i);
			}
		}
		for (int i = 0; i < admins.size(); i++) {
			u.remove(admins.get(i));
		}
		Collections.sort(u, new UserNameComparator());
		assertNotEquals(testInt, u.size());
		assertFalse(u.contains(testU));
		System.out.println("Test getUserList works");
	}
	
	@Test
	public void testGroupUserList() {
		List<User> users = new ArrayList<User>();
		System.out.println(testGroup.getGroupsName());
		System.out.println(testGroup.getGroupings());
		for (Grouping testGrouping : testGroup.getGroupings()) {
			users.add(testGrouping.getUser());
		}
		Collections.sort(users, new UserNameComparator());
		assertFalse(users.contains(theUser));
	}	

	@Test
	public void testRemoveUserFromGroup() {
		List<Grouping> grouping = testgrEJB.getAll();
		for (int i = 0; i < grouping.size(); i++) {
			if (grouping.get(i).getUser().equals(theUser)
					&& grouping.get(i).getGroups().equals(testGroup)) {
				testgrEJB.remove(grouping.get(i));
				break;
			}	
		}
		boolean t = false;
		for (int i = 0; i < grouping.size(); i++) {
			if (grouping.get(i).getUser().equals(theUser)
					&& grouping.get(i).getGroups().equals(testGroup)) {
				t=true;
			}
		}
		// t is true if the user was removed
		assertTrue(t);
	}
}

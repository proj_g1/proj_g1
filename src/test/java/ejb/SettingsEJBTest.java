package ejb;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import entity.Settings;


/**
 * This class tests SettingsEJB
 * @author JS
 *
 */


@Transactional(TransactionMode.ROLLBACK)
@RunWith(Arquillian.class)
public class SettingsEJBTest {
	@PersistenceContext
	private EntityManager em;
	
	Settings testSet = null;
	List<Settings> testList = null;
	@Inject
	SettingsEJB testEJB;
	
	@Before
	public void init() throws Exception{
		testSet = new Settings();
		testSet.setSettingsName("TestSetting");
		testSet.setSettingsDetails("These are the testdetails");
		testEJB.savesettings(testSet);
	}
	
	@Deployment
    public static WebArchive createDeployment() {

        File[] libs = Maven.resolver().loadPomFromFile("pom.xml").resolve("mysql:mysql-connector-java").withTransitivity().asFile();

        return ShrinkWrap.create(WebArchive.class,"test.war")
        		.addPackage(Settings.class.getPackage())
        		.addClasses(SettingsEJB.class)
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsLibraries(libs)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }
	
	@Test
	public void testSavesettings() {
		assertTrue(testEJB.getAll().contains(testSet));
	}

	@Test
	public void testUpdatesettings() {
		testSet.setSettingsDetails("much wow!");
		testEJB.updatesettings(testSet);
		assertTrue(testEJB.getAll().contains(testSet));
	}

	@Test
	public void testRemove() {
		testEJB.remove(testSet);
		assertFalse(testEJB.getAll().contains(testSet));
	}

	@Test
	public void testFindByName() {
		assertEquals(testEJB.findByName("TestSetting"), testSet);
	}


}

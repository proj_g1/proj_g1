package ejb;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;
import java.util.Calendar;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.UserWrapper;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import entity.Datelist;
import entity.DatelistId;
import entity.Meeting;
import entity.TDate;
import entity.User;

/**
 * This class tests DatelistEJB
 * @author JS
 *
 */

@Transactional(TransactionMode.ROLLBACK)
@RunWith(Arquillian.class)
public class DatelistEJBTest {
	
	@PersistenceContext
	private EntityManager em;
	
	Meeting testMeet = null;
	List<Datelist> testList = null;
	User theUser = null;
	TDate testDate = null;
	Datelist testdl = null;
	@Inject
	MeetingEJB testmEJB;
	@Inject
	UserEJB testuEJB;
	@Inject
	DatelistEJB testdlEJB;
	
	@Before
	public void init() throws Exception{
		//User part
		theUser = new User();
		theUser.setUserName("test1");
		theUser.setEmail("test@test.de");
		theUser.setPassword("test");
		theUser.setRole("user");
		theUser.setRegistered("yes");
		testuEJB.saveUser(theUser);
		//Meeting part
		testMeet = new Meeting();
		testMeet.setCreator(theUser.getUserId());
		testMeet.setDescription("testMessage");
		testMeet.setMaxParticipants(5);
		testMeet.setMinParticipants(2);
		testMeet.setAdminLink("www.flexplan.de/test1");
		testMeet.setUserLink("www.flexplan.de/usertest1");
		testMeet.setTitle("Testmeeting");
		testMeet.setLocation("Testloc");
		testMeet.setInvitemessage("Testers you're invited");
		testmEJB.saveMeeting(testMeet);	
		//Date part
		testDate = new TDate();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.MONTH, 9);
		cal.set(Calendar.DATE, 9);
		cal.set(Calendar.YEAR, 2015);
		cal.set(Calendar.HOUR, 15);
		cal.set(Calendar.MINUTE, 30);
		cal.set(Calendar.SECOND, 0);
		testDate.setDateTime(cal.getTime());
		testDate.setMeeting(testMeet);
		testDate.setStatus("invalid");
		em.persist(testDate);
		//Datelist part
		testdl = new Datelist();
		DatelistId dlistId = new DatelistId(testDate.getDateId(), theUser.getUserId());
		testdl.setId(dlistId);
		testdl.setDate(testDate);
		testdl.setUser(theUser);
		testdlEJB.saveDatelist(testdl);
		
	}
	
	@Deployment
    public static WebArchive createDeployment() {

        File[] libs = Maven.resolver().loadPomFromFile("pom.xml").resolve("mysql:mysql-connector-java").withTransitivity().asFile();

        return ShrinkWrap.create(WebArchive.class,"test.war")
        		.addPackage(TDate.class.getPackage())
        		.addClasses(DateEJB.class, MeetingEJB.class, UserEJB.class, UserWrapper.class, DatelistEJB.class)
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsLibraries(libs)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }
	
	@Test
	public void testSaveDatelist() {
		assertTrue(testdlEJB.getAll().contains(testdl));
	
	}

	@Test
	public void testUpdateDatelist() {
		testdl.setWaiting("yes");
		testdlEJB.updateDatelist(testdl);
		assertTrue(testdlEJB.getAll().contains(testdl));
		System.out.println("updateworks");
		

	}

	@Test
	public void testRemoveDateList() {
		testdlEJB.removeDateList(testdl);
		assertFalse(testdlEJB.getAll().contains(testdl));
	}

	@Test
	public void testGetAll() {
		assertTrue(testdlEJB.getAll().contains(testdl));
	}

}

package ejb;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import entity.Grouping;
import entity.GroupingId;
import entity.Groups;
import entity.User;
import model.UserWrapper;

import org.junit.Test;

/**
 * This class tests GroupingEJB
 * @author JS
 *
 */


@Transactional(TransactionMode.ROLLBACK)
@RunWith(Arquillian.class)
public class GroupingEJBTest {

	@PersistenceContext
	private EntityManager em;
	@Inject
	GroupsEJB testgEJB;
	@Inject
	UserEJB testuEJB;
	@Inject 
	GroupingEJB testEJB;
	
	User theUser =null;
	Groups testGroup = null;
	Grouping testGrouping = null;
	List<Grouping> testList = null;
	@Before
	public void init() throws Exception{
		//User part
		theUser = new User();
		theUser.setUserName("test1");
		theUser.setEmail("test@test.de");
		theUser.setPassword("test");
		theUser.setRole("user");
		theUser.setRegistered("yes");
		testuEJB.saveUser(theUser);
		theUser = testuEJB.getUserByEmail("test@test.de");
		//Group part
		testGroup = new Groups();
		testGroup.setGroupsAdmin(theUser.getUserId());
		testGroup.setGroupsDescription("The TestGroup");
		testGroup.setGroupsName("TestGroup");
		testgEJB.saveGroup(testGroup);
		//Grouping part
		testGrouping = new Grouping();
		testGrouping.setId(new GroupingId(testGroup.getGroupsId(), theUser.getUserId()));
		testGrouping.setGroups(testGroup);
		testGrouping.setUser(theUser);
		
		testEJB.saveGrouping(testGrouping);
		
	}

	@Deployment
    public static WebArchive createDeployment() {

        File[] libs = Maven.resolver().loadPomFromFile("pom.xml").resolve("mysql:mysql-connector-java").withTransitivity().asFile();

        return ShrinkWrap.create(WebArchive.class,"test.war")
        		.addPackage(User.class.getPackage())
        		.addClasses(UserEJB.class, UserWrapper.class, GroupsEJB.class, GroupingEJB.class)
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsLibraries(libs)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

	@Test
	public void testSaveGrouping() {
		assertTrue(testEJB.getAll().contains(testGrouping));
	}

	@Test
	public void testUpdateGroup() {
		testGrouping.setValidated("yes");
		testEJB.updateGroup(testGrouping);
		assertTrue(testEJB.getAll().contains(testGrouping));
	}

	@Test
	public void testRemove() {
		testEJB.remove(testGrouping);
		assertFalse(testEJB.getAll().contains(testGrouping));
	}


}

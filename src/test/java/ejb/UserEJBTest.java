package ejb;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import entity.User;
import model.UserWrapper;



/**
 * This class tests UserEJB
 * @author JS
 *
 */


@Transactional(TransactionMode.ROLLBACK)
@RunWith(Arquillian.class)
public class UserEJBTest {	
	@PersistenceContext
	private EntityManager em;
	User theUser =null;
	@Inject
	UserEJB testEJB;

	@Before
	public void init() throws Exception{
		theUser = new User();
		theUser.setUserName("test1");
		theUser.setEmail("test@test.de");
		theUser.setPassword("test");
		theUser.setRole("user");
		theUser.setRegistered("yes");
		testEJB.saveUser(theUser);

	}
	@Deployment
    public static WebArchive createDeployment() {

        File[] libs = Maven.resolver().loadPomFromFile("pom.xml").resolve("mysql:mysql-connector-java").withTransitivity().asFile();

        return ShrinkWrap.create(WebArchive.class,"test.war")
        		.addPackage(User.class.getPackage())
        		.addClasses(UserEJB.class, UserWrapper.class)
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsLibraries(libs)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

	@Test
	public void testSaveUser() {
		User tUser= em.find(User.class, theUser.getUserId());
		assertEquals(tUser, theUser);
		System.out.println("save works");
	}

	@Test
	public void testFindById() {
		assertNotNull(testEJB.findById(theUser.getUserId()));
		assertEquals(testEJB.findById(theUser.getUserId()), theUser);
		System.out.println("Find works");
		
	}
	@Test
	public void testRemoveUser() {
		testEJB.removeUser(theUser);
		assertNull(em.find(User.class, theUser.getUserId()));
		System.out.println("remove works");
	}


	@Test
	public void testFindByEMail() {
		assertEquals(theUser, testEJB.findByEMail("test@test.de"));
		System.out.println("findbyEmail works");
	}
	
	@Test
	public void testGetAll() {
		List<User> testList = testEJB.getAll();
		assertTrue(testList.contains(theUser));
		System.out.println("GetAll works");
	}

	@Test
	public void testGetUsers() {
		List<User> testList = testEJB.getUsers(true);
		assertTrue(testList.contains(theUser));
		List<User> testList2 = testEJB.getUsers(false);
		assertFalse(testList2.contains(theUser));
		System.out.println("getUsers works");
	}
	
	@Test
	public void testGetUserByEmail() {
		assertEquals(theUser, testEJB.getUserByEmail("test@test.de"));
		System.out.println("getUserByMail works");
		
	}

	@Test
	public void testDoesEmailAlreadyExist() {
		assertTrue(testEJB.doesEmailAlreadyExist("test@test.de"));
		testEJB.removeUser(theUser);
		assertFalse(testEJB.doesEmailAlreadyExist("test@test.de"));
		System.out.println("emailexists works");
	}

}

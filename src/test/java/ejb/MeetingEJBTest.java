package ejb;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.UserWrapper;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import entity.Meeting;
import entity.TDate;
import entity.User;

/**
 * This class tests MeetingEJB
 * @author JS
 *
 */



@Transactional(TransactionMode.ROLLBACK)
@RunWith(Arquillian.class)
public class MeetingEJBTest {
	

	@PersistenceContext
	private EntityManager em;
	Meeting testMeet = null;
	User theUser = null;
	@Inject
	MeetingEJB testmEJB;
	@Inject
	UserEJB testuEJB;
	@Before
	public void init() throws Exception{
		theUser = new User();
		theUser.setUserName("test1");
		theUser.setEmail("test@test.de");
		theUser.setPassword("test");
		theUser.setRole("user");
		theUser.setRegistered("yes");
		testuEJB.saveUser(theUser);
		theUser = testuEJB.getUserByEmail("test@test.de");
		testMeet = new Meeting();
		testMeet.setCreator(theUser.getUserId());
		testMeet.setDescription("testMessage");
		testMeet.setMaxParticipants(5);
		testMeet.setMinParticipants(2);
		testMeet.setAdminLink("www.flexplan.de/test1");
		testMeet.setUserLink("www.flexplan.de/usertest1");
		testMeet.setTitle("Testmeeting");
		testMeet.setLocation("Testloc");
		testMeet.setInvitemessage("Testers you're invited");
		testmEJB.saveMeeting(testMeet);
	}
	
	@Deployment
    public static WebArchive createDeployment() {

        File[] libs = Maven.resolver().loadPomFromFile("pom.xml").resolve("mysql:mysql-connector-java").withTransitivity().asFile();

        return ShrinkWrap.create(WebArchive.class,"test.war")
        		.addPackage(Meeting.class.getPackage())
        		.addClasses(MeetingEJB.class, UserEJB.class, UserWrapper.class)
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsLibraries(libs)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

	
	@Test
	public void testSaveMeeting() {
		List<Meeting> testList = testmEJB.getAll();
		assertTrue(testList.contains(testMeet));
		
	}

	@Test
	public void testUpdateMeeting() {
		testMeet.setMaxParticipants(6);
		testmEJB.updateMeeting(testMeet);
		List<Meeting> testList = testmEJB.getAll();
		assertTrue(testList.contains(testMeet));
		
	}

	@Test
	public void testRemoveMeeting() {
		testmEJB.removeMeeting(testMeet);
		List<Meeting> testList = testmEJB.getAll();
		assertFalse(testList.contains(testMeet));
	}
/*
	@Test
	public void testFindMeetingById() {
		assertEquals(testMeet.getMeetingId(), testmEJB.findMeetingById(testMeet.getMeetingId()));
	}
*/
	@Test
	public void testGetAll() {
		assertNotNull(testmEJB.getAll());
	}

}

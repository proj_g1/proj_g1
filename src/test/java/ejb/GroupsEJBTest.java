package ejb;
import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.TransactionMode;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import entity.Groups;
import entity.User;
import model.UserWrapper;


/**
 * This class tests GroupsEJB
 * @author JS
 *
 */


@Transactional(TransactionMode.ROLLBACK)
@RunWith(Arquillian.class)
public class GroupsEJBTest {

	
	@PersistenceContext
	private EntityManager em;
	@Inject
	GroupsEJB testEJB;
	@Inject
	UserEJB testuEJB;
	
	User theUser =null;
	Groups testGroup = null;
	List<Groups> testList = null;
	@Before
	public void init() throws Exception{
		theUser = new User();
		theUser.setUserName("test1");
		theUser.setEmail("test@test.de");
		theUser.setPassword("test");
		theUser.setRole("user");
		theUser.setRegistered("yes");
		testuEJB.saveUser(theUser);
		theUser = testuEJB.getUserByEmail("test@test.de");
		testGroup = new Groups();
		testGroup.setGroupsAdmin(theUser.getUserId());
		testGroup.setGroupsDescription("The TestGroup");
		testGroup.setGroupsName("TestGroup");
		testEJB.saveGroup(testGroup);
	}
	@Deployment
    public static WebArchive createDeployment() {

        File[] libs = Maven.resolver().loadPomFromFile("pom.xml").resolve("mysql:mysql-connector-java").withTransitivity().asFile();

        return ShrinkWrap.create(WebArchive.class,"test.war")
        		.addPackage(User.class.getPackage())
        		.addClasses(UserEJB.class, UserWrapper.class, GroupsEJB.class)
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsLibraries(libs)
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

	@Test
	public void testSaveGroup() {
		assertTrue(testEJB.getAll().contains(testGroup));
		System.out.println("saveGroup works");
	}

	@Test
	public void testUpdateGroup() {
		testGroup.setGroupsName("updatedTestGroup");
		testEJB.updateGroup(testGroup);
		assertTrue(testEJB.getAll().contains(testGroup));
		
		
	}
	@Test
	public void testRemove() {
		testEJB.remove(testGroup);
		assertFalse(testEJB.getAll().contains(testEJB));
	}

	@Test
	public void testGetAll() {
		assertTrue(testEJB.getAll().contains(testGroup));
	}

	@Test
	public void testGetMyGroupsByUserID() {
		assertNotNull(testEJB.getMyGroupsByUserID(theUser.getUserId()));
		assertTrue((testEJB.getMyGroupsByUserID(theUser.getUserId())).contains(testGroup));
		
	}

}

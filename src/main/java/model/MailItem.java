package model;

/**
 * Model representation to display a E-Mail in the webpage.
 * 
 * @author OK
 *
 */
public class MailItem {
	/**
	 * Wrapper for the string representation of a user e-mail.
	 * 
	 * @param email
	 *            Users e-mail
	 */
	public MailItem(String email) {
		this.value = email;
	}

	/**
	 * Entered E-Mail.
	 */
	private String value;

	/**
	 * Getter method for value.
	 * 
	 * @return Entered E-Mail.
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Setter method for value.
	 * 
	 * @param value
	 *            Entered E-Mail.
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return value;
	}

}
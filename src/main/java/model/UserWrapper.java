package model;

/**
 * Wrapper for name, mail and id from the database table User.
 * 
 * @author OK
 *
 */
public class UserWrapper {
	/**
	 * Value of userName.
	 */
	private String name;
	/**
	 * Value of email.
	 */
	private String mail;
	/**
	 * Value of userId.
	 */
	private int id;
	
	/**
	 * Creates a empty Wrapper.
	 */
	public UserWrapper() {
		
	}
	
	/**
	 * Populates the fields of the class.
	 * 
	 * @param name userName
	 * @param mail email
	 * @param id userId
	 */
	public UserWrapper(String name, String mail, int id) {
		this.name = name;
		this.mail = mail;
		this.id = id;
	}	
	
	/**
	 * Getter method for name.
	 * 
	 * @return Name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Setter method for name.
	 * 
	 * @param name Name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Getter method for mail.
	 * 
	 * @return E-Mail
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * Setter method for mail.
	 * 
	 * @param mail E-Mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * Getter method for id.
	 * 
	 * @return ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter method for id.
	 * @param id ID
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((mail == null) ? 0 : mail.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserWrapper other = (UserWrapper) obj;
		if (id != other.id)
			return false;
		if (mail == null) {
			if (other.mail != null)
				return false;
		} else if (!mail.equals(other.mail))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id + " " + name + " " + mail;
	}
	
	
}

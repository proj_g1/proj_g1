package model;

import org.joda.time.DateTime;

/**
 * Wrapps a date together with the participating users for that date.
 * 
 * @author OK
 *
 */
public class EditBeanDateWrapper implements Comparable<EditBeanDateWrapper> {
	/**
	 * Date from a meeting.
	 */
	private DateTime date;
	/**
	 * Users participating at date.
	 */
	private int participatingUsers;

	/**
	 * No values are set.
	 */
	public EditBeanDateWrapper() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Fills the dataelements with the specified date and participatingUsers.
	 * 
	 * @param date
	 *            Date from a meeting
	 * @param participatingUsers
	 *            Users participating at date.
	 */
	public EditBeanDateWrapper(DateTime date, int participatingUsers) {
		this.date = date;
		this.participatingUsers = participatingUsers;
	}

	/**
	 * Getter method for date.
	 * 
	 * @return Date from a meeting
	 */
	public  DateTime getDate() {
		return date;
	}

	/**
	 * Setter method for date.
	 * 
	 * @param date
	 *            Date from a meeting
	 */
	public  void setDate(DateTime date) {
		this.date = date;
	}

	/**
	 * Getter method for participatingUsers.
	 * 
	 * @return Users participating at date.
	 */
	public  int getParticipatingUsers() {
		return participatingUsers;
	}

	/**
	 * Setter method for participatingUsers.
	 * 
	 * @param participatingUsers
	 *            Users participating at date.
	 */
	public  void setParticipatingUsers(int participatingUsers) {
		this.participatingUsers = participatingUsers;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public  int compareTo(EditBeanDateWrapper o) {
		return date.compareTo(o.date);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public  int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public  boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EditBeanDateWrapper other = (EditBeanDateWrapper) obj;
		if (date == null) {
			if (other.date != null) {
				return false;
			}
		} else if (!date.equals(other.date)) {
			return false;
		}
		return true;
	}
}

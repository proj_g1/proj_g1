package ejb;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import entity.Meeting;

/**
 * EJB containing database actions concerning the share URLs and the creation of
 * those.
 * 
 * @author OK
 *
 */
@LocalBean
@Stateful
public class UrlEJB implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Length of the generated part of the URL to access a meeting.
	 */
	public static final int URL_IDENTIFIER_LENGTH = 8;
	/**
	 * Base Structure to the survey webpage.
	 */
	public static final String SHARELINK_BASE = "/flexplan/survey/survey.xhtml?id=";
	/**
	 * Base Structure to the editevent webpage.
	 */
	public static final String ADMINLINK_BASE = "/flexplan/edit/editevent.xhtml?id=";
	/**
	 * Base Structure to the ics webpage.
	 */
	public static final String ICS_BASE = "/flexplan/calendar/ics.xhtml?id=";
	/**
	 * Keyspace of the URL Identifier.
	 */
	private char[] pool;
	/**
	 * Used to build the URL Identifier.
	 */
	private StringBuilder url;
	/**
	 * All generated URL from the database to avoid duplicates.
	 */
	private transient List<String> generatedUrls;
	/**
	 * RNG to create the URL Identifier.
	 */
	private Random rnd;
	/**
	 * EntityManager to perform the database queries.
	 */
	@PersistenceContext
	private transient EntityManager em;

	/**
	 * Sets up the keyspace and loads the existing urls from the the database.
	 */
	@PostConstruct
	private void init() {
		pool = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
		generatedUrls = getUrlsFromDatabase();
	}

	/**
	 * @return String representation of the urls queried from the Meeting
	 *         database table.
	 */
	public List<String> getUrlsFromDatabase() {
		List<String> urlList = new LinkedList<String>();
		// Query q = em.createQuery("Select meeting from Meeting meeting");
		// List<Meeting> meetings = (List<Meeting>)q.getResultList();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<Meeting> root = criteriaQuery.from(Meeting.class);
		CriteriaQuery<Object> select = criteriaQuery.select(root);
		Query q = em.createQuery(select);
		List<Meeting> meetings = (List<Meeting>) q.getResultList();

		for (int i = 0; i < meetings.size(); i++) {
			urlList.add(meetings.get(i).getUserLink());
			urlList.add(meetings.get(i).getAdminLink());
		}
		return urlList;
	}

	/**
	 * Generates a unique URL identification String. Checks if the String
	 * already exists and only returns unique Strings.
	 * 
	 * @return Random string in URL_IDENTIFIER_LENGTH
	 */
	public synchronized String getGeneratedURL() {
		rnd = new Random();
		boolean generated = false;
		String urlString = "";

		while (!generated) {
			url = new StringBuilder();
			for (int i = 0; i < URL_IDENTIFIER_LENGTH; i++) {
				url.append(pool[rnd.nextInt(pool.length)]);
			}
			urlString = url.toString();

			if (!generatedUrls.contains(urlString)) {
				generatedUrls.add(urlString);
				generated = true;
			}
		}
		return urlString;
	}

	/**
	 * Returns the baselink to the survey webpage. For example:
	 * "http://someip/flexplan/survey/survey.xhtml?id=".
	 * 
	 * @return Baselink to the survey webpage
	 */
	public String shareLinkBase() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		String url = request.getRequestURL().toString();
		String baseURL = url.substring(0, url.length()
				- request.getRequestURI().length());
		baseURL += SHARELINK_BASE;
		return baseURL;
	}

	/**
	 * Returns the baselink to the editevent webpage. For example:
	 * "http://someip/flexplan/edit/editevent.xhtml?id=".
	 * 
	 * @return Baselink to the editevent webpage
	 */
	public String adminLinkBase() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		String url = request.getRequestURL().toString();
		String baseURL = url.substring(0, url.length()
				- request.getRequestURI().length());
		baseURL += ADMINLINK_BASE;
		return baseURL;
	}
	
	/**
	 * Returns the baselink to the ics webpage. For example:
	 * "http://someip/flexplan/calendar/ics.xhtml?id=".
	 * 
	 * @return Baselink to the ics webpage
	 */
	public String icsLinkBase() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		String url = request.getRequestURL().toString();
		String baseURL = url.substring(0, url.length()
				- request.getRequestURI().length());
		baseURL += ICS_BASE;
		return baseURL;
	}
}
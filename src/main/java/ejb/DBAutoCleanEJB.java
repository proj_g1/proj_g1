package ejb;

import java.io.Serializable;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import bean.AdminBean;

/**
 * Executes the database cleaning methods from AdminBean at a given interval.
 * 
 * @author OK
 *
 */
@ManagedBean
@Singleton
public class DBAutoCleanEJB implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Milliseconds per Day.
	 */
	public static final long MILLISECONDS_PER_DAY = 24L * 3600 * 1000;
	/**
	 * Milliseconds before the timer starts.
	 */
	public static final long STARTUP = 1000L;
	/**
	 * Logger.
	 */
	static final Logger LOGGER = Logger.getLogger(DBAutoCleanEJB.class
			.getName());

	/**
	 * Injection of AdminBean to call the necessary database cleaning methods.
	 */
	@Inject
	private AdminBean adminBean;

	/**
	 * TimerService needed to execute the cleaning on a given schedule.
	 */
	@Resource
	private transient TimerService timerService;

	/**
	 * Starts the cleaning of the database in a second for the first time and
	 * waits for the specified days before being executed again.
	 * 
	 * @param days
	 *            Number of days before the database is cleaned again
	 */
	public void startMonitoring(int days) {
		Collection<Timer> timers = timerService.getTimers();
		boolean check = false;
		for (Timer timer : timers) {
			if ("MyTimer".equals(timer.getInfo())) {
				check = true;
				break;
			}
		}
		if (!check) {
			timerService.createTimer(STARTUP, convertDaysToMilliseconds(days),
					"MyTimer");
		}

	}

	/**
	 * Turns the specified days into milliseconds.
	 * 
	 * @param days
	 *            Days to be converted to milliseconds
	 * @return days in a millisecond representation
	 */
	private long convertDaysToMilliseconds(int days) {
		return days * MILLISECONDS_PER_DAY;
	}

	/**
	 * Stops the currently running database cleaning job.
	 */
	public void stopMonitoring() {
		Collection<Timer> timers = timerService.getTimers();
		for (Timer timer : timers) {
			if ("MyTimer".equals(timer.getInfo())) {
				timer.cancel();
				break;
			}
		}
	}

	/**
	 * Actual job that does the database cleaning.
	 */
	@Timeout
	public void onTimeout() {
		LOGGER.log(Level.INFO, "Beginn database cleaning:");
		adminBean.searchOldMeetings(true);
		adminBean.cleanOldMeetings(true);
		adminBean.searchOldUsers(true);
		adminBean.cleanOldUsers(true);
		LOGGER.log(Level.INFO, "End database cleaning");
	}
}

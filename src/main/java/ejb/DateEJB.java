package ejb;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entity.TDate;

/**
 * EJB containing database actions concerning the Date database table.
 * 
 * @author OK
 *
 */
@LocalBean
@Stateless
public class DateEJB implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * EntityManager to perform the database queries.
	 */
	@PersistenceContext
	private transient EntityManager em;
	
	
	/**
	 * Updates the specified date in the database.
	 * 
	 * @param date
	 *            date to be updated in the database
	 */
	public void updateDate(TDate date) {
		em.merge(date);
	}

	/**
	 * Query the database to return all entries in Groups.
	 * 
	 * @return All entries in the Groups database table
	 */
	public List<TDate> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<TDate> root = criteriaQuery.from(TDate.class);
		CriteriaQuery<Object> select = criteriaQuery.select(root);
		Query q = em.createQuery(select);
		return (List<TDate>) q.getResultList();
	}

	/**
	 * Removes the given date from the Database.
	 * 
	 * @param date
	 *            TDate to be removed from the Database
	 */
	public void remove(TDate date) {
		em.remove(em.contains(date) ? date : em.merge(date));
	}
}

package ejb;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entity.Meeting;

/**
 * EJB containing database actions concerning the Groups database table.
 * 
 * @author OK
 *
 */
@LocalBean
@Stateless
public class MeetingEJB implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * EntityManager to perform the database queries.
	 */
	@PersistenceContext
	private transient EntityManager em;

	/**
	 * Persist the specified meeting in the database.
	 * 
	 * @param meeting
	 *            Meeting to be persisted in the database
	 */
	public void saveMeeting(Meeting meeting) {
		em.persist(meeting);
	}

	/**
	 * Update the specified meeting in the database.
	 * 
	 * @param meeting
	 *            Meeting to be updated in the database
	 */
	public void updateMeeting(Meeting meeting) {
		em.merge(meeting);
	}

	/**
	 * Remove the specified meeting from the database.
	 * 
	 * @param meeting
	 *            Meeting to be removed from the database
	 */
	public void removeMeeting(Meeting meeting) {
		em.remove(em.contains(meeting) ? meeting : em.merge(meeting));
	}

	/**
	 * Find a meeting with the specified id.
	 * 
	 * @param id
	 *            Id to search in the database
	 * @return Meeting with the specified id or null
	 */
	public Meeting findMeetingById(int id) {
		return em.find(Meeting.class, id);
	}

	/**
	 * Query the database to return all entries in Meeting.
	 * 
	 * @return All entries in the Meeting database table
	 */
	public List<Meeting> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<Meeting> root = criteriaQuery.from(Meeting.class);
		CriteriaQuery<Object> select = criteriaQuery.select(root);
		Query q = em.createQuery(select);
		return (List<Meeting>) q.getResultList();
	}
}

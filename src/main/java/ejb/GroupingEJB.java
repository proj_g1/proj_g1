package ejb;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entity.Grouping;

/**
 * EJB containing database actions concerning the Grouping database table.
 * 
 * @author OK
 *
 */
@LocalBean
@Stateless
public class GroupingEJB implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * EntityManager to perform the database queries.
	 */
	@PersistenceContext
	private transient EntityManager em;

	/**
	 * Persists the specified grouping in the database.
	 * 
	 * @param group
	 *            Grouping to be persisted in the database
	 */
	public void saveGrouping(Grouping grouping) {
		em.persist(grouping);
	}

	/**
	 * Updates the specified grouping in the database.
	 * 
	 * @param group
	 *            Grouping to be updated in the database
	 */
	public void updateGroup(Grouping grouping) {
		em.merge(grouping);
	}

	/**
	 * Removes the specified grouping from the database.
	 * 
	 * @param group
	 *            Grouping to be removed from the database
	 */
	public void remove(Grouping grouping) {
		em.remove(em.contains(grouping) ? grouping : em.merge(grouping));
	}

	/**
	 * Query the database to return all entries in Grouping.
	 * 
	 * @return All entries in the Groups database table
	 */
	public List<Grouping> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<Grouping> root = criteriaQuery.from(Grouping.class);
		CriteriaQuery<Object> select = criteriaQuery.select(root);
		Query q = em.createQuery(select);
		return (List<Grouping>) q.getResultList();
	}
}

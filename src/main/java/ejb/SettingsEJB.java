package ejb;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entity.Settings;

/**
 * EJB containing database actions concerning the Settings database table.
 * 
 * @author OK
 *
 */
@LocalBean
@Stateless
public class SettingsEJB implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * EntityManager to perform the database queries.
	 */
	@PersistenceContext
	private transient EntityManager em;

	/**
	 * Persists the specified settings in the database.
	 * 
	 * @param settings
	 *            settings to be persisted in the database
	 */
	public void savesettings(Settings settings) {
		em.persist(settings);
	}

	/**
	 * Updates the specified settings in the database.
	 * 
	 * @param settings
	 *            settings to be updated in the database
	 */
	public void updatesettings(Settings settings) {
		em.merge(settings);
	}

	/**
	 * Removes the specified settings from the database.
	 * 
	 * @param group
	 *            settings to be removed from the database
	 */
	public void remove(Settings settings) {
		em.remove(em.contains(settings) ? settings : em.merge(settings));
	}

	/**
	 * Find a setting with the specified settingsName.
	 * 
	 * @param settingsName
	 *            Name to search in the database
	 * @return Settings with the specified settingsName or null
	 */
	public Settings findByName(String settingsName) {
		return em.find(Settings.class, settingsName);
	}

	/**
	 * Query the database to return all entries in settings.
	 * 
	 * @return All entries in the Groups database table
	 */
	public List<Settings> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<Settings> root = criteriaQuery.from(Settings.class);
		CriteriaQuery<Object> select = criteriaQuery.select(root);
		Query q = em.createQuery(select);
		return (List<Settings>) q.getResultList();
	}
}

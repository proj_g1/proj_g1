package ejb;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entity.Datelist;

/**
 * EJB containing database actions concerning the Datelist database table.
 * 
 * @author OK
 *
 */
@LocalBean
@Stateless
public class DatelistEJB implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * EntityManager to perform the database queries.
	 */
	@PersistenceContext
	private transient EntityManager em;

	/**
	 * Persists the specified datelist in the database.
	 * 
	 * @param group
	 *            datelist to be persisted in the database
	 */
	public void saveDatelist(Datelist datelist) {
		em.persist(datelist);
	}

	/**
	 * Updates the specified datelist in the database.
	 * 
	 * @param group
	 *            datelist to be updated in the database
	 */
	public void updateDatelist(Datelist datelist) {
		em.merge(datelist);
	}

	/**
	 * Removes the specified datelist from the database.
	 * 
	 * @param group
	 *            datelist to be removed from the database
	 */
	public void removeDateList(Datelist datelist) {
		em.remove(em.contains(datelist) ? datelist : em.merge(datelist));
	}

	/**
	 * Query the database to return all entries in datelist.
	 * 
	 * @return All entries in the Groups database table
	 */
	public List<Datelist> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<Datelist> root = criteriaQuery.from(Datelist.class);
		CriteriaQuery<Object> select = criteriaQuery.select(root);
		Query q = em.createQuery(select);
		return (List<Datelist>) q.getResultList();
	}
}

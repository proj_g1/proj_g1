package ejb;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

/**
 * EJB that is used to send Facemessages to the webpage.
 * 
 * @author OK
 *
 */
@LocalBean
@Stateless
public class MessageEJB implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Throws a Facesmessage with the given severity and message.
	 * 
	 * @param severity
	 *            FacesMessage Enum that declares the Severity
	 * @param message
	 *            Message that is looked up in the translation file
	 */
	public void sendFacesMessage(Severity severity, String message) {
		ResourceBundle bundle = ResourceBundle.getBundle("text", FacesContext
				.getCurrentInstance().getViewRoot().getLocale());

		String m = bundle.getString(message);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, m, m));
	}
	
	/**
	 * Throws a targeted FacesMessage with a given UIComponent, Severity and message.
	 * This is targeted because it aims at a specific <p:message id="..." for="...">.
	 * 
	 * @author AH
	 * @param component
	 *        UIComponent component which is defined in a bean class
	 * @param severity
	 *        FacesMessage Enum that declares the Severity
	 * @param message
	 *        Message that is looked up in the translation file
	 */
	public void sendFacesMessage(UIComponent component, Severity severity, String message) {
		ResourceBundle bundle = ResourceBundle.getBundle("text", FacesContext
				.getCurrentInstance().getViewRoot().getLocale());

		String m = bundle.getString(message);
		FacesContext.getCurrentInstance().addMessage(component.getClientId(), 
				new FacesMessage(severity,m,m));
	}
	
	/**
	 * Throws a formated FacesMessage with a given severity, 
	 * message and webInput.
	 * 
	 * @author AH
	 * @param severity 
	 *        FacesMessage Enum that declares the Severity
	 * @param message
	 *        Message that is looked up in the translation file
	 * @param webInput 
	 *        This is the string that varies by user input.
	 * @param placeholder 
	 *        This is just a placeholder to differentiate between 
	 * an already existing method which I didn't want to modify since 
	 * it's not my and is used everywhere.
	 */
	public void sendFacesMessage(Severity severity, String message, String webInput, int placeholder) {
		ResourceBundle bundle = ResourceBundle.getBundle("text", FacesContext
				.getCurrentInstance().getViewRoot().getLocale());

		String msg = bundle.getString(message);
		msg = MessageFormat.format(msg, webInput);
		
		FacesContext.getCurrentInstance().addMessage(null, 
				new FacesMessage(severity,msg,msg));
	}

	/**
	 * Throws a Facesmessage with the given severity and the prefix added in
	 * front of the message followed by the message. Example: message =
	 * "People", messagePrefix ="8" would throw a FacesMessage with "8 People"
	 * 
	 * @param severity
	 *            FacesMessage Enum that declares the Severity
	 * @param message
	 *            Message that is looked up in the translation file
	 * @param messagePrefix
	 *            String to be put in front of the translation message
	 */
	public void sendFacesMessage(Severity severity, String message,
			String messagePrefix) {
		ResourceBundle bundle = ResourceBundle.getBundle("text", FacesContext
				.getCurrentInstance().getViewRoot().getLocale());

		String m = messagePrefix + " " + bundle.getString(message);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, m, m));
	}
}

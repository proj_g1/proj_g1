package ejb;

import java.util.Collections;
import java.util.LinkedList;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import util.TDateUserComparator;
import entity.Datelist;
import entity.Meeting;
import entity.TDate;

/**
 * 
 * @author EW This class supports a method to get the most efficient dates of a
 *         meeting
 */

@LocalBean
@Stateless
public class DateEvaluationEJB {

	/**
	 * this method searches the dates where the most participants has accepted.
	 * if there is only one date with most participants, only this date will be
	 * returned, if there are more dates with the same number of participants,
	 * all of this dates will be returned
	 * 
	 * @param meeting
	 * @return a list with most efficient dates
	 */
	public LinkedList<TDate> getMostEfficientDates(Meeting meeting) {
		LinkedList<TDate> fittingDates = new LinkedList<TDate>();

		if (meeting.getMinParticipants() <= meeting.getUsers().size()) {
			fittingDates = new LinkedList<TDate>(meeting.getDates());
			Collections.sort(fittingDates, new TDateUserComparator());

			LinkedList<TDate> removeDates = new LinkedList<TDate>();
			int maxCounter = 0;
			for (TDate tDate : fittingDates) {
				int counter = 0;
				for (Datelist dl : tDate.getDatelists()) {
					if (dl.getWaiting() != null
							&& !dl.getWaiting().equalsIgnoreCase("yes"))
						counter++;
				}
				if (counter >= maxCounter) {
					maxCounter = counter;
				} else {
					removeDates.add(tDate);
				}
			}
			for (TDate tDate : removeDates) {
				fittingDates.remove(tDate);
			}
		}

		return fittingDates;
	}
}

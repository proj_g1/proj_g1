package ejb;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import model.UserWrapper;
import entity.User;

/**
 * EJB containing database actions concerning the User database table.
 * 
 * @author OK
 *
 */
@LocalBean
@Stateless
public class UserEJB implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * List of wrappers that contains the User name, mail and id from the
	 * database table User.
	 */
	private transient List<UserWrapper> userNameMailIds;

	/**
	 * EntityManager to perform the database queries.
	 */
	@PersistenceContext
	private transient EntityManager em;

	/**
	 * Persists the specified user in the database.
	 * 
	 * @param user
	 *            User to be persisted in the database
	 */
	public void saveUser(User user) {
		em.persist(user);
	}

	/**
	 * Updates the specified user in the database.
	 * 
	 * @param user
	 *            User to be updated in the database
	 */
	public void updateUser(User user) {
		em.merge(user);
	}

	/**
	 * Removes the specified user from the database.
	 * 
	 * @param user
	 *            User to be removed from the database
	 */
	public void removeUser(User user) {
		em.remove(em.contains(user) ? user : em.merge(user));
	}

	/**
	 * Find a user with the specified id.
	 * 
	 * @param id
	 *            UserID to search
	 * @return User with the specified id or null
	 */
	public User findById(int id) {
		return em.find(User.class, id);
	}

	/**
	 * Returns the registered User with the specified E-Mail or null.
	 * 
	 * @param email
	 *            E-Mail Address of the registered user
	 * @return Registered user with the specified E-Mail or null.
	 */
	public User findByEMail(String email) {
		List<User> users = getUsers(true);
		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getEmail().equals(email)) {
				return users.get(i);
			}
		}
		return null;
	}

	/**
	 * Fetches User data from the database table User either from the registered
	 * or anonymous users.
	 * 
	 * @param registered
	 *            return values from the registered (true) or anonymous users
	 *            (false)
	 * @return List of Wrappers that contain the name, mail and id of the
	 *         database table User entries
	 */
	public List<UserWrapper> getUserNameMailIds(boolean registered) {
		userNameMailIds = new LinkedList<UserWrapper>();
		String arg1;
		arg1 = registered ? "yes" : "no";
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<User> from = criteriaQuery.from(User.class);
		CriteriaQuery<Object> select = criteriaQuery.select(from);
		Expression<String> literal = cb.upper(cb.literal(arg1));
		Predicate predicate = cb
				.like(cb.upper(from.get("registered")), literal);

		criteriaQuery.where(predicate);
		Query q = em.createQuery(select);
		List<User> userList = (List<User>) q.getResultList();

		for (int i = 0; i < userList.size(); i++) {
			userNameMailIds.add(new UserWrapper(userList.get(i).getUserName(),
					userList.get(i).getEmail(), userList.get(i).getUserId()));
		}
		return userNameMailIds;
	}

	/**
	 * Query the database to return all entries in User.
	 * 
	 * @return All entries in the User database table
	 */
	public List<User> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<User> root = criteriaQuery.from(User.class);
		CriteriaQuery<Object> select = criteriaQuery.select(root);
		Query q = em.createQuery(select);
		return (List<User>) q.getResultList();
	}

	/**
	 * Returns a List of registered or unregistered users depending on the flag
	 * registered.
	 * 
	 * @param registered
	 *            return values from the registered (true) or anonymous users
	 *            (false)
	 * @return List of users depending on the flag registered
	 */
	public List<User> getUsers(boolean registered) {
		String arg1 = "";
		arg1 = registered ? "yes" : "no";
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);
		Root<User> from = criteriaQuery.from(User.class);
		CriteriaQuery<User> select = criteriaQuery.select(from);
		Expression<String> literal = cb.upper(cb.literal((String) arg1));
		Predicate predicate = cb
				.like(cb.upper(from.get("registered")), literal);
		criteriaQuery.where(predicate);
		Query q = em.createQuery(select);
		return (List<User>) q.getResultList();
	}

	/**
	 * Get an User from the database by its email.
	 * 
	 * @author AH
	 * 
	 * @param email
	 *            by which the user should be searched by
	 * 
	 * @return User by user's email
	 */
	public User getUserByEmail(String email) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<User> q = cb.createQuery(User.class);
		Root<User> c = q.from(User.class);
		q.select(c);
		ParameterExpression<String> p = cb.parameter(String.class, "param");
		q.where(cb.equal(c.get("email"), p));
		Query query = em.createQuery(q).setParameter("param", email);
		@SuppressWarnings("unchecked")
		// This time it's ok to suppress warnings
		List<User> users = query.getResultList();
		if (users.size() == 1)
			return users.get(0);
		return null;
	}

	/**
	 * Checks if a given email is already present in the database.
	 * 
	 * @author AH
	 * 
	 * @email email to be searched
	 * 
	 * @return True if email is already present in database, False otherwise
	 */
	public boolean doesEmailAlreadyExist(String email) {
		User user = getUserByEmail(email);
		if (user == null)
			return false;
		else if (user.getEmail().equals(email))
			return true;
		return false;
	}

}

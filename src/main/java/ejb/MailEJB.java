package ejb;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.annotation.Resource;
import javax.ejb.Singleton;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerService;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import entity.Meeting;
import entity.TDate;
import entity.User;

/**
 * This class offers methods to send mails.
 * 
 * @author EW
 *
 */

@ManagedBean
@Singleton
public class MailEJB {
	@Inject
	SettingsEJB settingsEJB;

	@Inject
	MeetingEJB meetingEJB;

	@Inject
	UserEJB userEJB;

	/**
	 * 
	 */
	@Resource
	private transient TimerService timerService;

	@Inject
	private transient DateEvaluationEJB dateEvaluationService;


	/**
	 * Sends E-Mails to the specified addresses. Subject is the specified
	 * subjectFromBundle. The specified parameters will be inserted into the
	 * messageFromBundle in the given order.
	 * 
	 * @param addresses
	 *            String with E-Mail recipients. Multiple entries allowed,
	 *            separated by ","
	 * @param subjectFromBundle
	 *            Mail subject translated via resourcebundle
	 * @param messageFromBundle
	 *            message translated via resourcebundle
	 * @param parametersForMessage
	 *            parameters (String) to be inserted into the messageFromBundle.
	 *            See text.properties for the correct order.
	 */
	public void sendMail(String addresses, String subjectFromBundle,
			String messageFromBundle, Object[] parametersForMessage) {
		final String username = settingsEJB.findByName("adminMailUser")
				.getSettingsDetails();
		final String password = settingsEJB.findByName("adminMailPassword")
				.getSettingsDetails();
		final String mailAddress = settingsEJB.findByName("adminMailAddress")
				.getSettingsDetails();

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host",
				settingsEJB.findByName("adminMailSMTPServer")
						.getSettingsDetails());
		props.put("mail.smtp.port", settingsEJB.findByName("adminMailSMTP")
				.getSettingsDetails());
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(mailAddress));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(addresses));

			ResourceBundle bundle = ResourceBundle
					.getBundle("text", FacesContext.getCurrentInstance()
							.getViewRoot().getLocale());
			String sub = bundle.getString(subjectFromBundle);
			String msg = bundle.getString(messageFromBundle);
			msg = MessageFormat.format(msg, parametersForMessage);

			message.setSubject(sub);
			message.setText(msg);
			Transport.send(message);
			System.out.println("Mail sent succesfully!");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Sends E-Mails to the specified addresses. Subject is the specified
	 * subjectFromBundle. The specified parameters will be inserted into the
	 * messageFromBundle in the given order.
	 * 
	 * @param users
	 *            users that will be sent the e-mails that are localized
	 *            depending on their set locale
	 * @param subjectFromBundle
	 *            Mail subject translated via resourcebundle
	 * @param messageFromBundle
	 *            message translated via resourcebundle
	 * @param parametersForMessage
	 *            parameters (String) to be inserted into the messageFromBundle.
	 *            See text.properties for the correct order.
	 */
	public void sendMail(List<User> users, String subjectFromBundle,
			String messageFromBundle, Object[] parametersForMessage) {
		final String username = settingsEJB.findByName("adminMailUser")
				.getSettingsDetails();
		final String password = settingsEJB.findByName("adminMailPassword")
				.getSettingsDetails();
		final String mailAddress = settingsEJB.findByName("adminMailAddress")
				.getSettingsDetails();

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host",
				settingsEJB.findByName("adminMailSMTPServer")
						.getSettingsDetails());
		props.put("mail.smtp.port", settingsEJB.findByName("adminMailSMTP")
				.getSettingsDetails());
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});
		try {

			StringBuilder addressesDE = new StringBuilder();
			StringBuilder addressesEN = new StringBuilder();
			boolean firstDE = true;
			boolean firstEN = true;
			Message message;
			ResourceBundle bundle;
			String msg;
			String sub;

			for (int i = 0; i < users.size(); i++) {
				if ("de".equals(users.get(i).getLocale())) {
					if (firstDE) {
						firstDE = false;
					} else {
						addressesDE.append(",");
					}
					addressesDE.append(users.get(i).getEmail());
				} else {
					if (firstEN) {
						firstEN = false;
					} else {
						addressesEN.append(",");
					}
					addressesEN.append(users.get(i).getEmail());
				}

			}

			if (!"".equals(addressesDE.toString())) {
				message = new MimeMessage(session);
				message.setFrom(new InternetAddress(mailAddress));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(addressesDE.toString()));

				bundle = ResourceBundle.getBundle("text", new Locale("de"));
				sub = bundle.getString(subjectFromBundle);
				msg = bundle.getString(messageFromBundle);
				msg = MessageFormat.format(msg, parametersForMessage);

				message.setSubject(sub);
				message.setText(msg);
				Transport.send(message);
			}
			if (!"".equals(addressesEN.toString())) {
				message = new MimeMessage(session);
				message.setFrom(new InternetAddress(mailAddress));
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(addressesEN.toString()));

				bundle = ResourceBundle.getBundle("text", new Locale("en"));
				sub = bundle.getString(subjectFromBundle);
				msg = bundle.getString(messageFromBundle);
				msg = MessageFormat.format(msg, parametersForMessage);

				message.setSubject(sub);
				message.setText(msg);
				Transport.send(message);
			}

			System.out.println("Mail sent succesfully!");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Used to send a timed reminder to the creator of a meeting, if the meeting
	 * reached min particpants on a date and the creator didn't confirm it.
	 * 
	 * @param subjectFromBundle
	 *            Remindersubject
	 * @param messageFromBundle
	 *            Remindermessage
	 * @param adminURL
	 *            Complete adminlink URL
	 * @param time
	 *            Time Long millisecond representation of the date/time to send
	 *            the mail
	 * @param shareLink
	 *            Sharelink id part of the meeting
	 */
	public void sendTimedMail(String subjectFromBundle,
			String messageFromBundle, String adminURL, long time,
			String shareLink) {
		Date current = new Date();
		long sleep = time - current.getTime();
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("sharelink", shareLink);
		params.put("subject", subjectFromBundle);
		params.put("message", messageFromBundle);
		params.put("adminURL", adminURL);
		timerService.createTimer(sleep, params);
	}

	/**
	 * Stops a previously actived timer to not sent the reminder. Used if the
	 * creator confirmed a date.
	 * 
	 * @param sharelink
	 *            Identifies the meeting to not send a reminder for
	 */
	public void cancelTimedMail(String sharelink) {
		Collection<Timer> timers = timerService.getTimers();
		for (Timer timer : timers) {
			HashMap<String, String> params = (HashMap<String, String>) timer
					.getInfo();
			if (params.get("sharelink").equals(sharelink)) {
				timer.cancel();
				break;
			}
		}
	}

	/**
	 * Checks if meeting or creator still exist. If there are dates that have
	 * the minimum number of participants, the mail reminder is sent.
	 * 
	 * 
	 * @param timer
	 *            Timer that waited to send the mail reminder.
	 */
	@Timeout
	public void onTimeOut(Timer timer) {	
		HashMap<String, String> params = (HashMap<String, String>) timer
				.getInfo();
		String subject = params.get("subject");
		String message = params.get("message");
		String adminlink = params.get("adminURL");
		List<User> users = new LinkedList<User>();

		Meeting meeting = null;
		for (Meeting m : meetingEJB.getAll()) {
			if (m.getUserLink().equals(params.get("sharelink"))) {
				meeting = m;
			}
		}
		
		if (meeting != null) {
			User creator = userEJB.findById(meeting.getCreator());
			if (creator != null) {
				users.add(creator);

				List<TDate> dates = dateEvaluationService
						.getMostEfficientDates(meeting);
				if (!dates.isEmpty()) {
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < dates.size(); i++) {
						sb.append(dates.get(i).getDateTime().toString());
						sb.append("\n");
					}

					Object[] parametersForMessage = new Object[] {
							sb.toString(), adminlink };
					sendMail(users, subject, message, parametersForMessage);
				}
			}
		}

	}
}

package ejb;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import entity.Waitlist;

/**
 * EJB containing database actions concerning the Waitlist database table.
 * 
 * @author EW
 *
 */
@LocalBean
@Stateless
public class WaitlistEJB implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * EntityManager to perform the database queries.
	 */
	@PersistenceContext
	private transient EntityManager em;

	/**
	 * Persists the specified Waitlist in the database.
	 * 
	 * @param waitlist
	 *            Waitlist to be persisted in the database
	 */
	public void saveWaitlist(Waitlist waitlist) {
		em.persist(waitlist);
	}

	/**
	 * Updates the specified Waitlist in the database.
	 * 
	 * @param waitlist
	 *            Waitlist to be updated in the database
	 */
	public void updateWaitlist(Waitlist waitlist) {
		em.merge(waitlist);
	}

	/**
	 * Removes the specified Waitlist from the database.
	 * 
	 * @param waitlist
	 *            Waitlist to be removed from the database
	 */
	public void removeWaitList(Waitlist waitlist) {
		em.remove(em.contains(waitlist) ? waitlist : em.merge(waitlist));
	}

	/**
	 * Query the database to return all entries in Waitlist.
	 * 
	 * @return All entries in the Waitlist database table
	 */
	public List<Waitlist> getAll() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<Waitlist> root = criteriaQuery.from(Waitlist.class);
		CriteriaQuery<Object> select = criteriaQuery.select(root);
		Query q = em.createQuery(select);
		return (List<Waitlist>) q.getResultList();
	}
}

package ejb;

import java.io.Serializable;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import entity.Groups;

/**
 * EJB containing database actions concerning the Groups database table.
 * 
 * @author OK
 *
 */
@LocalBean
@Stateless
public class GroupsEJB implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * EntityManager to perform the database queries.
	 */
	@PersistenceContext
	private transient EntityManager em;

	/**
	 * Persists the specified group in the database.
	 * 
	 * @param group
	 *            Group to be persisted in the database
	 */
	public void saveGroup(Groups group) {
		em.persist(group);
	}

	/**
	 * Updates the specified group in the database.
	 * 
	 * @param group
	 *            Group to be updated in the database
	 */
	public void updateGroup(Groups group) {
		em.merge(group);
	}

	/**
	 * Removes the specified group from the database.
	 * 
	 * @param group
	 *            Group to be removed from the database
	 */
	public void remove(Groups group) {
		em.remove(em.contains(group) ? group : em.merge(group));
	}

	/**
	 * Query the database to return all entries in Groups.
	 * 
	 * @return All entries in the Groups database table
	 */
	public List<Groups> getAll() {
		// Query q = em.createQuery("Select groups from Groups groups");
		// return (List<Groups>)q.getResultList();

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Object> criteriaQuery = cb.createQuery();
		Root<Groups> root = criteriaQuery.from(Groups.class);
		CriteriaQuery<Object> select = criteriaQuery.select(root);
		Query q = em.createQuery(select);
		return (List<Groups>) q.getResultList();
	}
	
	/**
	 * @author AH
	 * 
	 * @param User ID to which a List of groups to be returned which belong to the user
	 * 
	 * @return List of groups which belong to the user.
	 */
	public List<Groups> getMyGroupsByUserID(int userID) {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Groups> q = cb.createQuery(Groups.class);
			Root<Groups> c = q.from(Groups.class);
			q.select(c);
			ParameterExpression<Integer> p = cb.parameter(Integer.class, "param");
			q.where(
					cb.equal(c.get("groupsAdmin"), p)
			);
			Query query = em.createQuery(q).setParameter("param", userID);
			@SuppressWarnings("unchecked") //This time it's ok to suppress warnings
			List<Groups> groups = query.getResultList();
			return groups;
	}
}

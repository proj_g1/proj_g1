package bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import model.MailItem;
import model.UserWrapper;

import org.joda.time.DateTime;
import org.omnifaces.util.Ajax;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import ejb.GroupsEJB;
import ejb.MailEJB;
import ejb.MeetingEJB;
import ejb.MessageEJB;
import ejb.UrlEJB;
import ejb.UserEJB;
import entity.Grouping;
import entity.Groups;
import entity.Meeting;
import entity.TDate;
import entity.User;

/**
 * Backing bean for the scheduling.xhtml webpage. Collects all needed data from
 * the user. Registers the user in the database if he doesn't exist. Generates
 * Urls for further use of the meeting and persists all the collected data in
 * the database.
 * 
 * @author OK
 */
@Named(value = "schedulingBean")
@SessionScoped
public class SchedulingBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Number of ajax pages in the scheduling process. Starts with 0(computer
	 * science style).
	 */
	public static final int SCHEDULEPAGES = 3;
	/**
	 * Current phase of the scheduling Process(1-4).
	 */
	private int pageStatus;
	/**
	 * Minimum number of participants for the meeting to take place.
	 */
	private int minParticipants;
	/**
	 * Maximum number of participants.
	 */
	private int maxParticipants;
	/**
	 * Title of the meeting.
	 */
	private String title;
	/**
	 * Location of the meeting.
	 */
	private String location;
	/**
	 * Detailed explanation of the meeting.
	 */
	private String description;
	/**
	 * Name of the current user.
	 */
	private String name;
	/**
	 * Username's E-mail.
	 */
	private String mail;
	/**
	 * Selected dates from the user.
	 */
	private transient List<DateTime> dates;
	/**
	 * Available groups.
	 */
	private transient List<Groups> groups;
	/**
	 * Groups selected by user.
	 */
	private transient List<Groups> selectedGroups;
	/**
	 * E-Mail Adresses to invite directly.
	 */
	private transient List<MailItem> inviteMails;
	/**
	 * Optional invitation message.
	 */
	private String inviteMessage;
	/**
	 * String from the current serverdate to restrict the calenderpicker in the
	 * webpage to only display future dates.
	 */
	private String timeString;
	/**
	 * Link for users to participate in the poll.
	 */
	private String sharelink;
	/**
	 * Link for the current user to edit the meetinginformation afterwards.
	 */
	private String adminlink;
	/**
	 * Date to user selects.
	 */
	private Date date;
	/**
	 * Date to send a reminder if no date was accepted.
	 */
	private Date reminderDate;
	/**
	 * Time the user selects.
	 */
	private Date time;
	/**
	 * Single Mail that is added to the inviteMails list.
	 */
	private String inviteMail;
	/**
	 * Root node for the tree to represent the groups -> users in groups with.
	 */
	private transient TreeNode groupRoot;
	/**
	 * Number of hours left on the survey before the mandatory first reminder is
	 * sent.
	 */
	private int mailNotificationHours1;

	/**
	 * Injection of the UrlEJB for link generation and DB usage.
	 */
	@Inject
	private UrlEJB urlService;
	/**
	 * Injection of the GroupsEJB for Group related DB usage.
	 */
	@Inject
	private GroupsEJB groupsService;
	/**
	 * Injection of the MeetingEJB for Meeting related DB usage.
	 */
	@Inject
	private MeetingEJB meetingService;
	/**
	 * Injection of the UserEJB for User related DB usage.
	 */
	@Inject
	private UserEJB userService;
	/**
	 * Injection of the MessageEJB for Facesmessages.
	 */
	@Inject
	private MessageEJB messageService;
	/**
	 * Injection of the MailEJB to send E-Mails.
	 */
	@Inject
	private transient MailEJB mailService;

	/**
	 * Injection of LoginBean to see if a user is logged in.
	 */
	@Inject
	private LoginBean loginBean;
	/**
	 * Injection of AdminBean to get the standard reminder timeframe.
	 */
	@Inject
	private AdminBean adminBean;

	/**
	 * (Re)sets all fields to a correct startingconfiguration. Exception are the
	 * urls to be displayed at the summary webpage.
	 */
	@PostConstruct
	public void init() {
		createTimeString();
		pageStatus = 0;
		title = "";
		location = "";
		description = "";
		if (loginBean.isLoggedIn()) {
			name = loginBean.getUser().getUserName();
			mail = loginBean.getUser().getEmail();
		} else {
			name = "";
			mail = "";
		}

		inviteMails = new ArrayList<MailItem>();
		dates = new ArrayList<DateTime>();
		minParticipants = 1;
		maxParticipants = 1;
		inviteMessage = "";
		groups = groupsService.getAll();
		selectedGroups = new LinkedList<Groups>();
		inviteMail = "";
		date = null;
		time = null;
		groupRoot = new DefaultTreeNode();
		mailNotificationHours1 = adminBean.getMailNotificationHours1();
	}

	/**
	 * Checks on page reload if the user is logged in and uses his credentials.
	 */
	public void checkLogin() {
		if (loginBean.isLoggedIn()) {
			name = loginBean.getUser().getUserName();
			mail = loginBean.getUser().getEmail();
		}
	}

	/**
	 * Advances the pageStatus to the next phase.
	 */
	public void next() {
		if ((pageStatus + 1 <= SCHEDULEPAGES)) {
			pageStatus++;
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Advances the pageStatus to the previous phase.
	 */
	public void previous() {
		if ((pageStatus - 1 >= 0)) {
			pageStatus--;
		}
		FacesContext.getCurrentInstance().renderResponse();
	}

	/**
	 * Generates the user and adminlink. Registers an anonymous User in the
	 * database. Gets the UserID to persist all the meeting data in the
	 * database. Resets the data of the bean. Sends all necessary mail to the
	 * creator and the directly invited users.
	 * 
	 * @return Returns the Webpage schedulingSummary.xhtml that displays the
	 *         results of the scheduled meeting
	 */
	public String submit() {
		// Check reminder date
		DateTime remDate = new DateTime(reminderDate);
		if (remDate.isAfter(new DateTime(dates.get(0)))
				|| remDate.isBefore(new DateTime())) {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
					"scheduleMailNotificationValidation");
			return "";
		}
		Integer userId;
		generateUrls();

		// Check if user is registered
		if (loginBean.isLoggedIn()) {
			userId = loginBean.getUser().getUserId();
		} else {
			userId = getUserId(mail, name, false);

			if (userId == -1) { // user wasn't in DB, search again
				userId = getUserId(mail, name, false);
			}
		}

		// persist meeting
		persistMeeting(userId);

		// send mails
		inviteByMail();
		mailService.sendMail(mail, "mailSchedulingLinksSubject",
				"mailSchedulingLinks", new Object[] { printShareLink(),
						printAdminLink() });

		// set reminder
		mailService.sendTimedMail("mailCreatorReminderSubject",
				"mailCreatorReminder", printAdminLink(), remDate.getMillis(),
				sharelink);

		// reset and results
		init();
		return "schedulingsummary";
	}

	/**
	 * Populates the field timeString with the current serverdate for the
	 * webpage Datepicker.
	 */
	public void createTimeString() {
		Calendar c = Calendar.getInstance();
		// c.add(Calendar.DATE, 1); //This line makes the first possible date
		// tomorrow, starting from the current server date
		Locale locale = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestLocale();
		String localeString = locale.toString();
		SimpleDateFormat dateFormat;
		if (localeString.contains("de")) {
			dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		} else {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		}
		timeString = dateFormat.format(c.getTime());
	}

	/**
	 * Add a new MailItem to the inviteMails to be represented in the webpage.
	 */
	public void addMailItem() {
		if (!inviteMail.isEmpty()) {
			boolean check = true;
			for (int i = 0; i < inviteMails.size(); i++) {
				if (inviteMails.get(i).getValue().equals(inviteMail)) {
					check = false;
				}
			}
			if (check) {
				inviteMails.add(new MailItem(inviteMail));
			} else {
				if (date == null) { // duplicate Date
					messageService.sendFacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"scheduleInviteMailsDuplicate");
				}
			}
		}
	}

	/**
	 * Delete MailItem m from field inviteMails.
	 * 
	 * @param m
	 *            MailItem to delete
	 */
	public void removeMailItem(MailItem m) {
		inviteMails.remove(m);
	}

	/**
	 * Add a new DateItem to the field dates to be represented in the webpage.
	 */
	public void addDateItem() {
		DateTime dateToAdd = new DateTime(date);
		DateTime timeOnly = new DateTime(time);

		dateToAdd = dateToAdd.plusHours(timeOnly.getHourOfDay());
		dateToAdd = dateToAdd.plusMinutes(timeOnly.getMinuteOfHour());
		if (date != null && time != null) {
			boolean check = dateTimeValidation(dateToAdd);
			if (check) {
				dates.add(dateToAdd);
				Collections.sort(dates);
			}
		} else {
			if (date == null) { // duplicate Date
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"scheduleValidationDateEmpty");
			}
			if (time == null) {
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"scheduleValidationTimeEmpty");
			}
		}
	}

	/**
	 * Delete DateItem d from the field dates.
	 * 
	 * @param d
	 *            DateItem to remove
	 */
	public void removeDateItem(DateTime d) {
		dates.remove(d);
	}

	/**
	 * Validates d to the following criteria: 1. Date with Time has to be unique
	 * 2. Date has to be in the Future
	 * 
	 * @return true if d got validated correctly (1&2 true), false otherwise
	 * 
	 */
	public boolean dateTimeValidation(DateTime d) {
		DateTime t = new DateTime();

		if (dates.contains(d)) { // duplicate Date
			messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
					"scheduleValidationTime");
			return false;
		}

		if (d.isBefore(t)) {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
					"scheduleValidationDate");
			return false;
		}

		return true;
	}

	/**
	 * Checks if at least one date was added. Calls the next() Method to advance
	 * the scheduling. Fills the date for the reminder message accordingly.
	 * 
	 * @return True if at least one Date was added. False otherwise
	 */
	public boolean checkDates() {
		if (dates.isEmpty()) {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
					"scheduleValidationNoDate");
			return false;
		} else {
			fillReminderDate();
			next();
			return true;
		}
	}

	/**
	 * Fill the weppage datepicker for the mail reminder with a date that is
	 * "earliest selected date MINUS system admin reminder settings". For
	 * Example if the admin choose "remind 1 day before the end" then the chosen
	 * date is the earliest selected date minus one day. If that date would be
	 * in the past, "(earliest date MINUS current date) / 2" is calculated.
	 */
	private void fillReminderDate() {
		DateTime current = new DateTime();
		Collections.sort(dates);
		DateTime firstDate = dates.get(0);
		firstDate = firstDate.minusDays(mailNotificationHours1);

		if (firstDate.isAfter(current)) {
			reminderDate = firstDate.toDate();
		} else {
			firstDate = dates.get(0);
			long differenceMedian = (firstDate.getMillis() - current
					.getMillis()) / 2;
			reminderDate = firstDate.minus(differenceMedian).toDate();
		}
	}

	/**
	 * Returns a String of the date selected by the user, that is the earliest
	 * in the future minus 5 minutes. This is set in the datepicker to prevent
	 * people to set reminder to be sent, if the survey is already over.
	 * 
	 * @return String of the date selected by the user, that is earliest in the
	 *         future
	 */
	public String calculateMaxDate() {
		String result = "";
		Collections.sort(dates);
		DateTime maxDate = dates.get(0);
		maxDate = maxDate.minusMinutes(5);

		Locale locale = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestLocale();
		String localeString = locale.toString();
		SimpleDateFormat dateFormat;
		if (localeString.contains("de")) {
			dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		} else {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		}
		result = dateFormat.format(maxDate.toDate());

		return result;
	}

	/**
	 * Combines the SHARE_URL with the identifier to generate the full
	 * sharelink.
	 * 
	 * @return String representation of the full sharelink generated by the
	 *         scheduling process
	 */
	public String printShareLink() {
		return urlService.shareLinkBase() + sharelink;
	}

	/**
	 * Combines the Admin_URL with the identifier to generate the full
	 * adminlink.
	 * 
	 * @return String representation of the full adminlink generated by the
	 *         scheduling process
	 */
	public String printAdminLink() {
		return urlService.adminLinkBase() + adminlink;
	}

	/**
	 * Generate the full tree from groups selected by the user.
	 */
	public void updateGroupTree() {
		groupRoot = new DefaultTreeNode();
		TreeNode group;

		for (int i = 0; i < selectedGroups.size(); i++) {
			group = new DefaultTreeNode(selectedGroups.get(i), groupRoot);
			HashSet<User> users = new HashSet<User>();
			for (Grouping g : selectedGroups.get(i).getGroupings()) {
				users.add(g.getUser());
			}
			DefaultTreeNode node = new DefaultTreeNode(users, group);
			node.getChildCount(); // Does nothing, but keeps Sonar at ease.
		}
	}

	/**
	 * Delete a group from the tree.
	 * 
	 * @param g
	 *            Group to delete
	 */
	public void deleteGroupFromTree(Groups g) {
		selectedGroups.remove(g);
		updateGroupTree();
		Ajax.update("form:groups");
	}

	/**
	 * Populates the fields sharelink and adminlink with urls using the
	 * urlService.
	 */
	private void generateUrls() {
		sharelink = urlService.getGeneratedURL();
		adminlink = urlService.getGeneratedURL();
	}

	/**
	 * Readies a Meeting object to persist in the database. Transforms the data
	 * from the field dates into a appropriate form for the database. Persists
	 * the Meeting into the database.
	 * 
	 * @param userId
	 *            UserId of the current user, fetched from the database
	 */
	private void persistMeeting(int userId) {

		Meeting m = new Meeting();
		m.setDescription(description);
		m.setCreator(userId);
		m.setMinParticipants(minParticipants);
		m.setMaxParticipants(maxParticipants);
		m.setAdminLink(adminlink);
		m.setUserLink(sharelink);
		m.setLocation(location);
		m.setInvitemessage(inviteMessage);
		m.setTitle(title);
		m.setMailSend("no");

		if (location != null) {
			if (location.equals("")) {
				m.setLocation(null);
			}
		}
		if (description != null) {
			if (description.equals("")) {
				m.setDescription(null);
			}
		}
		if (inviteMessage != null) {
			if (inviteMessage.equals("")) {
				m.setInvitemessage(null);
			}
		}

		Set<TDate> convertedDates = new HashSet<TDate>();
		for (int i = 0; i < dates.size(); i++) {
			convertedDates.add(new TDate(m, dates.get(i).toDate(), "invalid"));
		}

		m.setDates(convertedDates);
		meetingService.saveMeeting(m);
	}

	/**
	 * Searches the database for the UserId identified by name and mail. The
	 * flag registered defines if the registered or anonymous users are
	 * searched. If the user is not in the database he will be registered via
	 * the method registerAnonymousUser and -1 is returned.
	 * 
	 * @param mail
	 *            Current user mail
	 * @param name
	 *            Current user name
	 * @param registered
	 *            Flag to check either the registered user (true) or the
	 *            anonymous users (false)
	 * @return UserId connected to the user identified by name and mail from the
	 *         database either from the registered or anonymous users. Return -1
	 *         if the user is not in the database.
	 */
	private int getUserId(String mail, String name, boolean registered) {
		List<UserWrapper> users = userService.getUserNameMailIds(registered);
		Integer id = null;
		for (int i = 0; i < users.size(); i++) {
			if (name.equals(users.get(i).getName())
					&& mail.equals(users.get(i).getMail())) {
				id = users.get(i).getId();
			}
		}
		if (id == null) { // User/Mail combination was not in DB
			registerAnonymousUser();
			id = -1;
		}
		return id;
	}

	/**
	 * Registers an anonymous user to the database.
	 */
	private void registerAnonymousUser() {
		User u = new User();
		u.setEmail(mail);
		u.setUserName(name);
		u.setRegistered("no");
		Locale locale = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestLocale();
		String localeString = locale.toString();
		if (localeString.contains("de")) {
			u.setLocale("de");
		} else {
			u.setLocale("en");
		}
		userService.saveUser(u);
	}

	/**
	 * Placeholder to invite all necessary people via mail
	 */
	private void inviteByMail() {

		// Search for all unique users from all selected groups
		ArrayList<User> uniqueUsers = new ArrayList<User>();
		for (int i = 0; i < selectedGroups.size(); i++) {
			for (Grouping g : selectedGroups.get(i).getGroupings()) {
				if (!uniqueUsers.contains(g.getUser())) {
					uniqueUsers.add(g.getUser());
				}
			}
		}

		StringBuilder mails = new StringBuilder();

		for (int i = 0; i < inviteMails.size(); i++) {
			if (i != 0) {
				mails.append(",");
			}
			mails.append(inviteMails.get(i));
		}

		if (inviteMessage == null) {
			inviteMessage = "";
		}

		if (!uniqueUsers.isEmpty()) {
			mailService.sendMail(uniqueUsers, "mailSchedulingInviteSubject",
					"mailSchedulingInvite", new Object[] { name, title,
							inviteMessage, printShareLink() });
		}

		if (!"".equals(mails.toString())) {
			mailService
					.sendMail(mails.toString(), "mailSchedulingInviteSubject",
							"mailSchedulingInvite", new Object[] { name, title,
									inviteMessage, printShareLink() });
		}
	}

	/**
	 * Getter method for pageStatus.
	 * 
	 * @return The current phase of the scheduling()
	 */
	public int getPageStatus() {
		return pageStatus;
	}

	/**
	 * Setter method for pageStatus. Sets the current status of the scheduling
	 * phase (0-3).
	 * 
	 * @param pageStatus
	 *            The PageStatus
	 */
	public void setPageStatus(int pageStatus) {
		this.pageStatus = pageStatus;
	}

	/**
	 * Getter method for name.
	 * 
	 * @return Username from the scheduling
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter method for name.
	 * 
	 * @param name
	 *            The Username
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter method for mail.
	 * 
	 * @return User Email
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * Setter method for mail.
	 * 
	 * @param mail
	 *            User E-Mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * Getter method for title.
	 * 
	 * @return Title of the event
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Setter method for title.
	 * 
	 * @param title
	 *            Title of the event
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Getter method for location.
	 * 
	 * @return Location of the event
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Setter method for location.
	 * 
	 * @param location
	 *            Location of the event
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Getter method for descripton.
	 * 
	 * @return Description of the Event
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter method for description.
	 * 
	 * @param description
	 *            Description of the Event
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Getter method for minParticipants.
	 * 
	 * @return Minimum of event participants
	 */
	public int getMinParticipants() {
		return minParticipants;
	}

	/**
	 * Setter method for minParticipants.
	 * 
	 * @param minParticipants
	 *            Minimum of event participants
	 */
	public void setMinParticipants(int minParticipants) {
		this.minParticipants = minParticipants;
	}

	/**
	 * Getter method for maxParticipants.
	 * 
	 * @return Maximum of event participants
	 */
	public int getMaxParticipants() {
		return maxParticipants;
	}

	/**
	 * Setter method for maxParticipants.
	 * 
	 * @param maxParticipants
	 *            Maximum of event participants
	 */
	public void setMaxParticipants(int maxParticipants) {
		this.maxParticipants = maxParticipants;
	}

	/**
	 * Getter method for inviteMails.
	 * 
	 * @return List of mails to invite directly
	 */
	public List<MailItem> getInviteMails() {
		return inviteMails;
	}

	/**
	 * Setter method for inviteMails.
	 * 
	 * @param inviteMails
	 *            List of mails to invite directly
	 */
	public void setInviteMails(List<MailItem> inviteMails) {
		this.inviteMails = inviteMails;
	}

	/**
	 * Getter method for inviteMessage.
	 * 
	 * @return Invitemessage for the invitation of the event
	 */
	public String getInviteMessage() {
		return inviteMessage;
	}

	/**
	 * Setter method for inviteMessage.
	 * 
	 * @param inviteMessage
	 *            Invitemessage for the invitation of the event
	 */
	public void setInviteMessage(String inviteMessage) {
		this.inviteMessage = inviteMessage;
	}

	/**
	 * Getter method for groups.
	 * 
	 * @return Groups in the database
	 */
	public List<Groups> getGroups() {
		return groups;
	}

	/**
	 * Setter method for groups.
	 * 
	 * @param groups
	 *            Groups in the database
	 */
	public void setGroups(List<Groups> groups) {
		this.groups = groups;
	}

	/**
	 * Getter method for timeString.
	 * 
	 * @return String representation of the current time
	 */
	public String getTimeString() {
		return timeString;
	}

	/**
	 * Setter method for timeStrings.
	 * 
	 * @param timeString
	 *            String representation of the current time
	 */
	public void setTimeString(String timeString) {
		this.timeString = timeString;
	}

	/**
	 * Getter method for dates.
	 * 
	 * @return Selected event dates
	 */
	public List<DateTime> getDates() {
		return dates;
	}

	/**
	 * Setter method for dates.
	 * 
	 * @param dates
	 *            Selected event dates
	 */
	public void setDates(List<DateTime> dates) {
		this.dates = dates;
	}

	/**
	 * Getter method for sharelink.
	 * 
	 * @return Sharelink for the event
	 */
	public String getSharelink() {
		return sharelink;
	}

	/**
	 * Setter method for sharelink.
	 * 
	 * @param sharlink
	 *            Sharelink for the event
	 */
	public void setSharelink(String sharlink) {
		this.sharelink = sharlink;
	}

	/**
	 * Getter method for adminlink.
	 *
	 * @return Adminlink for the event
	 */
	public String getAdminlink() {
		return adminlink;
	}

	/**
	 * Setter method for adminlink.
	 * 
	 * @param adminlink
	 *            Adminlink for the event
	 */
	public void setAdminlink(String adminlink) {
		this.adminlink = adminlink;
	}

	/**
	 * Getter method for selectedGroups.
	 * 
	 * @return Groups selected by the user
	 */
	public List<Groups> getSelectedGroups() {
		return selectedGroups;
	}

	/**
	 * Setter method for selectedGroups.
	 * 
	 * @param selectedGroups
	 *            Groups selected by the user
	 */
	public void setSelectedGroups(List<Groups> selectedGroups) {
		this.selectedGroups = selectedGroups;
	}

	/**
	 * Getter method for date.
	 * 
	 * @return Date selected by the user
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Setter method for registered.
	 * 
	 * @param date
	 *            Date selected by the user
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Getter method for time.
	 * 
	 * @return Time selected by the user
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * Setter method for time.
	 * 
	 * @param time
	 *            Time selected by the user
	 */
	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * Getter method for inviteMail.
	 * 
	 * @return InviteMail address entered by the user
	 */
	public String getInviteMail() {
		return inviteMail;
	}

	/**
	 * Setter method for inviteMail.
	 * 
	 * @param inviteMail
	 *            InviteMail address entered by the user
	 */
	public void setInviteMail(String inviteMail) {
		this.inviteMail = inviteMail;
	}

	/**
	 * Getter method for groupRoot.
	 * 
	 * @return Root node for the group tree
	 */
	public TreeNode getGroupRoot() {
		return groupRoot;
	}

	/**
	 * Setter method for groupRoot.
	 * 
	 * @param groupRoot
	 *            Root node for the group tree
	 */
	public void setGroupRoot(TreeNode groupRoot) {
		this.groupRoot = groupRoot;
	}

	/**
	 * Getter for the injection of LoginBean
	 * 
	 * @return LoginBean injection
	 */
	public LoginBean getLoginBean() {
		return loginBean;
	}

	/**
	 * Setter for the injection of LoginBean
	 * 
	 * @param loginBean
	 *            LoginBean injection
	 */
	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	/**
	 * Getter method for mailNotificationHours1.
	 * 
	 * @return Number of hours left on the survey before the mandatory first
	 *         reminder is sent
	 */
	public int getMailNotificationHours1() {
		return mailNotificationHours1;
	}

	/**
	 * Setter method for mailNotificationHours1.
	 * 
	 * @param mailNotificationHours1
	 *            Number of hours left on the survey before the mandatory first
	 *            reminder is sent
	 */
	public void setMailNotificationHours1(int mailNotificationHours1) {
		this.mailNotificationHours1 = mailNotificationHours1;
	}

	/**
	 * Getter method for reminderDate.
	 * 
	 * @return Date to send a reminder if no date was accepted.
	 */
	public Date getReminderDate() {
		return reminderDate;
	}

	/**
	 * Setter method for reminderDate.
	 * 
	 * @param reminderDate
	 *            Date to send a reminder if no date was accepted.
	 */
	public void setReminderDate(Date reminderDate) {
		this.reminderDate = reminderDate;
	}
}

package bean;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.Location;
import net.fortuna.ical4j.model.property.ProdId;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import ejb.MeetingEJB;
import ejb.MessageEJB;
import entity.Datelist;
import entity.Meeting;
import entity.TDate;
import entity.User;

/**
 * Receives a sharelink to search the database for the corresponding meeting. If
 * found the bean gives a rendering hint to the webpage, that displays a
 * download button. Clicking the button will create a tempfile that is a
 * calendar .ics file that is send to the user. If the download is finished the
 * tempfile is deleted.
 * 
 * @author OK/EW
 *
 */
@ManagedBean
@ViewScoped
public class ExportIcsBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Download file.
	 */
	private transient StreamedContent file;
	/**
	 * Tempfile of the .ics file.
	 */
	private transient File temp;
	/**
	 * Flag if the id was not in the database.
	 */
	private boolean idError;
	/**
	 * Flag if the meeting has a confirmed date.
	 */
	private boolean noMeetingConfirmed;
	/**
	 * Meeting to create the .ics file from.
	 */
	private Meeting meeting;
	/**
	 * The confirmed date.
	 */
	private TDate confirmedTDate;
	/**
	 * String representation of the confirmed date to be displayed in the
	 * webpage.
	 */
	private String confirmedDateString;

	/**
	 * Injection of the MeetingEJB for meeting related db usage.
	 */
	@Inject
	private transient MeetingEJB meetingService;
	/**
	 * Injection of the MessageEJB to send Facesmessages.
	 */
	@Inject
	private transient MessageEJB messageService;
	/**
	 * Logger.
	 */
	static final Logger LOGGER = Logger
			.getLogger(ExportIcsBean.class.getName());

	/**
	 * Searches the database for the meeting that is corresponding to the
	 * supplied webpage id. Sets the rendering hints for the webpage
	 * accordingly.
	 */
	@PostConstruct
	public void init() {
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> paramMap = context.getExternalContext()
				.getRequestParameterMap();
		String requestId = paramMap.get("id");
		meeting = null;
		setIdError(true);
		setNoMeetingConfirmed(true);

		if (requestId != null) {

			for (Meeting m : meetingService.getAll()) {
				if (requestId.equals(m.getUserLink())) {
					meeting = m;
					setIdError(false);
				}
			}
		}

		if (meeting != null) {
			for (TDate date : meeting.getDates()) {
				if ("valid".equals(date.getStatus())) {
					confirmedTDate = date;
					setNoMeetingConfirmed(false);
					break;
				}
			}
		}
	}

	/**
	 * Populates the field confirmedDateString with the confirmed date from the
	 * meeting.
	 */
	public void createConfirmedDateString() {
		Locale locale = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestLocale();
		String localeString = locale.toString();
		SimpleDateFormat dateFormat;
		if (localeString.contains("de")) {
			dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		} else {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		}
		confirmedDateString = dateFormat.format(confirmedTDate.getDateTime());
	}

	/**
	 * Generates the .ics file from the meeting.
	 * 
	 * @return true if the file was created. False on error
	 */
	public boolean generateIcsFile() {
		try {
			temp = File.createTempFile("meeting", ".ics");

			// Create the event
			String eventName = meeting.getTitle();
			Location location;
			Description description;
			if (meeting.getDescription() != null) {
				description = new Description(meeting.getDescription());
			} else {
				description = new Description("");
			}
			if (meeting.getLocation() != null) {
				location = new Location(meeting.getLocation());
			} else {
				location = new Location("");
			}

			DateTime start = new DateTime(confirmedTDate.getDateTime());

			VEvent meetingEvent = new VEvent(start, eventName);
			meetingEvent.getProperties().add(location);
			meetingEvent.getProperties().add(description);

			// add attending users
			List<User> attendingUsers = new LinkedList<User>();
			for (Datelist datelist : confirmedTDate.getDatelists()) {
				if ("no".equals(datelist.getWaiting())) {
					attendingUsers.add(datelist.getUser());
				}
			}

			// Create a calendar
			net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();
			icsCalendar.getProperties().add(
					new ProdId("-//Events Calendar//iCal4j 1.0//EN"));
			icsCalendar.getProperties().add(CalScale.GREGORIAN);

			// Add the event and print
			icsCalendar.getComponents().add(meetingEvent);

			BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
			bw.write(icsCalendar.toString());
			bw.close();

			file = new DefaultStreamedContent(new FileInputStream(temp),
					"text/calendar", "meeting.ics");
		} catch (IOException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
			messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
					"icsFileError");
			return false;
		}
		return true;
	}

	/**
	 * Deletes the tempfile after user finished downloading.
	 */
	public void onDlFinished() {
		if (temp != null) {
			if (temp.exists()) {
				temp.delete();
			}
		}
	}

	/**
	 * Getter method for file
	 * 
	 * @return File that is send to the user as a download
	 */
	public StreamedContent getFile() {
		return file;
	}

	/**
	 * Getter method for idError
	 * 
	 * @return True if id was wrong, false otherwise
	 */
	public boolean isIdError() {
		return idError;
	}

	/**
	 * Setter method for idError
	 * 
	 * @param idError
	 *            True if id was wrong, false otherwise
	 */
	public void setIdError(boolean idError) {
		this.idError = idError;
	}

	/**
	 * Getter method for noMeetingConfirmed.
	 * 
	 * @return true if no no meeting is confirmed, false otherwise.
	 */
	public boolean isNoMeetingConfirmed() {
		return noMeetingConfirmed;
	}

	/**
	 * Getter method for noMeetingConfirmed.
	 * 
	 * @param noMeetingConfirmed
	 *            true if no no meeting is confirmed, false otherwise.
	 */
	public void setNoMeetingConfirmed(boolean noMeetingConfirmed) {
		this.noMeetingConfirmed = noMeetingConfirmed;
	}

	/**
	 * Getter method for confirmedDateString.
	 * 
	 * @return String representation of the confirmed date to be displayed in
	 *         the webpage.
	 */
	public String getConfirmedDateString() {
		createConfirmedDateString();
		return confirmedDateString;
	}

}

package bean;

import java.util.List;
import java.util.Locale;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import util.HashTool;
import ejb.UserEJB;
import entity.User;

/**
 * Used by register.xhtml
 * 
 * @author AH
 */

@Named(value = "registerBean")
@ApplicationScoped
public class RegisterBean {
	private String username;
	private String email;
	private String password;
	private String registered;
	// private String role;

	/**
	 * Injection of the UserEJB for User related DB usage.
	 */
	@Inject
	private UserEJB userService;
	
	public UserEJB getUserEJB() {
		return userService;
	}
	public void setUserEJB(UserEJB userService) {
		this.userService = userService;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public String getRegistered() {
		return registered;
	}
	public void setRegistered(String registered) {
		this.registered = registered;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	/*
	 * public String getRole() { return role; } public void setRole(String role)
	 * { this.role = role; }
	 */

	public List<User> getAllUsers() {
		return userService.getAll();
	}

	public String submit() {
		User user = new User();
		user.setEmail(email);
		user.setUserName(username);
		// SHA-256 hashed password
		user.setPassword(HashTool.createSHA256Hash(password));
		user.setRegistered("yes");
		user.setRole("user");
		Locale locale = FacesContext.getCurrentInstance().getExternalContext().getRequestLocale();
		if(locale.toString().contains("de")) 
			user.setLocale("de");
		else 
			user.setLocale("en");
		userService.saveUser(user);
		return "login";
	}

}
package bean;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import model.EditBeanDateWrapper;

import org.joda.time.DateTime;

import util.DatelistWaittimeComparator;
import util.TDateComparator;
import util.WaitlistComparator;
import ejb.DateEJB;
import ejb.DateEvaluationEJB;
import ejb.DatelistEJB;
import ejb.MailEJB;
import ejb.MeetingEJB;
import ejb.MessageEJB;
import ejb.UrlEJB;
import ejb.UserEJB;
import ejb.WaitlistEJB;
import entity.Datelist;
import entity.Meeting;
import entity.TDate;
import entity.User;
import entity.Waitlist;

/**
 * Backing bean for the webpage editEvent. Fetches all necessary parameters to
 * alter an Event based on the given id paramter of the requestURL. Handles
 * updating the changed fields in the DB. Also allows manual waitlist merging
 * and handles the confirmation of a date.
 * 
 * 
 * @author OK
 *
 */
@ManagedBean
@ViewScoped
public class EditEventBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Minimum number of participants for the meeting to take place.
	 */
	private int minParticipants;
	/**
	 * Maximum number of participants.
	 */
	private int maxParticipants;
	/**
	 * Title of the meeting.
	 */
	private String title;
	/**
	 * Location of the meeting.
	 */
	private String location;
	/**
	 * Detailed explanation of the meeting.
	 */
	private String description;
	/**
	 * Name of the current user.
	 */
	private String name;
	/**
	 * Username's E-mail.
	 */
	private String mail;
	/**
	 * Selected dates from the user.
	 */
	private transient List<EditBeanDateWrapper> dates;
	/**
	 * String from the current serverdate to restrict the calenderpicker in the
	 * webpage to only display future dates.
	 */
	private String timeString;
	/**
	 * Link for users to participate in the poll.
	 */
	private String sharelink;
	/**
	 * Link for the current user to edit the meeting information afterwards.
	 */
	private String adminlink;
	/**
	 * Flag to tell if it is an anonymous or a registered user.
	 */
	private boolean registered;
	/**
	 * Date to user selects.
	 */
	private Date date;
	/**
	 * Time the user selects.
	 */
	private Date time;
	/**
	 * The meeting identified by the requestUrl.
	 */
	private Meeting meeting;
	/**
	 * Part of the requestUrl that is used to find the meeting in the database.
	 */
	private String requestId;
	/**
	 * Flag if id in the requestUrl was in the database.
	 */
	private boolean idError;
	/**
	 * Flag if the settings were edited.
	 */
	private boolean settingsEdited;
	/**
	 * List representation of the Dateset in the Meeting.
	 */
	private transient List<TDate> dateList;
	/**
	 * Date from the Dateset in Meeting that are to be deleted after updating
	 * the date.
	 */
	private transient List<TDate> datesToRemove;
	/**
	 * List of Users waiting on the meeting.
	 */
	private transient List<User> waitlistUsers;
	/**
	 * List with dates that have at least as much as min participants users
	 * attached to them. Therefore these dates are eligible to be confirmed as
	 * the resulting date for the meeting by the meetingadmin.
	 */
	private transient List<TDate> confirmDateList;
	/**
	 * Date to be confirmed by the user.
	 */
	private Date confirmedDate;
	/**
	 * TDate to be confirmed by the user.
	 */
	private TDate confirmedTDate;

	/**
	 * Injection of the MeetingEJB for Meeting related DB usage.
	 */
	@Inject
	private MeetingEJB meetingService;

	/**
	 * Injection of the MeetingEJB for Date related DB usage.
	 */
	@Inject
	private DateEJB dateService;

	/**
	 * Injection of the MessageEJB for Facesmessages.
	 */
	@Inject
	private MessageEJB messageService;
	/**
	 * Injection of the WaitlistEJB for waitlist related DB usage.
	 */
	@Inject
	private WaitlistEJB waitlistService;
	/**
	 * Injection of the datelistEJB for datelist related DB usage.
	 */
	@Inject
	private DatelistEJB datelistService;
	/**
	 * Injection of the DateEvaluationEJB to find the most efficient Dates.
	 */
	@Inject
	private transient DateEvaluationEJB dateEvaluationService;
	/**
	 * Injection of the MailEJB to send E-Mails.
	 */
	@Inject
	private transient MailEJB mailService;
	/**
	 * Injection of the UserEJB for User DB related usage.
	 */
	@Inject
	private UserEJB userService;
	/**
	 * Inection of the UrlEJB for sharelink related usage.
	 */
	@Inject
	private UrlEJB urlService;

	/**
	 * General bean operation setup. Fetches the id parameter of the requestURL
	 * and searches the DB for the matching adminlink. Sets the idError flag to
	 * render the webpage with the requested meeting or an error.
	 */
	@PostConstruct
	public void init() {
		// General load from webpage id and setting up first tab
		FacesContext context = FacesContext.getCurrentInstance();
		Map<String, String> paramMap = context.getExternalContext()
				.getRequestParameterMap();
		requestId = paramMap.get("id");
		dateList = new ArrayList<TDate>();
		datesToRemove = new ArrayList<TDate>();
		settingsEdited = false;

		if (requestId != null) {
			for (Meeting m : meetingService.getAll()) {
				if (m.getAdminLink().equals(requestId)) {
					this.meeting = m;
				}
			}
		}

		if (meeting != null) {

			this.title = meeting.getTitle();
			this.location = meeting.getLocation();
			if (location == null) {
				location = "";
			}
			this.description = meeting.getDescription();
			if (description == null) {
				description = "";
			}
			this.minParticipants = meeting.getMinParticipants();
			this.maxParticipants = meeting.getMaxParticipants();

			Set<TDate> utilDates = meeting.getDates();
			dates = new ArrayList<EditBeanDateWrapper>();
			for (TDate t : utilDates) {
				int partUsers = 0;
				for (Datelist datelist : t.getDatelists()) {
					if ("no".equals(datelist.getWaiting())) {
						partUsers++;
					}
				}
				dates.add(new EditBeanDateWrapper(
						new DateTime(t.getDateTime()), partUsers));
			}
			dateList.addAll(meeting.getDates());
			Collections.sort(dates);
			Collections.sort(dateList, new TDateComparator());

			createTimeString();
			setIdError(false);

			// Setting up tab 2
			waitlistUsers = new ArrayList<User>();
			for (TDate date : meeting.getDates()) {
				for (Datelist datelist : date.getDatelists()) {
					if (datelist.getWaiting().equalsIgnoreCase("yes")
							&& !waitlistUsers.contains(datelist.getUser())) {
						waitlistUsers.add(datelist.getUser());
					}
				}
			}

			// Setting up tab 3
			confirmDateList = dateEvaluationService
					.getMostEfficientDates(meeting);

			for (TDate t : dateList) {
				if (t.getDatelists().size() >= minParticipants) {
					if ("valid".equals(t.getStatus())) {
						confirmedTDate = t;
						confirmedDate = confirmedTDate.getDateTime();
						break;
					}
				}
			}
		} else {
			setIdError(true);
		}
	}

	/**
	 * Called when something is edited to enable the submit button
	 */
	public void editListener() {
		settingsEdited = true;
	}

	/**
	 * Triggers the method updateMeeting to update the Meeting with the new
	 * information in the DB and throws a facesMessage. If the creator lowers
	 * the minimum participants and the new number would be enough for the date
	 * to be confirmable, then a mail is sent to the creator.
	 * 
	 * @return Returns a Facesmessage if everthing was ok and saved. Returns a
	 *         Facesmessage with an error instead, if the survey has changed
	 *         while editing and the new min/max participants would be below
	 *         actual Participants.
	 */
	public String submit() {
		if (requestId != null) {
			Meeting actualDBMeeting = new Meeting();
			for (Meeting m : meetingService.getAll()) {
				if (m.getAdminLink().equals(requestId)) {
					actualDBMeeting = m;
				}
			}

			if (minParticipants < 1
					|| maxParticipants < actualDBMeeting.getUsers().size()) {
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"editValidationChange");
				return "";
			}

			int oldMin = meeting.getMinParticipants();
			int oldSize = meeting.getUsers().size();

			updateMeeting();

			if (maxParticipants > actualDBMeeting.getMaxParticipants()) {
				autoMergeUsersFromWaitlist();
			}

			// Check if the date admin needs to informed about a possible date
			// If checks: No date confirmed; min was lowered; current users
			// greater
			// than or equals min.
			if (!"yes".equals(meeting.getMailSend())) {
				if (minParticipants < oldMin) {
					if (oldSize >= minParticipants) {
						System.out.println("min!");
						User creator = userService.findById(meeting
								.getCreator());
						List<TDate> mostEfficientDates = dateEvaluationService
								.getMostEfficientDates(meeting);
						StringBuilder mostEfficientDatesString = new StringBuilder();
						for (int i = 0; i < mostEfficientDates.size(); i++) {
							if ("de".equals(creator.getLocale())) {
								mostEfficientDatesString
										.append(createDateString(
												mostEfficientDates.get(i), "de"));
								mostEfficientDatesString.append("\n");
							} else {
								mostEfficientDatesString
										.append(createDateString(
												mostEfficientDates.get(i), "en"));
								mostEfficientDatesString.append("\n");
							}

						}

						if (!"".equals(mostEfficientDatesString.toString())) {
							mailService
									.sendMail(
											creator.getEmail(),
											"mailMinParticipantsReachedSubject",
											"mailMinParticipantsReached",
											new Object[] {
													actualDBMeeting.getTitle(),
													mostEfficientDatesString
															.toString(),
													urlService.adminLinkBase()
															+ actualDBMeeting
																	.getAdminLink() });
							meeting.setMailSend("yes");
							meetingService.updateMeeting(meeting);
						}
					}
				}
			}
			init();
		}
		return "";
	}

	/**
	 * Populates the field timeString with the current serverdate for the
	 * webpage Datepicker.
	 */
	public void createTimeString() {
		Calendar c = Calendar.getInstance();
		// c.add(Calendar.DATE, 1); //This line makes the first possible date
		// tomorrow, starting from the current server date
		Locale locale = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestLocale();
		String localeString = locale.toString();
		SimpleDateFormat dateFormat;
		if (localeString.contains("de")) {
			dateFormat = new SimpleDateFormat("dd.MM.yyyy");
		} else {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		}
		timeString = dateFormat.format(c.getTime());
	}

	/**
	 * Add a new DateTime to the field dates to be represented in the webpage.
	 * Also adds a new TDate to the dateset in meeting
	 */
	public void addDateItem() {
		DateTime dateToAdd = new DateTime(date);
		DateTime timeOnly = new DateTime(time);

		dateToAdd = dateToAdd.plusHours(timeOnly.getHourOfDay());
		dateToAdd = dateToAdd.plusMinutes(timeOnly.getMinuteOfHour());
		if (date != null && time != null) {
			boolean check = dateTimeValidation(dateToAdd);
			if (check) {
				dates.add(new EditBeanDateWrapper(dateToAdd, 0));
				Collections.sort(dates);
				TDate t = new TDate(meeting, dateToAdd.toDate(), "invalid");
				dateList.add(t);
				Collections.sort(dateList, new TDateComparator());
				meeting.getDates().add(t);
				editListener();
			}
		} else {
			if (date == null) { // duplicate Date
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"scheduleValidationDateEmpty");
			}

			if (time == null) {
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"scheduleValidationTimeEmpty");
			}
		}
	}

	/**
	 * Delete DateTime d from the field dates. Also removes the date from the
	 * dateset in Meeting.
	 * 
	 * @param d
	 *            DateTime to remove
	 */
	public void removeDateItem(EditBeanDateWrapper d) {
		int index = dates.indexOf(d);
		dates.remove(d);
		meeting.getDates().remove(dateList.get(index));
		datesToRemove.add(dateList.get(index));
		dateList.remove(index);
		editListener();
	}

	/**
	 * Used to check if the delete Button in the webpage should be disabled or
	 * not. If the specified Wrapper belongs to the date that is confirmed, the
	 * method returns true. False otherwise.
	 * 
	 * @param d
	 *            Wrapper to check if the wrapped date is confirmed
	 * @return True if the date wrapped by the Wrapper is confirmed. False
	 *         otherwise.
	 */
	public boolean checkConfirmedDateFromDateWrapper(EditBeanDateWrapper d) {
		int index = dates.indexOf(d);
		if (dateList.get(index).getStatus().equals("valid")) {
			return true;
		}
		return false;
	}

	/**
	 * Validates d to the following criteria: 1. Date with Time has to be unique
	 * 2. Date has to be in the Future
	 * 
	 * @param d
	 *            datetime to check
	 * 
	 * @return true if d got validated correctly(1&2 true), false otherwise.
	 * 
	 */
	public boolean dateTimeValidation(DateTime d) {
		DateTime t = new DateTime();

		for (int i = 0; i < dates.size(); i++) {
			if (dates.get(i).getDate().equals(d)) {
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"scheduleValidationTime");
				return false;
			}
		}

		if (d.isBefore(t)) {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
					"scheduleValidationDate");
			return false;
		}

		return true;
	}

	/**
	 * Returns the number of users participating in the meeting survey. This is
	 * primarily used for validation purposes(min/max participants) in the
	 * webpage. If no user is participating the minimum of 1 is returned.
	 * 
	 * @return Number of users participating in the meeting survey. 1 if this
	 *         number would be zero.
	 */
	public int participatingUsers() {
		if (meeting != null) {
			return meeting.getUsers().size() == 0 ? 1 : meeting.getUsers()
					.size();
		}
		return 1;
	}

	/**
	 * Automerge users from waitlist if maxPartipants is raised. The waiting
	 * users join in a FIFO manner. Users receive mails, that they were merged.
	 */
	public void autoMergeUsersFromWaitlist() {
		updateWaitingParticipants();

		messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
				"editAutoMergeWaitlist");
	}

	/**
	 * Removes the specified user from the waitlist and merges the user to the
	 * meeting. User receives a mail.
	 * 
	 * @param user
	 *            User to be merged
	 * @param byHand
	 *            True if single merge from webpage, false if automerge from
	 *            bean.
	 */
	public void mergeUserFromWaitlist(User user, boolean byHand) {
		for (TDate date : meeting.getDates()) {
			for (Datelist datelist : date.getDatelists()) {
				if (datelist.getUser().equals(user)) {
					datelist.setWaiting("no");
					datelistService.updateDatelist(datelist);
				}
			}
		}

		init();
		if (meeting != null) {
			if (!meeting.getUsers().contains(user)) {
				meeting.getUsers().add(user);
			}
			if (participatingUsers() + 1 > maxParticipants && byHand) {
				meeting.setMaxParticipants(meeting.getMaxParticipants() + 1);
			}
			meetingService.updateMeeting(meeting);

			if (byHand) {
				messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
						"editWaitlistOK");
			}

			String sharelink = urlService.shareLinkBase()
					+ meeting.getUserLink();

			String icslink = urlService.icsLinkBase() + meeting.getUserLink();
			User creator = userService.findById(meeting.getCreator());

			if (!checkForConfirmedDate()) {
				mailService.sendMail(user.getEmail(),
						"mailWaitlistRemovedSubject", "mailWaitlistRemoved",
						new Object[] { sharelink });
			} else {
				mailService.sendMail(user.getEmail(),
						"mailMeetingConfirmedUserSubject",
						"mailMeetingConfirmedUser", new Object[] { sharelink,
								title, location, creator.getUserName(), date,
								icslink });
			}

			updateWaitingParticipants();
		}
	}

	/**
	 * Set the specified dates status to valid. Persist that in the database and
	 * send confirmation mails to all users participating on that date.
	 * 
	 */
	public void validateTDate() {
		confirmedTDate.setStatus("valid");
		dateService.updateDate(confirmedTDate);
		int index = confirmDateList.indexOf(confirmedTDate);
		if (index != -1) {
			confirmDateList.get(index).setStatus("valid");
		}
		// send mails
		User creator = userService.findById(meeting.getCreator());
		String title = meeting.getTitle();
		String location = meeting.getLocation();
		if (location == null) {
			location = "";
		}
		String date = createConfirmedDateString();
		String sharelink = urlService.shareLinkBase() + meeting.getUserLink();
		String icslink = urlService.icsLinkBase() + meeting.getUserLink();

		mailService.sendMail(creator.getEmail(),
				"mailMeetingConfirmedCreatorSubject",
				"mailMeetingConfirmedCreator", new Object[] { date, title,
						location, icslink });

		List<User> users = new ArrayList<User>();
		for (Datelist datelist : confirmedTDate.getDatelists()) {
			users.add(datelist.getUser());
		}

		mailService.sendMail(users, "mailMeetingConfirmedUserSubject",
				"mailMeetingConfirmedUser", new Object[] { sharelink, title,
						location, creator.getUserName(), date, icslink });

		// disable reminder
		mailService.cancelTimedMail(meeting.getUserLink());
	}

	/**
	 * Set the specified dates status to invalid. Persist that in the database
	 * and send date cancel mails to all users participating users on that date.
	 * 
	 * @param date
	 *            Date to cancel
	 */
	public void invalidateTDate() {
		confirmedTDate.setStatus("invalid");
		dateService.updateDate(confirmedTDate);
		int index = confirmDateList.indexOf(confirmedTDate);
		if (index != -1) {
			confirmDateList.get(index).setStatus("invalid");
		}
		// send mails
		User creator = userService.findById(meeting.getCreator());
		String title = meeting.getTitle();
		String location = meeting.getLocation();
		if (location == null) {
			location = "";
		}
		String date = createConfirmedDateString();
		String sharelink = urlService.shareLinkBase() + meeting.getUserLink();

		mailService.sendMail(creator.getEmail(),
				"mailMeetingCancelledCreatorSubject",
				"mailMeetingCancelledCreator", new Object[] { sharelink, date,
						title, location });

		List<User> users = new ArrayList<User>();
		for (Datelist datelist : confirmedTDate.getDatelists()) {
			users.add(datelist.getUser());
		}

		mailService.sendMail(users, "mailMeetingCancelledUserSubject",
				"mailMeetingCancelledUser",
				new Object[] { sharelink, creator.getUserName(), date, title,
						location });

	}

	/**
	 * Get all users that participate the specified date.
	 * 
	 * @param date
	 *            Date to get users from
	 * @return List with users that participate on the specified date
	 */
	public List<User> getParticipatingUsersFromDate(TDate date) {
		List<User> users = new ArrayList<User>();
		for (Datelist datelist : date.getDatelists()) {
			if ("no".equals(datelist.getWaiting())) {
				users.add(datelist.getUser());
			}
		}
		return users;
	}

	/**
	 * Checks all dates that could be confirmed by the creator to see if one of
	 * them was already confirmed. Returns true if a confirmed date exists,
	 * false otherwise.
	 * 
	 * @return true if a meeting was already confirmed. False otherwise
	 */
	public boolean checkForConfirmedDate() {
		for(TDate date : meeting.getDates()){
			if (date.getStatus().equals("valid")) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the status of the specified date is valid or invalid. Returns
	 * true if valid, false otherwise.
	 * 
	 * @param date
	 *            Date to check the status from
	 * @return True if date status is valid, false otherwise
	 */
	public boolean checkTDateStatus(TDate date) {
		if (date.getStatus().equals("valid")) {
			return true;
		} else if (date.getStatus().equals("invalid")) {
			return false;
		} else {
			return false; // This shouldn't happen....
		}
	}

	/**
	 * Returns the specified t as a DateTime.
	 * 
	 * @param t
	 *            TDate to be converted to DateTime
	 * @return DateTime representation of t
	 */
	public DateTime convertTDateToDateTime(TDate date) {
		return new DateTime(date.getDateTime());
	}

	/**
	 * Create a String from confirmedDate to set the maxDate for the datepicker
	 * in the confirm dialog.
	 * 
	 * @return String representation of confirmedDate to set maxDate in the
	 *         datepicker
	 */
	public String createConfirmedDateString() {
		String result = "";

		if (confirmedDate != null) {
			Locale locale = FacesContext.getCurrentInstance()
					.getExternalContext().getRequestLocale();
			String localeString = locale.toString();
			SimpleDateFormat dateFormat;
			if (localeString.contains("de")) {
				dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
			} else {
				dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			}
			DateTime t = new DateTime(confirmedDate);
			t = t.minusMinutes(5);
			result = dateFormat.format(t.toDate());
		}
		return result;
	}

	/**
	 * Create a localized string of the specified date. Includes hours and
	 * minutes. Supported locales are "de" and "en".
	 * 
	 * @param date
	 *            date to generate the string
	 * @param locale
	 *            locale of the generated string
	 * @return Localized string representation of the spezified date
	 */
	public String createDateString(TDate date, String locale) {
		String res = "";
		SimpleDateFormat dateFormat;
		if ("de".equals(locale)) {
			dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		} else {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		}

		res = dateFormat.format(date.getDateTime());
		return res;
	}

	/**
	 * Method to be called by the radiogroup in the webpage. Updates the
	 * DateTime representation of the date to be confirmed.
	 */
	public void radioValueChange() {
		confirmedDate = confirmedTDate.getDateTime();
	}

	/**
	 * Calculate earliest Date for the datepicker for the invitation date.
	 */
	public String calculateMinDate() {
		Locale locale = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestLocale();
		String localeString = locale.toString();
		SimpleDateFormat dateFormat;
		if (localeString.contains("de")) {
			dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		} else {
			dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		}

		return dateFormat.format(new Date());
	}

	/**
	 * Fill confirmedDate with the specified date.
	 * 
	 * @param d
	 *            TDate to be the confirmed Date
	 */
	public void fillConfirmedDate(TDate d) {
		confirmedDate = d.getDateTime();
		confirmedTDate = d;
	}

	/**
	 * Updates the Meeting in the DB. Deletes datesToRemove from the DB
	 * afterwards. If a Date was deleted in the webpage and made anew the exact
	 * same way afterwards, the old date is retrieved from datesToRemove to be
	 * more consistent with the DB. For Example, the old date was valid, a newly
	 * made will always be invalid.
	 */
	private void updateMeeting() {
		meeting.setTitle(title);
		meeting.setLocation(location);
		meeting.setDescription(description);
		meeting.setMinParticipants(minParticipants);
		meeting.setMaxParticipants(maxParticipants);

		// reattach dates if they got made anew to save consistency -> MON 10:00
		// got deleted and newly registered. Take the old one.

		for (int i = 0; i < datesToRemove.size(); i++) {
			for (int j = 0; j < dateList.size(); j++) {
				if (datesToRemove.get(i).getDateTime().getTime() == dateList
						.get(j).getDateTime().getTime()) {
					meeting.getDates().remove(dateList.get(j));
					meeting.getDates().add(datesToRemove.get(i));
					datesToRemove.set(i, null);
					j = dateList.size();
				}
			}
		}

		meetingService.updateMeeting(meeting);
		deleteOldDatesFromDB();
		messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
				"editSuccess");
	}

	/**
	 * Removes datesToRemove from the DB.
	 */
	private void deleteOldDatesFromDB() {
		for (int i = 0; i < datesToRemove.size(); i++) {
			if (datesToRemove.get(i) != null) {
				dateService.remove(datesToRemove.get(i));
			}
		}
		datesToRemove.clear();
	}

	/**
	 * Checks if the specified Date reached its max participants.
	 * 
	 * @param date
	 *            Date to check max participants
	 * @return true if max participants reacher, false otherwise.
	 */
	private boolean maxParticipantsReached(TDate date) {
		int counter = 0;
		for (Datelist datelist : date.getDatelists()) {
			if (!datelist.getWaiting().equalsIgnoreCase("yes"))
				counter++;
		}

		if (counter >= meeting.getMaxParticipants())
			return true;

		return false;
	}

	private void updateWaitingParticipants() {
		LinkedList<Datelist> datelistsToChange = new LinkedList<Datelist>();
		for (TDate date : meeting.getDates()) {
			if (!maxParticipantsReached(date))
				for (Datelist datelist : date.getDatelists()) {
					if (datelist.getWaiting().equalsIgnoreCase("yes"))
						datelistsToChange.add(datelist);
				}
		}

		Collections.sort(datelistsToChange, new DatelistWaittimeComparator());
		if (!datelistsToChange.isEmpty()) {
			User userToChange = datelistsToChange.getFirst().getUser();
			String sharelink = urlService.shareLinkBase()
					+ meeting.getUserLink();

			String icslink = urlService.icsLinkBase() + meeting.getUserLink();
			User creator = userService.findById(meeting.getCreator());

			if (!checkForConfirmedDate()) {
				mailService.sendMail(userToChange.getEmail(),
						"mailWaitlistRemovedSubject", "mailWaitlistRemoved",
						new Object[] { sharelink });
			} else {
				mailService.sendMail(userToChange.getEmail(),
						"mailMeetingConfirmedUserSubject",
						"mailMeetingConfirmedUser", new Object[] { sharelink,
								title, location, creator.getUserName(), date,
								icslink });
			}

			if (!meeting.getUsers().contains(userToChange))
				meeting.getUsers().add(userToChange);

			for (Datelist datelist : datelistsToChange) {
				if (datelist.getUser().equals(userToChange)) {
					datelist.setWaiting("no");
					datelistService.updateDatelist(datelist);
				}
			}
			meetingService.updateMeeting(meeting);
		}

		LinkedList<Datelist> datelistsToChange2 = new LinkedList<Datelist>();
		for (TDate date : meeting.getDates()) {
			if (!maxParticipantsReached(date))
				for (Datelist datelist : date.getDatelists()) {
					if (datelist.getWaiting().equalsIgnoreCase("yes"))
						datelistsToChange2.add(datelist);
				}
		}
		if (datelistsToChange2.size() != datelistsToChange.size())
			updateWaitingParticipants();
	}

	/**
	 * Getter method for name.
	 * 
	 * @return Username from the scheduling
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter method for name.
	 * 
	 * @param name
	 *            The Username
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter method for mail.
	 * 
	 * @return User Email
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * Setter method for mail.
	 * 
	 * @param mail
	 *            User E-Mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * Getter method for title.
	 * 
	 * @return Title of the event
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Setter method for title.
	 * 
	 * @param title
	 *            Title of the event
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Getter method for location.
	 * 
	 * @return Location of the event
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * Setter method for location.
	 * 
	 * @param location
	 *            Location of the event
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Getter method for description.
	 * 
	 * @return Description of the Event
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter method for description.
	 * 
	 * @param description
	 *            Description of the Event
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Getter method for minParticipants.
	 * 
	 * @return Minimum of event participants
	 */
	public int getMinParticipants() {
		return minParticipants;
	}

	/**
	 * Setter method for minParticipants.
	 * 
	 * @param minParticipants
	 *            Minimum of event participants
	 */
	public void setMinParticipants(int minParticipants) {
		this.minParticipants = minParticipants;
	}

	/**
	 * Getter method for maxParticipants.
	 * 
	 * @return Maximum of event participants
	 */
	public int getMaxParticipants() {
		return maxParticipants;
	}

	/**
	 * Setter method for maxParticipants.
	 * 
	 * @param maxParticipants
	 *            Maximum of event participants
	 */
	public void setMaxParticipants(int maxParticipants) {
		this.maxParticipants = maxParticipants;
	}

	/**
	 * Getter method for timeString.
	 * 
	 * @return String representation of the current time
	 */
	public String getTimeString() {
		return timeString;
	}

	/**
	 * Setter method for timeStrings.
	 * 
	 * @param timeString
	 *            String representation of the current time
	 */
	public void setTimeString(String timeString) {
		this.timeString = timeString;
	}

	/**
	 * Getter method for dates.
	 * 
	 * @return Selected event dates
	 */
	public List<EditBeanDateWrapper> getDates() {
		return dates;
	}

	/**
	 * Setter method for dates.
	 * 
	 * @param dates
	 *            Selected event dates
	 */
	public void setDates(List<EditBeanDateWrapper> dates) {
		this.dates = dates;
	}

	/**
	 * Getter method for sharelink.
	 * 
	 * @return Sharelink for the event
	 */
	public String getSharelink() {
		return sharelink;
	}

	/**
	 * Setter method for sharelink.
	 * 
	 * @param sharlink
	 *            Sharelink for the event
	 */
	public void setSharelink(String sharlink) {
		this.sharelink = sharlink;
	}

	/**
	 * Getter method for adminlink.
	 *
	 * @return Adminlink for the event
	 */
	public String getAdminlink() {
		return adminlink;
	}

	/**
	 * Setter method for adminlink.
	 * 
	 * @param adminlink
	 *            Adminlink for the event
	 */
	public void setAdminlink(String adminlink) {
		this.adminlink = adminlink;
	}

	/**
	 * Getter method for registered.
	 * 
	 * @return True if the user is currently logged in. False otherwise
	 */
	public boolean isRegistered() {
		return registered;
	}

	/**
	 * Setter method for registered.
	 * 
	 * @param registered
	 *            True if the user is currently logged in. False otherwise
	 */
	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	/**
	 * Getter method for date.
	 * 
	 * @return Date selected by the user
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Setter method for registered.
	 * 
	 * @param date
	 *            Date selected by the user
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Getter method for time.
	 * 
	 * @return Time selected by the user
	 */
	public Date getTime() {
		return time;
	}

	/**
	 * Setter method for time.
	 * 
	 * @param time
	 *            Time selected by the user
	 */
	public void setTime(Date time) {
		this.time = time;
	}

	/**
	 * Getter Method for idError. Flag if the requested URL id parameter was
	 * found in the DB. Rendering Hint for the webpage
	 * 
	 * @return True = not found. False = found.
	 */
	public boolean isIdError() {
		return idError;
	}

	/**
	 * Setter method for idError.
	 * 
	 * @param idError
	 *            Flag if the requested URL id parameter was found in the DB.
	 *            Also a rendering hint for the webpage. true = not found.
	 */
	public void setIdError(boolean idError) {
		this.idError = idError;
	}

	/**
	 * Getter method for meeting.
	 * 
	 * @return Meeting that was loaded from the database via the given webpage
	 *         id attribute
	 */
	public Meeting getMeeting() {
		return meeting;
	}

	/**
	 * Setter method for meeting.
	 * 
	 * @param meeting
	 *            Meeting that was loaded from the database via the given
	 *            webpage id attribute
	 */
	public void setMeeting(Meeting meeting) {
		this.meeting = meeting;
	}

	/**
	 * Getter method for waitListUsers.
	 * 
	 * @return List of Users waiting on the meeting
	 */
	public List<User> getWaitListUsers() {
		return waitlistUsers;
	}

	/**
	 * Setter method for waitListUsers.
	 * 
	 * @param waitListUsers
	 *            List of Users waiting on the meeting.
	 */
	public void setWaitListUsers(List<User> waitListUsers) {
		this.waitlistUsers = waitListUsers;
	}

	/**
	 * Getter method for confirmDateList.
	 * 
	 * @return List with dates that are eligible to be confirmed as the
	 *         resulting date for the meeting by the meetingadmin.
	 */
	public List<TDate> getConfirmDateList() {
		return confirmDateList;
	}

	/**
	 * Setter method for confirmDateList.
	 * 
	 * @param confirmDate
	 *            List are eligible to be confirmed as the resulting date for
	 *            the meeting by the meetingadmin.
	 */
	public void setConfirmDateList(List<TDate> confirmDateList) {
		this.confirmDateList = confirmDateList;
	}

	/**
	 * Getter method for confirmedTDate.
	 * 
	 * @return TDate to be confirmed by the user.
	 */
	public TDate getConfirmedTDate() {
		return confirmedTDate;
	}

	/**
	 * Setter method for confirmedTDate.
	 * 
	 * @param confirmedTDate
	 *            TDate to be confirmed by the user.
	 */
	public void setConfirmedTDate(TDate confirmedTDate) {
		this.confirmedTDate = confirmedTDate;
	}

	/**
	 * Getter method for settingsEdited
	 * 
	 * @return Flag if the settings were edited.
	 */
	public boolean isSettingsEdited() {
		return settingsEdited;
	}

	/**
	 * Setter method for settingsEdited
	 * 
	 * @param settingsEdited
	 *            Flag if the settings were edited.
	 */
	public void setSettingsEdited(boolean settingsEdited) {
		this.settingsEdited = settingsEdited;
	}
}

package bean;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import entity.User;

/**
 * Used by login.xhtml
 * 
 * @author AH
 */

@Named(value = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String username;
	private String password;
	private String email;

	private boolean loggedIn;

	private User user = new User();

	/**
	 * Sets loggedIn to false. Invalidate the current user session and return to
	 * the Startpage.
	 * 
	 * @return Redirects the user to index.xhtml
	 */
	public String logout() {
		loggedIn = false;
		user = new User();
		HttpSession session = ((HttpSession) FacesContext.getCurrentInstance()
				.getExternalContext().getSession(true));
		session.invalidate();
		return "index";
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public String redirectToLogin() {
		return "/login.xhtml?faces-redirect=true";
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}

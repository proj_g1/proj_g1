package bean;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.joda.time.DateTime;

import util.DatelistWaittimeComparator;
import util.TDateComparator;
import util.UserComparator;
import util.WaitlistComparator;
import ejb.DateEJB;
import ejb.DateEvaluationEJB;
import ejb.DatelistEJB;
import ejb.MailEJB;
import ejb.MeetingEJB;
import ejb.UrlEJB;
import ejb.UserEJB;
import ejb.WaitlistEJB;
import entity.Datelist;
import entity.DatelistId;
import entity.Meeting;
import entity.TDate;
import entity.User;
import entity.Waitlist;

/**
 * Backing Bean for the survey.xhtml page. Collects all data from the Meeting.
 * Adds an removes Users from Users
 * 
 * @author EW
 */
@ManagedBean
@ViewScoped
public class MeetingBean {

	/**
	 * Injection of MeetingEJB for Database connection
	 */
	@Inject
	MeetingEJB meetingEJB;

	/**
	 * Injection of UserEJB for Database connection
	 */
	@Inject
	UserEJB userEJB;

	/**
	 * Injection of DatelistEJB for Database connection
	 */
	@Inject
	DatelistEJB datelistEJB;

	/**
	 * Injection of DateEJB for Database connection
	 */
	@Inject
	DateEJB dateEJB;

	/**
	 * Injection of WaitlistEJB for Database connection
	 */
	@Inject
	WaitlistEJB waitlistEJB;

	/**
	 * Injection of DateEvaluationEJB for DateEvaluation
	 */
	@Inject
	DateEvaluationEJB dateEvaluationEJB;

	/**
	 * Injection of LoginBean to manage registered users
	 */
	@Inject
	LoginBean loginBean;

	@Inject
	MailEJB mailEJB;

	@Inject
	UrlEJB urlEJB;

	/**
	 * specified meeting
	 */
	private Meeting meeting = null;

	static final Logger LOGGER = Logger.getLogger(MeetingBean.class.getName());

	/**
	 * Dummy-User to add a new user
	 */
	private User user;

	/**
	 * sorted userlist with all participants of the meetingwho are not on the
	 * waitlist
	 */
	private LinkedList<User> users;

	/**
	 * stores which user has accepted a to which dates
	 */
	private HashMap<User, HashMap<TDate, Boolean>> userDates;

	private HashMap<User, HashMap<TDate, Boolean>> userDatesWaitlist;

	/**
	 * Map to count how many users has accepted the specified dates
	 */
	private HashMap<TDate, Integer> numberOfParticipants;

	/**
	 * stores the users on the waitlist
	 */
	private LinkedList<User> usersOnWaitlist;

	/**
	 * help variable to get the status of the meeting
	 */
	private String ststus;

	/**
	 * identifier of the meeting (user_link)
	 */
	private String id;

	/**
	 * help variable that should prevent the id from reloading
	 */
	private boolean firstLoad = true;

	/**
	 * 
	 */
	private User editableUser;

	/**
	 * initializes all
	 */
	@PostConstruct
	void init() {
		initComponents();

		loadID();

		initMeeting();

		if (meeting != null) {

			initUser();

			initUsers();

			initUsersOnWaitlist();

			initUserDates();

			initStatus();

			updateNumberOfParticipants();
		}
	}

	/**
	 * initializes the variables
	 */
	private void initComponents() {
		editableUser = null;
		users = new LinkedList<User>();
		userDates = new HashMap<User, HashMap<TDate, Boolean>>();
		userDatesWaitlist = new HashMap<User, HashMap<TDate, Boolean>>();
		usersOnWaitlist = new LinkedList<User>();
		user = new User("no");
		user.setUserId(-1);
		ststus = "invalid";
	}

	/**
	 * loads the id
	 */
	private void loadID() {
		if (firstLoad) {
			FacesContext context = FacesContext.getCurrentInstance();
			Map<String, String> paramMap = context.getExternalContext()
					.getRequestParameterMap();
			id = paramMap.get("id");
			firstLoad = false;
		}
	}

	/**
	 * initializes the meeting
	 */
	private void initMeeting() {
		for (Meeting meeting : meetingEJB.getAll()) {
			if (meeting.getUserLink().equals(id)) {
				this.meeting = meeting;
			}
		}
	}

	/**
	 * if a user is logged in this method will set the user to the logged in
	 * user
	 */
	private void initUser() {
		Locale locale = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestLocale();
		String localeString = locale.toString();
		if (localeString.contains("de")) {
			user.setLocale("de");
		} else {
			user.setLocale("en");
		}
		if (loginBean.isLoggedIn()) {
			if (!meeting.getUsers().contains(loginBean.getUser())) {
				user = loginBean.getUser();
			} else {
				editableUser = loginBean.getUser();
			}
		}
	}

	/**
	 * adds all the users of the meeting to userList
	 */
	private void initUsers() {
		users.addAll(meeting.getUsers());
		// if(!maxParticipantsReached())
		users.add(user);

		Collections.sort(users, new UserComparator());
	}

	/**
	 * adds all waiting users to the waitlist
	 */
	private void initUsersOnWaitlist() {
		LinkedList<Waitlist> waitlists;

		for (TDate date : meeting.getDates()) {
			waitlists = new LinkedList<Waitlist>(date.getWaitlists());
			Collections.sort(waitlists, new WaitlistComparator());
			for (Datelist datelist : date.getDatelists()) {
				if (datelist.getWaiting().equalsIgnoreCase("yes")
						&& !usersOnWaitlist.contains(datelist.getUser()))
					usersOnWaitlist.add(datelist.getUser());
			}
			// for (Waitlist waitlist : waitlists) {
			// // users.remove(waitlist.getUser());
			// usersOnWaitlist.add(waitlist.getUser());
			// }
			// break;
		}
		Collections.sort(usersOnWaitlist, new UserComparator());
		// TODO
		if (maxParticipantsReached()) {
			users.remove(user);
			// editableUser = user;
		}
		if (loginBean.isLoggedIn()
				&& usersOnWaitlist.contains(loginBean.getUser())) {
			editableUser = loginBean.getUser();
		} else {
			usersOnWaitlist.add(user);
		}
	}

	/**
	 * initializes the userDates
	 */
	private void initUserDates() {
		for (User user : users) {
			userDates.put(user, new HashMap<TDate, Boolean>());
		}

		for (User user : usersOnWaitlist) {
			userDatesWaitlist.put(user, new HashMap<TDate, Boolean>());
		}

		if (maxParticipantsReached())
			userDates.put(user, new HashMap<TDate, Boolean>());

		for (TDate date : meeting.getDates()) {
			for (Datelist datelist : date.getDatelists()) {
				for (User user : meeting.getUsers()) {
					if (datelist.getUser().equals(user)
							&& datelist.getWaiting().equalsIgnoreCase("no"))
						userDates.get(user).put(date, true);
					// else
					// userDatesWaitlist.get(user).put(date, true);
				}
				for (User user : usersOnWaitlist) {
					if (datelist.getUser().equals(user)
							&& datelist.getWaiting().equalsIgnoreCase("yes"))
						userDatesWaitlist.get(user).put(date, true);
				}
			}
		}
	}

	/**
	 * initializes the status
	 */
	private void initStatus() {
		for (TDate date : meeting.getDates()) {
			if (date.getStatus().equalsIgnoreCase("valid")) {
				ststus = "valid";
				users.remove(user);
				usersOnWaitlist.remove(user);
				break;
			}
		}
	}

	/**
	 * This method shows if a date of a meeting is confirmed or not
	 * 
	 * @param o
	 *            - could be null or something else
	 * @return true if the status of the meeting is valid else it returns false
	 */
	public boolean isConfirmed(Object o) {
		return ststus.equalsIgnoreCase("valid");
	}

	/**
	 * This method shows if a user is logged in or not
	 * 
	 * @return true if a user is logged in
	 */
	public boolean isLoggedIn() {
		return loginBean.isLoggedIn();
	}

	/**
	 * this method compares the user with the logged in user
	 * 
	 * @param user
	 *            to compare with loggedIn User
	 * @return true if the user is the same as the logged in user
	 */
	public boolean isUserLoggedIn(User user) {
		return !loginBean.isLoggedIn() ? false : loginBean.getUser().equals(
				user) ? true : false;
	}

	/**
	 * This method returns all the waiting users
	 * 
	 * @return the waiting users of the meeting
	 */
	public LinkedList<User> getUsersOnWaitlist() {
		return usersOnWaitlist;
	}

	/**
	 * updates the numberOfParticipants map
	 */
	public void updateNumberOfParticipants() {
		numberOfParticipants = new HashMap<TDate, Integer>();
		for (TDate date : meeting.getDates()) {
			numberOfParticipants.put(date, 0);
			for (User user : meeting.getUsers()) {
				if (!userDates.get(user).isEmpty())
					// TODO
					try {
						if (userDates.get(user).get(date))
							numberOfParticipants.put(date,
									numberOfParticipants.get(date) + 1);

					} catch (Exception e) {
						LOGGER.log(Level.ALL, e.toString(), e);
						// System.out.println(date.getDateTime());
					}
			}
		}
	}

	/**
	 * This method returns the number of the actual participants
	 * 
	 * @return the number of actual participants
	 */
	public HashMap<TDate, Integer> getNumberOfParticipants() {
		return numberOfParticipants;
	}

	/**
	 * Getter for userDates
	 * 
	 * @return userdates
	 */
	public HashMap<User, HashMap<TDate, Boolean>> getUserDates() {
		return userDates;
	}

	public HashMap<User, HashMap<TDate, Boolean>> getUserDatesWaitlist() {
		return userDatesWaitlist;
	}

	/**
	 * This method takes all dates of the meeting and returns them as a sorted
	 * list
	 * 
	 * @return a sortet list of the dates of the meeting
	 */
	public List<TDate> sortedDates() {
		LinkedList<TDate> dates = new LinkedList<TDate>();
		dates.addAll(meeting.getDates());
		Collections.sort(dates, new TDateComparator());
		return dates;
	}

	/**
	 * Getter for meeting
	 * 
	 * @return the specified meeting
	 */
	public Meeting getMeeting() {
		return meeting;
	}

	/**
	 * getter for userList
	 * 
	 * @return sorted LKist of users of the specified meeting
	 */
	public LinkedList<User> getUsers() {
		return users;
	}

	/**
	 * This method takes a UserID and returns the associated user
	 * 
	 * @param userID
	 *            - get the id with meeting.getCreator()
	 * @return userObject with given id
	 */
	public User getCreator(int userID) {
		for (User user : userEJB.getAll()) {
			if (user.getUserId() == userID) {
				return user;
			}
		}
		return null;
	}

	/**
	 * This method will format a given Date to the format "MMM - yyyy" eg.
	 * "Jun - 2015"
	 * 
	 * @param date
	 * @return a formated date in format "MMM - yyyy"
	 */
	public String getMonthAsString(TDate date) {
		return new DateTime(date.getDateTime()).toString("MMM - yyyy");
	}

	/**
	 * This method will format a given date to he format "dd - E" eg. "12 Mon"
	 * 
	 * @param date
	 * @return a formated date in format "dd - E"
	 */
	public String getDayAsString(TDate date) {
		return new DateTime(date.getDateTime()).toString("dd - E");
	}

	/**
	 * this method will format a given date to format "HH:mm" eg. "20:15"
	 * 
	 * @param date
	 * @return a formated date in format "HH:mm"
	 */
	public String getTimeAsString(TDate date) {
		return new DateTime(date.getDateTime()).toString("HH:mm");
	}

	/**
	 * This method will return all the possible meeting dates to the given month
	 * 
	 * @param date
	 * @return the number of all possible meeting dates in a month
	 */
	public int getTimesInMonth(TDate date) {
		int timesInMonth = 0;
		for (TDate date_ : getDaysOfMonth(date)) {
			timesInMonth += getTimesOfDay(date_).size();
		}
		return timesInMonth;
	}

	/**
	 * with this method you can make a user editable
	 * 
	 * @param user
	 *            that should be editable
	 */
	public void makeUserEditable(User user) {
		editableUser = user;
	}

	/**
	 * 
	 * @param user
	 * @return true if the user is editable
	 */
	public boolean isUserEditable(User user) {
		return user.equals(editableUser);
	}

	/**
	 * This method takes all dates of the meeting and selects the months.
	 * 
	 * @param date_
	 *            - this parameter can be null
	 * @return a list with all potential months of meetingdates
	 */
	public List<TDate> getMonths(TDate date_) {
		List<TDate> months = new LinkedList<TDate>();
		List<String> monthStringList = new LinkedList<String>();
		String actMonth, lastMonth = "";
		for (TDate date : meeting.getDates()) {
			actMonth = new DateTime(date.getDateTime()).toString("MM-yyyy");
			if(!monthStringList.contains(actMonth)){
				months.add(date);
				monthStringList.add(actMonth);
			}
		}
		Collections.sort(months, new TDateComparator());
		return months;
	}

	/**
	 * This method takes the given month and returns a list with all dates in
	 * this month
	 * 
	 * @param date
	 * @return a list wit all potential days in the specified month
	 */
	public LinkedList<TDate> getDaysOfMonth(TDate date) {
		LinkedList<TDate> daysOfMonth = new LinkedList<TDate>();
		LinkedList<String> daysAsString = new LinkedList<String>();
		String actDay, lastday = "", month = new DateTime(date.getDateTime())
				.toString("MM-yyyy");// .format(date.getDateTime());
		for (TDate date_ : meeting.getDates()) {
			actDay = new DateTime(date_.getDateTime()).toString("MM-yyyy-dd");
			if (month.equals(new DateTime(date_.getDateTime())
					.toString("MM-yyyy"))
					&& !actDay.equals(lastday)
					&& !daysAsString.contains(actDay)) {
				daysOfMonth.add(date_);
				daysAsString.add(actDay);
				lastday = actDay;
			}
		}
		Collections.sort(daysOfMonth, new TDateComparator());
		return daysOfMonth;
	}

	/**
	 * this method takes the given day and returns all potential times of this
	 * day
	 * 
	 * @param date
	 * @return a list with all potential times of given day
	 */
	public LinkedList<TDate> getTimesOfDay(TDate date) {
		LinkedList<TDate> timesOfDay = new LinkedList<TDate>();
		String actDay, lastday = "", initDay = new DateTime(date.getDateTime())
				.toString("MM-yyyy-dd");// .format(date.getDateTime());
		for (TDate date_ : meeting.getDates()) {
			actDay = new DateTime(date_.getDateTime())
					.toString("MM-yyyy-dd HH:mm");
			if (initDay.equals(new DateTime(date_.getDateTime())
					.toString("MM-yyyy-dd")) && !actDay.equals(lastday)) {
				timesOfDay.add(date_);
				lastday = actDay;
			}
		}
		Collections.sort(timesOfDay, new TDateComparator());
		return timesOfDay;
	}

	/**
	 * This method checks if the maximum number of participants is reached
	 * 
	 * @return true if the maximum number of participants is reached
	 */
	// old Method
	// public boolean maxParticipantsReached() {
	// // -1 because DummyUser in userList
	// // for (TDate date : meeting.getDates()) {
	// // int counter = 0;
	// // for (Datelist datelist : date.getDatelists()) {
	// // if(!datelist.getWaiting().equalsIgnoreCase("yes"))
	// // counter++;
	// // }
	// // if(counter >= meeting.getMaxParticipants())
	// // return true;
	// // }
	// // return false;
	// return users.contains(user) ? users.size() - 1 >= meeting
	// .getMaxParticipants() : users.size() >= meeting
	// .getMaxParticipants();
	// }

	public boolean maxParticipantsReached() {
		for (TDate date : meeting.getDates()) {
			if (maxParticipantsReached(date))
				return true;
		}
		return false;
	}

	public boolean maxParticipantsReached(TDate date) {
		int counter = 0;
		for (Datelist datelist : date.getDatelists()) {
			if (!datelist.getWaiting().equalsIgnoreCase("yes"))
				counter++;
		}

		if (counter >= meeting.getMaxParticipants())
			return true;

		return false;
	}

	/**
	 * 
	 * @param user
	 * @return true if the name-field of user equals null or the user is the
	 *         editableUser
	 */
	public boolean dummyUser(User user) {
		return user.getUserName() == null || user.equals(editableUser);
	}

	/**
	 * this method returns the editableUser if it is set before. else it returns
	 * null.
	 * 
	 * @return editableUser if set null else
	 */
	public User getEditableUser() {
		return editableUser;
	}

	/**
	 * adds a user to the meeting
	 * 
	 * @return link to overview.xhtml
	 */
	// old Method
	// public String addUser() {
	// Meeting m2 = null;
	// for (Meeting meeting : meetingEJB.getAll()) {
	// if (meeting.getUserLink().equals(id)) {
	// m2 = meeting;
	// }
	// }
	//
	// if (m2 != null && !m2.equals(meeting)) {
	// return "error.xhtml?id=" + id;
	// }
	//
	// User u = doesUserExist();
	// if (!loginBean.isLoggedIn()) {
	// if (!meeting.getUsers().contains(loginBean.getUser())) {
	// u.setUserId(null);
	// }
	// }
	// if (u.equals(this.user)) {
	// // Save a new user in the database
	// if (!loginBean.isLoggedIn())
	// userEJB.saveUser(u);
	// }
	//
	// User u2 = doesUserExist();
	//
	// if (meeting.getUsers().contains(u2)) {
	// return "erroradduser.xhtml?id=" + id;
	// }
	//
	// if (!loginBean.isLoggedIn()) {
	// if (!meeting.getUsers().contains(loginBean.getUser())) {
	// u.setUserId(-1);
	// }
	// }
	//
	// if (maxParticipantsReached()) {
	// for (TDate date : meeting.getDates()) {
	// Waitlist wl = new Waitlist(new WaitlistId(date.getDateId(),
	// u2.getUserId()), date, u2, date.getDateTime());
	// wl.setJointime(new Date());
	// date.getWaitlists().add(wl);
	//
	// }
	// }
	// if (!maxParticipantsReached())
	// meeting.getUsers().add(u2);
	//
	// for (TDate date : meeting.getDates()) {
	// if (userDates.get(u).get(date)) {
	// Datelist datelist = new Datelist();
	// datelist.setId(new DatelistId(date.getDateId(), u2.getUserId()));
	// datelist.setUser(u2);
	// datelist.setDate(date);
	// datelist.setWaiting(maxParticipantsReached() ? "yes" : "no");
	// date.getDatelists().add(datelist);
	// datelistEJB.saveDatelist(datelist);
	// }
	// }
	//
	// for (TDate date : meeting.getDates()) {
	// for (Datelist datelist : date.getDatelists()) {
	// if (datelist.getUser().equals(user))
	// datelist.setWaiting("no");
	// }
	// }
	//
	// sendMail();
	//
	// meetingEJB.updateMeeting(meeting);
	// return "overview.xhtml?id=" + id;
	// }

	public String addUser() {
		Meeting m2 = null;
		for (Meeting meeting : meetingEJB.getAll()) {
			if (meeting.getUserLink().equals(id)) {
				m2 = meeting;
			}
		}

		if (m2 != null && !m2.equals(meeting)) {
			return "error.xhtml?id=" + id;
		}

		User u = doesUserExist();
		if (!loginBean.isLoggedIn()) {
			if (!meeting.getUsers().contains(loginBean.getUser())) {
				u.setUserId(null);
			}
		}
		if (u.equals(this.user)) {
			// Save a new user in the database
			if (!loginBean.isLoggedIn())
				userEJB.saveUser(u);
		}

		User u2 = doesUserExist();

		if (meeting.getUsers().contains(u2)) {
			return "erroradduser.xhtml?id=" + id;
		}

		if (!loginBean.isLoggedIn()) {
			if (!meeting.getUsers().contains(loginBean.getUser())) {
				u.setUserId(-1);
			}
		}

		for (TDate date : meeting.getDates()) {
//			if (!maxParticipantsReached(date)) {
				if (userDates.get(u) != null)
					if (userDates.get(u).get(date) != null)
						if (userDates.get(u).get(date)) {
							Datelist datelist = new Datelist();
							datelist.setId(new DatelistId(date.getDateId(), u2
									.getUserId()));
							datelist.setUser(u2);
							datelist.setDate(date);
							if (maxParticipantsReached(date)) {
								datelist.setWaiting("yes");
							} else {
								datelist.setWaiting("no");
								meeting.getUsers().add(u2);
							}
							datelist.setWaittime(new Date());
							date.getDatelists().add(datelist);
							datelistEJB.saveDatelist(datelist);
						}
//			} else {
				if (userDatesWaitlist.get(u) != null)
					if (userDatesWaitlist.get(u).get(date) != null)
						if (userDatesWaitlist.get(u).get(date)) {
							Datelist datelist = new Datelist();
							datelist.setId(new DatelistId(date.getDateId(), u2
									.getUserId()));
							datelist.setUser(u2);
							datelist.setDate(date);
							if (maxParticipantsReached(date)) {
								datelist.setWaiting("yes");
							} else {
								datelist.setWaiting("no");
								meeting.getUsers().add(u2);
							}
							datelist.setWaittime(new Date());
							date.getDatelists().add(datelist);
							datelistEJB.saveDatelist(datelist);
						}
//			}
		}

		// for (TDate date : meeting.getDates()) {
		// for (Datelist datelist : date.getDatelists()) {
		// if (datelist.getUser().equals(user))
		// datelist.setWaiting("no");
		// }
		// }

		sendMail();

		meetingEJB.updateMeeting(meeting);
		return "overview.xhtml?id=" + id;
	}

	/**
	 * this method saves the changes of the editableUSer to Database
	 * 
	 * @return link to overview.xhtml
	 */
	// old Method
	// public String editUSer() {
	// Set<Datelist> removeLists = new HashSet<Datelist>();
	// try {
	// for (TDate date : meeting.getDates()) {
	// if (userDates.get(editableUser).get(date) != null) {
	// if (userDates.get(editableUser).get(date)) {
	// boolean check = false;
	// for (Datelist dl : date.getDatelists()) {
	// if (dl.getUser().getUserId() == editableUser
	// .getUserId())
	// check = true;
	// }
	// if (!check) {
	// Datelist datelist = new Datelist();
	// datelist.setId(new DatelistId(date.getDateId(),
	// editableUser.getUserId()));
	// datelist.setUser(editableUser);
	// datelist.setDate(date);
	// datelist.setWaiting(usersOnWaitlist
	// .contains(editableUser) ? "yes" : "no");
	// date.getDatelists().add(datelist);
	// datelistEJB.saveDatelist(datelist);
	// }
	// } else {
	// for (Datelist dl : date.getDatelists()) {
	// if (dl.getUser().getUserId() == editableUser
	// .getUserId())
	// removeLists.add(dl);
	// }
	// }
	// }
	// }
	//
	// meetingEJB.updateMeeting(meeting);
	//
	// for (Datelist datelist : removeLists) {
	// for (TDate date : meeting.getDates()) {
	// date.getDatelists().remove(datelist);
	// datelistEJB.removeDateList(datelist);
	// }
	// }
	//
	// } catch (Exception e) {
	// LOGGER.log(Level.ALL, e.toString(), e);
	// // e.printStackTrace();
	// // System.out.println("sdfgsdafgsdfhg");
	// }
	//
	// userEJB.updateUser(editableUser);
	//
	// sendMail();
	//
	// return "overview.xhtml?id=" + id;
	// }

	public String editUSer() {
		Set<Datelist> removeLists = new HashSet<Datelist>();
		try {
			for (TDate date : meeting.getDates()) {
				if (userDates.get(editableUser) != null){
					if (userDates.get(editableUser).get(date) != null) 
						if (userDates.get(editableUser).get(date)) {
							boolean check = false;
							for (Datelist dl : date.getDatelists()) {
								if (dl.getUser().getUserId() == editableUser
										.getUserId())
									check = true;
							}
							if (!check) {
								Datelist datelist = new Datelist();
								datelist.setId(new DatelistId(date.getDateId(),
										editableUser.getUserId()));
								datelist.setUser(editableUser);
								datelist.setDate(date);
								datelist.setWaiting(maxParticipantsReached(date) ? "yes"
										: "no");
								System.out
										.println(maxParticipantsReached(date));
								date.getDatelists().add(datelist);
								datelistEJB.saveDatelist(datelist);
							}
						} else {
							for (Datelist dl : date.getDatelists()) {
								if (dl.getUser().getUserId() == editableUser
										.getUserId()) {
									removeLists.add(dl);
									System.out.println("remove");
								}
							}
						}
					} else if (userDatesWaitlist.get(editableUser) != null)
						if (userDatesWaitlist.get(editableUser).get(date) != null) {
							if (userDatesWaitlist.get(editableUser).get(date)) {
								boolean check = false;
								for (Datelist dl : date.getDatelists()) {
									if (dl.getUser().getUserId() == editableUser
											.getUserId())
										check = true;
								}
								if (!check) {
									Datelist datelist = new Datelist();
									datelist.setId(new DatelistId(date
											.getDateId(), editableUser
											.getUserId()));
									datelist.setUser(editableUser);
									datelist.setDate(date);
									datelist.setWaiting(maxParticipantsReached(date) ? "yes"
											: "no");
									date.getDatelists().add(datelist);
									datelistEJB.saveDatelist(datelist);
								}
							} else {
								for (Datelist dl : date.getDatelists()) {
									if (dl.getUser().getUserId() == editableUser
											.getUserId())
										removeLists.add(dl);
								}
							}
						}
			}
			meetingEJB.updateMeeting(meeting);

			for (Datelist datelist : removeLists) {
				// for (TDate date : meeting.getDates()) {
				// date.getDatelists().remove(datelist);
				datelistEJB.removeDateList(datelist);
				System.out.println("removed");
				// }
			}

		} catch (Exception e) {
			LOGGER.log(Level.ALL, e.toString(), e);
			e.printStackTrace();
			// System.out.println("Exception");
		}

		userEJB.updateUser(editableUser);

		init();
		updateWaitingParticipants();

		sendMail();

		return "overview.xhtml?id=" + id;
	}

	/**
	 * deletes the specified user
	 * 
	 * @param user
	 */
	// old Method
	// public void deleteUser(User user) {
	// // users.remove(user);
	// // meeting.getUsers().remove(user);
	//
	// Set<Datelist> removeLists = new HashSet<Datelist>();
	// Set<Waitlist> removeWaitlists = new HashSet<Waitlist>();
	// for (TDate date : meeting.getDates()) {
	// for (Datelist datelist : date.getDatelists()) {
	// if (datelist.getUser().equals(user)) {
	// removeLists.add(datelist);
	// // date.getDatelists().remove(datelist);
	// // datelistEJB.removeDateList(datelist);
	// }
	// }
	// for (Waitlist waitlist : date.getWaitlists()) {
	// if (waitlist.getUser().equals(user)) {
	// removeWaitlists.add(waitlist);
	// }
	// }
	// }
	//
	// users.remove(user);
	//
	// if (removeWaitlists.isEmpty()) {
	// if (!maxParticipantsReached()) {
	// if (!usersOnWaitlist.getFirst().equals(this.user)) {
	// User u = usersOnWaitlist.removeFirst();
	// users.add(u);
	// meeting.getUsers().add(u);
	// for (TDate date : meeting.getDates()) {
	// for (Waitlist waitlist : date.getWaitlists()) {
	// if (waitlist.getUser().equals(u)) {
	// removeWaitlists.add(waitlist);
	// }
	// }
	// }
	// }
	// }
	//
	// }
	//
	// usersOnWaitlist.remove(user);
	// meeting.getUsers().remove(user);
	// meetingEJB.updateMeeting(meeting);
	//
	// for (Datelist datelist : removeLists) {
	// for (TDate date : meeting.getDates()) {
	// date.getDatelists().remove(datelist);
	// datelistEJB.removeDateList(datelist);
	// }
	// }
	// for (Waitlist waitlist : removeWaitlists) {
	// // for (TDate date : meeting.getDates()) {
	// waitlistEJB.removeWaitList(waitlist);
	// user.getWaitlists().remove(waitlist);
	// // dateEJB.updateDate(date);
	// // }
	// }
	//
	// for (User user_ : userEJB.getAll()) {
	// if (user_.getUserName().equals(user.getUserName())
	// && user_.getEmail().equals(user.getEmail())) {
	// user = user_;
	// }
	// }
	//
	// if (user.getRegistered() != null
	// && !user.getRegistered().equalsIgnoreCase("yes")) {
	// userEJB.removeUser(user);
	// }
	// sendMail();
	// init();
	// }

	public void deleteUser(User user) {
		// users.remove(user);
		// meeting.getUsers().remove(user);

		Set<Datelist> removeLists = new HashSet<Datelist>();
		for (TDate date : meeting.getDates()) {
			for (Datelist datelist : date.getDatelists()) {
				if (datelist.getUser().equals(user)) {
					removeLists.add(datelist);
					// date.getDatelists().remove(datelist);
					// datelistEJB.removeDateList(datelist);
				}
			}
		}

		users.remove(user);

		usersOnWaitlist.remove(user);
		meeting.getUsers().remove(user);
		meetingEJB.updateMeeting(meeting);

		for (Datelist datelist : removeLists) {
			for (TDate date : meeting.getDates()) {
				date.getDatelists().remove(datelist);
				datelistEJB.removeDateList(datelist);
			}
		}

		for (User user_ : userEJB.getAll()) {
			if (user_.getUserName().equals(user.getUserName())
					&& user_.getEmail().equals(user.getEmail())) {
				user = user_;
			}
		}

		if (user.getRegistered() != null
				&& !user.getRegistered().equalsIgnoreCase("yes")) {
			userEJB.removeUser(user);
		}
		sendMail();
		init();

		updateWaitingParticipants();

		init();
	}

	private void updateWaitingParticipants() {
		LinkedList<Datelist> datelistsToChange = new LinkedList<Datelist>();
		for (TDate date : meeting.getDates()) {
			if (!maxParticipantsReached(date))
				for (Datelist datelist : date.getDatelists()) {
					if (datelist.getWaiting().equalsIgnoreCase("yes"))
						datelistsToChange.add(datelist);
				}
		}

		Collections.sort(datelistsToChange, new DatelistWaittimeComparator());
		if (!datelistsToChange.isEmpty()) {
			User userToChange = datelistsToChange.getFirst().getUser();
			mailEJB.sendMail(userToChange.getEmail(),
					"mailWaitlistRemovedSubject", "mailWaitlistRemoved",
					new Object[] { meeting.getTitle() });

			if (!meeting.getUsers().contains(userToChange))
				meeting.getUsers().add(userToChange);

			for (Datelist datelist : datelistsToChange) {
				if (datelist.getUser().equals(userToChange)) {
					datelist.setWaiting("no");
					datelistEJB.updateDatelist(datelist);
				}
			}
			meetingEJB.updateMeeting(meeting);
		}

		LinkedList<Datelist> datelistsToChange2 = new LinkedList<Datelist>();
		for (TDate date : meeting.getDates()) {
			if (!maxParticipantsReached(date))
				for (Datelist datelist : date.getDatelists()) {
					if (datelist.getWaiting().equalsIgnoreCase("yes"))
						datelistsToChange2.add(datelist);
				}
		}
		if (datelistsToChange2.size() != datelistsToChange.size())
			updateWaitingParticipants();
	}

	private void sendMail() {
		LinkedList<TDate> mostEfficientDates = dateEvaluationEJB
				.getMostEfficientDates(meeting);
		String mostEfficientDatesString = "";
		for (TDate date : mostEfficientDates) {
			mostEfficientDatesString += date.getDateTime().toString() + "\n";
		}
		if (mostEfficientDates.size() > 0) {
			if (meeting.getMailSend().equals("no")) {
				mailEJB.sendMail(
						getCreator(meeting.getCreator()).getEmail(),
						"mailMinParticipantsReachedSubject",
						"mailMinParticipantsReached",
						new Object[] { meeting.getTitle(),
								mostEfficientDatesString,
								urlEJB.adminLinkBase() + meeting.getAdminLink() });
				meeting.setMailSend("yes");
			}
		} else {
			if (meeting.getMailSend().equalsIgnoreCase("yes")) {
				if (isConfirmed(null)) {
					mailEJB.sendMail(
							getCreator(meeting.getCreator()).getEmail(),
							"mailConfirmedBelowMinSubject",
							"mailConfirmedBelowMin",
							new Object[] { urlEJB.adminLinkBase()
									+ meeting.getUserLink() });
				}
			}
			meeting.setMailSend("no");
		}
	}

	private User doesUserExist() {
		for (User user : userEJB.getAll()) {
			if (user.getRegistered().equalsIgnoreCase("no")) {
				if (user.getUserName().equals(this.user.getUserName())) {
					if (user.getEmail().equals(this.user.getEmail())) {
						return user;
					}
				}
			}
		}
		return this.user;
	}

	// /**
	// * @author EW This class can be used to sort dates
	// */
	// class TDateComparator implements Comparator<TDate> {
	// @Override
	// public int compare(TDate date1, TDate date2) {
	// if (date1.getDateTime().before(date2.getDateTime())) {
	// return -1;
	// } else if (date1.getDateTime().after(date2.getDateTime())) {
	// return 1;
	// }
	// return 0;
	// }
	// }
	//
	// /**
	// * @author EW This class can be used to sort users by username
	// */
	// class UserComparator implements Comparator<User> {
	// @Override
	// public int compare(User user1, User user2) {
	// if (user1.getUserName() == null)
	// return 1;
	// if (user2.getUserName() == null)
	// return -1;
	// return user1.getUserName().compareTo(user2.getUserName());
	// }
	// }
	//
	// /**
	// *
	// * @author EW This class can be used so sort Waitlists by jointime
	// *
	// */
	// class WaitlistComparator implements Comparator<Waitlist> {
	// @Override
	// public int compare(Waitlist o1, Waitlist o2) {
	// if (o1.getJointime().before(o2.getJointime()))
	// return -1;
	// if (o1.getJointime().after(o2.getJointime()))
	// return 1;
	// return 0;
	// }
	// }
}

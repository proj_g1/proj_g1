package bean;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import ejb.MeetingEJB;
import ejb.UrlEJB;
import entity.Meeting;
import entity.User;

@ManagedBean
@ViewScoped
public class OverviewBean {
	@Inject
	private MeetingEJB meetingEJB;	
	@Inject
	private UrlEJB urlEJB;
	@Inject
	private LoginBean loginBean;

	private List<Meeting> ownMeetings;
	private List<Meeting> otherMeetings;	
	private String adminUrl;
	private String shareUrl;

	@PostConstruct
	public void init() {
		
		adminUrl= urlEJB.adminLinkBase();
		shareUrl = urlEJB.shareLinkBase();
		
		ownMeetings = new ArrayList<Meeting>();
		otherMeetings = new ArrayList<Meeting>();
		List<Integer> userIds=new ArrayList<Integer>();		
		int userId = loginBean.getUser().getUserId();

		for (Meeting m : meetingEJB.getAll()) {
			if (m.getCreator() == userId)
				ownMeetings.add(m);
			
			for(User u:m.getUsers())
			userIds.add(u.getUserId());
			if (userIds.contains(userId))
				otherMeetings.add(m);
			userIds.clear();
		}
	}

	public List<Meeting> getOwnMeetings() {
		return ownMeetings;
	}

	public List<Meeting> getOtherMeetings() {
		return otherMeetings;
	}

	public String getAdminUrl() {
		return adminUrl;
	}

	public String getShareUrl() {
		return shareUrl;
	}
	
	
}
package bean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import util.GroupsNameComparator;
import ejb.GroupingEJB;
import ejb.GroupsEJB;
import ejb.MailEJB;
import ejb.MessageEJB;
import ejb.UserEJB;
import entity.Grouping;
import entity.GroupingId;
import entity.Groups;
import entity.User;

/**
 * Used by groupmanagement.xhtml
 * 
 * @author AH
 */

@ManagedBean(name = "userBean")
@SessionScoped
public class UserBean {
	private String username;
	private String email;
	private String password;
	private String registered;
	// private String role;
	
	/**
	 * Injection of the UserEJB for User related DB usage.
	 */
	@Inject
	private UserEJB userService;
	
	/**
	 * Injection of the GroupsEJB for Group related DB usage.
	 */
	@Inject
	private GroupsEJB groupsService;
	
	/**
	 * Injection of the MessageEJB for Facesmessages.
	 */
	@Inject
	private MessageEJB messageService;
	
	/**
	 * Injection of the GroupingEJB for Grouping related DB usage.
	 */
	@Inject
	private GroupingEJB groupingService;
	
	/**
	 * Injection of MailEJB to send E-Mails.
	 */
	@Inject
	private transient MailEJB mailService;
	/**
	 * Injection of LoginBean to see if a user is logged in.
	 */
	@Inject
	private LoginBean loginBean;
	
	/**
	 * A UIComponent which binds to a corresponding commandButton.
	 */
	private UIComponent componentA, componentB;
	
	/**
	 * General bean operation setup.
	 */
	@PostConstruct
	public void init() {
		initGroupList();
		initMyGroupList();
		initMyJoinedGroupsList();
		initMyGroupingInquiriesList();
	}
	
	public UserEJB getUserEJB() {
		return userService;
	}
	public void setUserEJB(UserEJB userService) {
		this.userService = userService;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRegistered() {
		return registered;
	}

	public void setRegistered(String registered) {
		this.registered = registered;
	}

	/*
	 * public String getRole() { return role; } public void setRole(String role)
	 * { this.role = role; }
	 */

	public List<User> getAllUsers() {
		return userService.getAll();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	/**
	 * Getter method for groupName.
	 * 
	 * @return Name of the group
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * Setter method for groupName.
	 * 
	 * @param groupName
	 *            Name of the group
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	
	
	public String getCreateGroupGroupName() {
		return createGroupGroupName;
	}
	public void setCreateGroupGroupName(String createGroupGroupName) {
		this.createGroupGroupName = createGroupGroupName;
	}
	private String createGroupGroupName;
	
	public String getCreateGroupGroupDescription() {
		return createGroupGroupDescription;
	}
	public void setCreateGroupGroupDescription(String createGroupGroupDescription) {
		this.createGroupGroupDescription = createGroupGroupDescription;
	}
	private String createGroupGroupDescription;
	
	
	
	/**
	 * Getter method for groupDesciption.
	 * 
	 * @return Description of the group
	 */
	public String getGroupDescription() {
		return groupDescription;
	}

	/**
	 * Setter method for groupDesciption.
	 * 
	 * @param groupDescription
	 *            Description of the group
	 */
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	/**
	 * Getter method for groupsForUsersAdding.
	 * 
	 * @return List representation of Groups which are used to create Groupings
	 */
	public List<Groups> getGroupsForUsersAdding() {
		return groupsForUsersAdding;
	}
	/**
	 * Setter method for groupsForUsersAdding.
	 * 
	 * @param groupsForUsersAdding
	 *        List representation of Groups which are used to create Groupings
	 */
	public void setGroupsForUsersAdding(List<Groups> groupsForUsersAdding) {
		this.groupsForUsersAdding = groupsForUsersAdding;
	}
	
	/**
	 * Getter method for groupBeanList.
	 * 
	 * @return List representation of all groups in the database
	 */
	public List<Groups> getGroupBeanList() {
		return groupBeanList;
	}
	/**
	 * Setter method for groupBeanList.
	 * 
	 * @param groupBeanList    List representation of all groups in the database
	 */
	public void setGroupBeanList(List<Groups> groupBeanList) {
		this.groupBeanList = groupBeanList;
	}

	/**
	 * Getter method for myGroupBeanList.
	 * 
	 * @return List of groups which belong to the user
	 */
	public List<Groups> getMyGroupBeanList() {
		return myGroupBeanList;
	}
	/**
	 * Setter method for myGroupBeanList.
	 * 
	 * @param myGroupBeanList    List representation of groups which belong to the user
	 */
	public void setMyGroupBeanList(List<Groups> myGroupBeanList) {
		this.myGroupBeanList = myGroupBeanList;
	}
	
	/**
	 * Getter method for myJoinedGroupsBeanList.
	 * 
	 * @return List representation of groups which the user joined.
	 */
	public List<Groups> getMyJoinedGroupsBeanList() {
		return myJoinedGroupsBeanList;
	}
	/**
	 * Setter method for myJoinedGroupsBeanList.
	 * 
	 * @param myJoinedGroupsBeanList
	 *        List representation of groups which the user joined.
	 */
	public void setMyJoinedGroupsBeanList(List<Groups> myJoinedGroupsBeanList) {
		this.myJoinedGroupsBeanList = myJoinedGroupsBeanList;
	}
	
	/**
	 * Setter method for component.
	 * 
	 * @return UIComponent.
	 */
	public UIComponent getComponentA() {
		return componentA;
	}
	/**
	 * Setter method for component.
	 * 
	 * @param component
	 *        UIComponent component.
	 */
	public void setComponentA(UIComponent component) {
		this.componentA = component;
	}
	
	/**
	 * Setter method for component.
	 * 
	 * @return UIComponent.
	 */
	public UIComponent getComponentB() {
		return componentB;
	}
	/**
	 * Setter method for component.
	 * 
	 * @param component
	 *        UIComponent component.
	 */
	public void setComponentB(UIComponent component) {
		this.componentB = component;
	}
	
	/**
	 * Setter method for myGroupingInquiriesBeanList.
	 * 
	 * @return List representation of groupings which the logged-in user 
	 * requested to be validated (accepted or denied) by a group admin.
	 */
	public List<Grouping> getMyGroupingInquiriesBeanList() {
		return myGroupingInquiriesBeanList;
	}
	/**
	 * Getter method for myGroupingInquiriesBeanList.
	 * 
	 * @param myGroupingInquiriesBeanList
	 *        List representation of groupings which the logged-in user 
	 *        requested to be validated (accepted or denied) by a group admin.
	 */
	public void setMyGroupingInquiriesBeanList(
			List<Grouping> myGroupingInquiriesBeanList) {
		this.myGroupingInquiriesBeanList = myGroupingInquiriesBeanList;
	}

	/**
	 * Fetches a List with all existing Groups from the database.
	 * 
	 * @return List representation of all Groups from the Database, sorted by name
	 */
	public List<Groups> getGroupsList() {
		List<Groups> g = groupsService.getAll();
		Collections.sort(g, new GroupsNameComparator());
		return g;
	}
	
	/**
	 * Searches the database for groups which belong to the logged-in user.
	 * 
	 * @return List containing groups belonging to the logged-in user
	 */
	public List<Groups> getMyGroupsList() {
	//	String currentUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
	 //   User user = userService.getUserByEmail(currentUser);
	    List<Groups> g = groupsService.getMyGroupsByUserID(loginBean.getUser().getUserId());
		Collections.sort(g, new GroupsNameComparator()); 
		return g;
	}
	
	/**
	 * Searches the database for groups which the logged-in user joined.
	 * 
	 * @return List of groups which the logged-in user joined
	 */
	public List<Groups> getMyJoinedGroupsList() {
		//String currentUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
		User user = userService.findById(loginBean.getUser().getUserId());
		ArrayList<Groups> groups = new ArrayList<Groups>();
		if(user.getGroupings() != null){
			for(Grouping g : user.getGroupings()) { //it doesn't update if "userBean.getUser()" instead of "user" is used
				if(g.getValidated().equals("yes"))
					groups.add(g.getGroups());
			}
		}
			
		Collections.sort(groups, new GroupsNameComparator());
		return groups;
	}
	
	/**
	 * Searches the database for groupings which arise when a 
	 * logged-in user wants to join a group.
	 * 
	 * @return List of groupings inquiries which a group admin 
	 * needs to review (deny or accept that a user be allowed 
	 * to join a group)
	 */
	public List<Grouping> getMyGroupingInquiriesList() {
		//String currentUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
		User user = userService.findById(loginBean.getUser().getUserId());
		
		List<Grouping> groupings = groupingService.getAll();
		List<Grouping> groupingsNeedValidation = new ArrayList<Grouping>();
		for(int i=0;i<groupings.size();i++) {
			if(groupings.get(i).getValidated().equals("no") && 
			   groupings.get(i).getGroups().getGroupsAdmin() == user.getUserId())
				groupingsNeedValidation.add(groupings.get(i));
		}
			
		return groupingsNeedValidation;
	}

	/**
	 * Persists a new Group in the database with the data provided by the
	 * webpage/bean. Throws a Facesmessage if the groupname already exists
	 */
	public void saveMyGroup() {
		//check if a given group already exists
		boolean groupExists = false;
		for(int i=0; i<groupBeanList.size(); i++) 
			if(createGroupGroupName.equals(groupBeanList.get(i).getGroupsName())) 
				groupExists = true;

		if(!groupExists) {
			Groups group = new Groups();
			
		//	String currentUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
		//	User user = userService.getUserByEmail(currentUser);
		
			group.setGroupsAdmin(/*user*/ loginBean.getUser().getUserId());
			group.setGroupsName(createGroupGroupName);
			group.setGroupsDescription(createGroupGroupDescription);
			groupsService.saveGroup(group);
			groupBeanList.add(group);
			Collections.sort(groupBeanList, new GroupsNameComparator());
			
			//let the user who just created this group also join/participate
			//  in it by joining him to his group automatically 
			List<Grouping> grouping = groupingService.getAll();
			Grouping g = new Grouping(
				new GroupingId(group.getGroupsId(), /*user*/ loginBean.getUser().getUserId()), 
				group,
				/*user*/ loginBean.getUser(),
				"yes"
			);
			if(!grouping.contains(g)) {
				groupingService.saveGrouping(g);
				initGroupList();
				initMyGroupList();
				initMyJoinedGroupsList();
				initMyGroupingInquiriesList();
			}
			
			messageService.sendFacesMessage(componentA, FacesMessage.SEVERITY_INFO, "adminUserGroupCreated");
		} else 
			messageService.sendFacesMessage(componentA, FacesMessage.SEVERITY_ERROR, "adminUserGroupNameExists");
	}

	/**
	 * Removes the specified group from the database.
	 * 
	 * @param group
	 *        Group which is to be removed from the database
	 */
	public void removeGroup(Groups group) {
		groupsService.remove(group);
		groupBeanList.remove(group);
		initGroupList();
		initMyGroupList();
		initMyJoinedGroupsList();
		initMyGroupingInquiriesList();
	}
	
	/**
	 * Removes a logged-in user from a specified group.
	 * 
	 * @param group
	 *        Group which the logged-in user wants to leave
	 */
	public void leaveGroup(Groups group) { 
		//String currentUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
	    //User user = userService.getUserByEmail(currentUser);
	    List<Grouping> grouping = groupingService.getAll();
		for (int i = 0; i < grouping.size(); i++) {
			if (grouping.get(i).getUser().equals(loginBean.getUser()) && grouping.get(i).getGroups().equals(group)) {
				groupingService.remove(grouping.get(i));
				i = grouping.size();
				
			}
		}
		initGroupList();
		initMyGroupList();
		initMyJoinedGroupsList();
		initMyGroupingInquiriesList();
	}

	/**
	 * Add myself (logged-in user) to a number of user selected groups. 
	 * User is the logged-in user and Group(s) are provided by the webpage.
	 */
	/* not used so for a cleaner code view commented out */
	/*public void addMyselfToGroups() {
		boolean update = false;
		for(int i=0; i<groupsForUsersAdding.size(); i++) {
			update = false;

			List<Grouping> grouping = groupingService.getAll();

			String currentUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
			User user = userService.getUserByEmail(currentUser);
			
			Grouping g = new Grouping(
				new GroupingId(groupsForUsersAdding.get(i).getGroupsId(), user.getUserId()), 
				groupsForUsersAdding.get(i),
				user,
				"yes"
			);

			if(!grouping.contains(g)) {
				groupingService.saveGrouping(g);
				update = true;
			}
			
			if(update) 
				initGroupList();
		}
	}*/
	
	/**
	 * The logged-in user asks each group admin of a group(s) if he may join the selected group(s).
	 * Each group admin gets an email where he decides if he confirms or rejects the user's request.
	 */
	public void askGroupadminForGroupJoining() {
		List<Groups> myJoinedGroups = getMyJoinedGroupsList();
		boolean alreadyInGroup = false;
		String alreadyInTheseGroups="";
		
		//check if user is already in a group
		for(int i=0; i<groupsForUsersAdding.size(); i++) {
			String groupName = groupsForUsersAdding.get(i).getGroupsName();
			for(int j=0;j<myJoinedGroups.size();j++) {
				if(groupName.equals(myJoinedGroups.get(j).getGroupsName())) {
					alreadyInGroup = true;
					alreadyInTheseGroups += myJoinedGroups.get(j).getGroupsName();
					alreadyInTheseGroups += ", ";
				}
			}
		}
		
		//remove last comma (don't try to put this into the for-loops, 
		//  not only is this faster, handling it within the for-loops is harder)
		if(!alreadyInTheseGroups.equals(""))
			alreadyInTheseGroups = alreadyInTheseGroups.substring(0,alreadyInTheseGroups.length()-2);
		
		//proceed only if the user has re-checked his selection
		if(!alreadyInGroup) {
			for(int i=0; i<groupsForUsersAdding.size(); i++) {
	
				int groupAdminID = groupsForUsersAdding.get(i).getGroupsAdmin();
				User groupAdmin = userService.findById(groupAdminID);
				
				//String currentUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
				//User user = userService.getUserByEmail(currentUser);
				User user = loginBean.getUser();
				
	
				//check if a logged-in user owns a group and if so add him automatically
				//  to his group, without sending a request email
				if(user.getUserId() == groupsForUsersAdding.get(i).getGroupsAdmin()) {
					//add grouping with validated="yes"
					Grouping g = new Grouping(
						new GroupingId(groupsForUsersAdding.get(i).getGroupsId(), 
									   user.getUserId()
						), 
						groupsForUsersAdding.get(i), 
						user,
						"yes"
					);
					groupingService.saveGrouping(g);
					messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
							"groupadminAddUserToGroupResponseAdded");
				} else {
					//add grouping with validated="no" 
					//(this grouping needs to be validated by this group's group admin)
					Grouping g = new Grouping(
						new GroupingId(groupsForUsersAdding.get(i).getGroupsId(), 
									   user.getUserId()), 
						groupsForUsersAdding.get(i), 
						user,
						"no"
					);
					/* The "List<Grouping>... if(!grouping.contains(g)) { ..." is commented out because 
					 * it's a design decision: it basically boils down to: do we want a user a get a 
					 * message saying that he already sent a request to join a group or no. The advantage
					 * is that a group admin can not be spammed, the disadvantage is that a group admin 
					 * can't also receive any emails anymore (and therefore not be remembered by a user 
					 * more than once).
					 * The sending frequency may also be set in the email-server */
					//List<Grouping> grouping = groupingService.getAll();
					//and other checks for appropriate facesmessage like "you already send a request"
					//if(!grouping.contains(g)) { 
						groupingService.saveGrouping(g);
						
						//send to each group's group admin a request email
						String username = user.getUserName();
						//String useremail = user.getEmail();
						String adminsGroupName = groupsForUsersAdding.get(i).getGroupsName();
						mailService.sendMail(groupAdmin.getEmail(), 
							"groupadminAddUserToGroupResponseEmailRequestSubject", 
							"groupadminAddUserToGroupResponseEmailRequest", 
							new Object[]{username, adminsGroupName}
						);
						
						messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
								"groupadminGroupJoiningRequestConfirmation");
					/*} else 
						messageService.sendFacesMessage(FacesMessage.SEVERITY_WARN,
								"-> replace this line by an appropriate text (in .propert that is), like "you already send a request");
					}*/
				}
				
			} //for
			initGroupList();
			initMyGroupList();
			initMyJoinedGroupsList();
			initMyGroupingInquiriesList();
		} else
			messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
					"groupadminAddUserToGroupResponseAlreadyExistsPersonalized",alreadyInTheseGroups,0);
	}
	
	/**
	 * Since groups are invite-only, the group admin uses this method 
	 * to add a certain user to a certain group.
	 */
	/* not used so for a cleaner code view commented out */
	/*public void addUserToGroup() {
		if(groupName.length() > 0 && email.length() > 5) {
			boolean userExist = false;
			boolean groupExist = false;
			
			//check if user exists in database
			User userWhichIsAddedByGroupadmin = userService.getUserByEmail(email);
			if(userWhichIsAddedByGroupadmin != null)
				userExist = true;
			else 
				messageService.sendFacesMessage(componentB,FacesMessage.SEVERITY_WARN,
						"groupadminAddUserToGroupResponseEmailerror");

			//check if a given group exists in the database
			List<Groups> groups = groupsService.getAll();
			boolean doesGroupExist = false;
			Groups group = null;
			for(int i=0; i<groups.size(); i++) {
				if(groups.get(i).getGroupsName().equals(groupName)) {
					doesGroupExist = true;
					group = groups.get(i);
					i = groups.size();
				}
			}
			if(doesGroupExist)
				groupExist = true;
			else 
				messageService.sendFacesMessage(componentB,FacesMessage.SEVERITY_WARN,
						"groupadminAddUserToGroupResponseGrouperror");
			
			//both, user and group exist -- add user to group
			if(userExist && groupExist) {
				//check if logged-in user owns a given group
				String currentUser = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
				User loggedInUser = userService.getUserByEmail(currentUser);
				int userID = loggedInUser.getUserId();
				int groupAdminID = group.getGroupsAdmin();
				if(userID != groupAdminID)
					messageService.sendFacesMessage(componentB,FacesMessage.SEVERITY_WARN,
							"groupadminAddUserToGroupResponseGroupBelonging");
				else {
					Grouping g = new Grouping(
						new GroupingId(group.getGroupsId(), userWhichIsAddedByGroupadmin.getUserId()), 
						group, 
						userWhichIsAddedByGroupadmin, 
						"no"
					);
				
					//check if user is already in the group 
					List<Grouping> grouping = groupingService.getAll();
					if(!grouping.contains(g)) {
						//send email notification to user that he has been added to a group
						String groupname = group.getGroupsName();
						String username = userWhichIsAddedByGroupadmin.getUserName();
						mailService.sendMail(userWhichIsAddedByGroupadmin.getEmail(), 
											 "You have been added to this group: " + group.getGroupsName(), 
											 "You have been added to this group: " + group.getGroupsName(), 
											 new Object[]{groupname, username});
						
						groupingService.saveGrouping(g);
						
						initGroupList();
						initMyGroupList();
						initMyJoinedGroupsList();
						
						messageService.sendFacesMessage(componentB,FacesMessage.SEVERITY_INFO,
								"groupadminAddUserToGroupResponse");
					} else 
						messageService.sendFacesMessage(componentB,FacesMessage.SEVERITY_WARN,
								"groupadminAddUserToGroupResponseAlreadyExists");
				}
			}
		}
	}*/
	
	/**
	 * Removes a given grouping from the database.
	 * 
	 * @param grouping
	 *        Grouping which will be removed from the database.
	 */
	public void removeGrouping(Grouping grouping) {
		groupingService.remove(grouping);
		initGroupList();
		initMyGroupList();
		initMyJoinedGroupsList();
		initMyGroupingInquiriesList();
	}
	
	/**
	 * Updates a given grouping validation-field to "yes", and notifies 
	 * the user via email that his request to join a group has been 
	 * accepted by a group admin.
	 * 
	 * @param grouping
	 *        Grouping which will be updated in the database.
	 */
	public void acceptGroupJoinRequest(Grouping grouping) {
		grouping.setValidated("yes");
		groupingService.updateGroup(grouping);
		
		User userThatRequestedJoining = grouping.getUser();
		Groups groupName = grouping.getGroups();
		mailService.sendMail(userThatRequestedJoining.getEmail(), 
			"groupadminAddUserToGroupResponseEmailResponseSubject", 
			"groupadminAddUserToGroupResponseEmailResponseAccepted", 
			new Object[]{groupName.getGroupsName()}
		);
		
		initGroupList();
		initMyGroupList();
		initMyJoinedGroupsList();
		initMyGroupingInquiriesList();
	}
	/**
	 * Removes a given grouping from the database and notifies the user 
	 * via email that his request to join a group has been denied by 
	 * a group admin.
	 * 
	 * @param grouping
	 *        Grouping which will be removed from the database.
	 */
	public void denyGroupJoinRequest(Grouping grouping) {
		groupingService.remove(grouping);
		
		User userThatRequestedJoining = grouping.getUser();
		Groups groupName = grouping.getGroups();
		mailService.sendMail(userThatRequestedJoining.getEmail(), 
			"groupadminAddUserToGroupResponseEmailResponseSubject", 
			"groupadminAddUserToGroupResponseEmailResponseDenied", 
			new Object[]{groupName.getGroupsName()}
		);
		
		initGroupList();
		initMyGroupList();
		initMyJoinedGroupsList();
		initMyGroupingInquiriesList();
	}

	/* Private init methods */
	
	/**
	 * Initialize the groupBeanList representation for the bean with data from the database.
	 */
	private void initGroupList() {
		groupBeanList = getGroupsList();
	}
	
	/**
	 * Initialize the myGroupBeanList representation for the bean with data from the database.
	 */
	private void initMyGroupList() {
		myGroupBeanList = getMyGroupsList();
	}
	
	/**
	 * Initialize the myJoinedGroupsBeanList representation for the bean with data from the database.
	 */
	private void initMyJoinedGroupsList() {
		myJoinedGroupsBeanList = getMyJoinedGroupsList();
	}
	
	/**
	 * Initialize the myGroupBeanList representation for the bean with data from the database.
	 */
	private void initMyGroupingInquiriesList() {
		myGroupingInquiriesBeanList = getMyGroupingInquiriesList();
	}
	
	/* Private data types */
	
	/**
	 * Name of the Group.
	 */
	private String groupName;
	
	/**
	 * Description of the Group.
	 */
	private String groupDescription;

	/**
	 * Groups for the User to be added to.
	 */
	private List<Groups> groupsForUsersAdding;

	/**
	 * Bean representation of the database list.
	 */
	private List<Groups> groupBeanList;
	
	/**
	 * List of groups which belong to the logged-in user.
	 */
	private List<Groups> myGroupBeanList;
	
	/**
	 * List of groupings 
	 */
	private List<Grouping> myGroupingInquiriesBeanList;

	
	/**
	 * List of groups which the logged-in user joined.
	 */
	private List<Groups> myJoinedGroupsBeanList;
}
package bean;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.joda.time.DateTime;
import org.primefaces.model.UploadedFile;

import util.GroupsComparator;
import util.GroupsNameComparator;
import util.HashTool;
import util.UserNameComparator;
import ejb.DBAutoCleanEJB;
import ejb.GroupingEJB;
import ejb.GroupsEJB;
import ejb.MailEJB;
import ejb.MeetingEJB;
import ejb.MessageEJB;
import ejb.SettingsEJB;
import ejb.UrlEJB;
import ejb.UserEJB;
import entity.Datelist;
import entity.Grouping;
import entity.GroupingId;
import entity.Groups;
import entity.Meeting;
import entity.Settings;
import entity.TDate;
import entity.User;

/**
 * Backing bean that handles all necessary data and functions for the webpage
 * admin.xhtml. Featured functions are editing of groups and users, database
 * cleanup and mailsettings.
 * 
 * 
 * @author OK
 *
 */
@Named(value = "adminBean")
@ApplicationScoped
public class AdminBean implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Maximum filesize that is allowed for the CSV file in bytes.
	 */
	public static final int FILE_SIZE = 1000000;
	/**
	 * Number of required entries per CSV File row.
	 */
	public static final int CSV_REQUIRED = 4;
	/**
	 * Name of the Group to be added by the admin.
	 */
	private String groupName;
	/**
	 * Admin of the Group to be added by the admin.
	 */
	private User groupAdmin;
	/**
	 * Description of the Group to be added by the admin.
	 */
	private String groupDescription;

	/**
	 * Username of the user to be added by the admin.
	 */
	private String userName;
	/**
	 * Password of the user to be added by the admin.
	 */
	private String userPassword;
	/**
	 * E-Mail of the user to be added by the admin.
	 */
	private String userMail;
	/**
	 * Language of the user to be added by the admin.
	 */
	private String userLang;
	/**
	 * Available language for translating the mails.
	 */
	private transient List<String> availableLanguages;

	/**
	 * User to be added to groupForUserAdding.
	 */
	private transient List<User> usersForGroupsAdding;
	/**
	 * Groups for the User to be added to.
	 */
	private transient List<Groups> groupsForUsersAdding;

	/**
	 * List of old Meeting to be removed from the database.
	 */
	private transient List<Meeting> meetingsToRemove;
	/**
	 * DateTime of the current systemdate -1 day.
	 */
	private DateTime yesterday;
	/**
	 * List of unnecessary unregistered Users to be removed from the database.
	 */
	private transient List<User> usersToRemove;

	/**
	 * UploadFile for the .csv file to create new Users from.
	 */
	private transient UploadedFile userCSVFile;
	/**
	 * List of user from userCSVFile.
	 */
	private transient List<User> userCSVList;
	/**
	 * Defines how often the automatic cleaning is called in days.
	 */
	private int autoCleanDays;
	/**
	 * Status if the automatic database cleaning is either "started" or
	 * "stopped".
	 */
	private String autoCleanStatus;

	/**
	 * Number of hours left on the survey before the mandatory first reminder is
	 * sent.
	 */
	private int mailNotificationHours1;
	/**
	 * Bean representation of the database list.
	 */
	private transient List<User> userBeanList;
	/**
	 * Bean representation of the database list.
	 */
	private transient List<Groups> groupBeanList;
	/**
	 * Save new groupings from CSV file for update in the db.
	 */
	private transient List<Grouping> csvGroupingUpdateList;
	/**
	 * E-Mail to send notifactions from.
	 */
	private String mailSettingsAddress;
	/**
	 * SMTP Port for mailSettingsAddress.
	 */
	private String mailSettingsSMTP;
	/**
	 * SMTP Server for mailSettingsAddress.
	 */
	private String mailSettingsSMTPServer;
	/**
	 * Username for mailSettingsAddress.
	 */
	private String mailSettingsUser;
	/**
	 * Password for mailSettingsAddress.
	 */
	private String mailSettingsPassword;
	/**
	 * E-Mail for the admin account.
	 */
	private String adminAccountSettingsEMail;
	/**
	 * Password for the admin account.
	 */
	private String adminAccountSettingsPassword;
	/**
	 * Admin user.
	 */
	private User admin;

	/**
	 * Injection of the UserEJB for User related DB usage.
	 */
	@Inject
	private UserEJB userService;
	/**
	 * Injection of the GroupsEJB for Group related DB usage.
	 */
	@Inject
	private GroupsEJB groupsService;
	/**
	 * Injection of the MeetingEJB for Meeting related DB usage.
	 */
	@Inject
	private MeetingEJB meetingService;
	/**
	 * Injection of the MessageEJB for Facesmessages.
	 */
	@Inject
	private MessageEJB messageService;
	/**
	 * Injection of the DBAutoCleanEJB to automatically clean the database.
	 */
	@Inject
	private DBAutoCleanEJB autoCleanService;
	/**
	 * Injection of the GroupingEJB for Grouping related DB usage.
	 */
	@Inject
	private GroupingEJB groupingService;
	/**
	 * Injection of the SettingsEJB for Settings related DB usage.
	 */
	@Inject
	private SettingsEJB settingsService;
	/**
	 * Injection of MailEJB to send E-Mails.
	 */
	@Inject
	private transient MailEJB mailService;
	/**
	 * Inject of UrlEJB to generate URLs.
	 */
	@Inject
	private transient UrlEJB urlService;
	/**
	 * Logger.
	 */
	static final Logger LOGGER = Logger.getLogger(AdminBean.class.getName());

	/**
	 * General bean operation setup. Triggers loading of the necessary settings
	 * from the database.
	 */
	@PostConstruct
	public void init() {
		yesterday = new DateTime();
		yesterday = yesterday.minusDays(1);
		meetingsToRemove = new ArrayList<Meeting>();
		usersToRemove = new ArrayList<User>();
		initGroupList();
		initUserList();
		initSettings();
		initAdminAccountSettings();
		availableLanguages = new ArrayList<String>();
		availableLanguages.add("de");
		availableLanguages.add("en");
	}

	/**
	 * Fetches a List with all existing groups from the database.
	 * 
	 * @return List of all groups from the database, sorted by name
	 */
	public List<Groups> getGroupsList() {
		List<Groups> g = groupsService.getAll();
		Collections.sort(g, new GroupsNameComparator());
		return g;
	}

	/**
	 * Returns a List with all registered users from the database.
	 * 
	 * @return List of all registered users from the database, sorted by name
	 */
	public List<User> getUserList() {
		List<User> u = userService.getUsers(true);
		List<User> admins = new ArrayList<User>();
		for (int i = 0; i < u.size(); i++) {
			if ("admin".equals(u.get(i).getRole())) {
				admins.add(u.get(i));
			}
		}
		for (int i = 0; i < admins.size(); i++) {
			u.remove(admins.get(i));
		}
		Collections.sort(u, new UserNameComparator());
		return u;
	}

	/**
	 * Persists a new Group in the database with the data provided by the
	 * webpage/bean. Throws a Facesmessage if the groupname already exists.
	 */
	public void saveGroup() {
		boolean nameExists = false;
		for (int i = 0; i < groupBeanList.size(); i++) {
			if (groupName.equals(groupBeanList.get(i).getGroupsName())) {
				nameExists = true;
			}
		}

		if (!nameExists) {
			Groups group = new Groups();
			group.setGroupsAdmin(groupAdmin.getUserId());
			group.setGroupsName(groupName);
			group.setGroupsDescription(groupDescription);
			groupsService.saveGroup(group);
			groupBeanList.add(group);
			Collections.sort(groupBeanList, new GroupsNameComparator());
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminUserGroupAdded");
		} else {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
					"adminUserGroupNameExists");
		}
	}

	/**
	 * Removes the specified group from the database.
	 * 
	 * @param group
	 *            Group to be removed from the database
	 */
	public void removeGroup(Groups group) {
		groupsService.remove(group);
		initGroupList();
		initUserList();
	}

	/**
	 * Persists a new User in the Database. Throws a Facesmessage if the
	 * specified E-Mail is already registered.
	 */
	public void saveUser() {
		User user = new User();
		user.setUserName(userName);
		user.setPassword(HashTool.createSHA256Hash(userPassword));
		user.setEmail(userMail);
		user.setRegistered("yes");
		user.setRole("user");
		user.setLocale(userLang);
		boolean existsAlready = false;
		boolean mail = false;
		boolean name = false;

		for (int i = 0; i < userBeanList.size(); i++) {
			if (user.getEmail().equals(userBeanList.get(i).getEmail())) {
				existsAlready = true;
				mail = true;
			}
			if (user.getUserName().equals(userBeanList.get(i).getUserName())) {
				existsAlready = true;
				name = true;
			}
		}
		if (!existsAlready) {
			userService.saveUser(user);
			userBeanList.add(user);
			Collections.sort(userBeanList, new UserNameComparator());
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminUserAddSuccess");
		} else {
			if (mail) {
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"adminUserAddFail");
			}
			if (name) {
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"adminUserAddFail2");
			}
		}
	}

	/**
	 * Removes the specified user from the database. If the user is the creator
	 * of a running and confirmed meeting, then all participating user for that
	 * meeting will receive a cancellation mail.
	 * 
	 * @param user
	 *            User to be removed from the database
	 */
	public void removeUser(User user) {
		List<Groups> groups = groupsService.getAll();
		for (int i = 0; i < groups.size(); i++) {
			if (user.getUserId().equals(groups.get(i).getGroupsAdmin())) {
				groupsService.remove(groups.get(i));
			}
		}
		List<Meeting> meetings = meetingService.getAll();
		for (int i = 0; i < meetings.size(); i++) {
			if (user.getUserId().equals(meetings.get(i).getCreator())) {
				meetingService.removeMeeting(meetings.get(i));
				Date current = new Date();
				boolean running = false;
				boolean valid = false;
				TDate validDate = null;

				for (TDate date : meetings.get(i).getDates()) {
					if (date.getDateTime().after(current)) {
						running = true;
					}
					if ("valid".equals(date.getStatus())) {
						valid = true;
						validDate = date;
					}
				}

				if (running && valid) {
					String title = meetings.get(i).getTitle();
					String location = meetings.get(i).getLocation();
					if (location == null) {
						location = "";
					}
					String dateString;
					SimpleDateFormat dateFormat;
					if ("de".equals(user.getLocale())) {
						dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
					} else {
						dateFormat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
					}

					dateString = dateFormat.format(validDate.getDateTime());
					String sharelink = urlService.shareLinkBase()
							+ meetings.get(i).getUserLink();

					mailService.sendMail(user.getEmail(),
							"mailMeetingCancelledCreatorSubject",
							"mailMeetingCancelledCreator", new Object[] {
									sharelink, dateString, title, location });

					List<User> users = new ArrayList<User>();
					for (Datelist datelist : validDate.getDatelists()) {
						users.add(datelist.getUser());
					}

					mailService.sendMail(users,
							"mailMeetingCancelledUserSubject",
							"mailMeetingCancelledUser", new Object[] {
									sharelink, user.getUserName(), dateString,
									title, location });

				}

			}
		}
		userService.removeUser(user);
		initGroupList();
		initUserList();
	}

	/**
	 * Removes user from group and updates the database accordingly.
	 * 
	 * @param user
	 *            User to be removed from group
	 * @param group
	 *            Group the user will be removed from
	 */
	public void removeUserFromGroup(User user, Groups group) {
		List<Grouping> grouping = groupingService.getAll();
		for (int i = 0; i < grouping.size(); i++) {
			if (grouping.get(i).getUser().equals(user)
					&& grouping.get(i).getGroups().equals(group)) {
				groupingService.remove(grouping.get(i));
				initGroupList();
				initUserList();
				break;
			}
		}

	}

	public void removeGrouping(Grouping grouping) {
		groupingService.remove(grouping);
		initGroupList();
		initUserList();
	}

	/**
	 * Add existing users to a group. User and Group are provided by the
	 * bean/webpage.
	 */
	public void addUsersToGroups() {
		boolean globalUpdate = false;
		for (int i = 0; i < groupsForUsersAdding.size(); i++) {

			List<Grouping> grouping = groupingService.getAll();

			for (int j = 0; j < usersForGroupsAdding.size(); j++) {
				Grouping g = new Grouping(new GroupingId(groupsForUsersAdding
						.get(i).getGroupsId(), usersForGroupsAdding.get(j)
						.getUserId()), groupsForUsersAdding.get(i),
						usersForGroupsAdding.get(j), "yes");

				if (!grouping.contains(g)) {
					groupingService.saveGrouping(g);
					globalUpdate = true;
				}
			}

		}

		if (globalUpdate) {
			initUserList();
			initGroupList();
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminUserAddedToGroup");
		} else {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_WARN,
					"adminUserNothingAdded");
		}
	}

	/**
	 * Prepares a ordered List(ascending by group ID) representation of groups
	 * the specified user is a part of.
	 * 
	 * @param user
	 *            User to get the ordered List of participating groups from
	 * @return List of participating groups from user. The List is in ascending
	 *         order by group ID.
	 */
	public List<Groups> userGroupList(User user) {
		List<Groups> groups = new ArrayList<Groups>();
		for (Grouping g : user.getGroupings()) {
			groups.add(g.getGroups());
		}
		Collections.sort(groups, new GroupsComparator());
		return groups;
	}

	/**
	 * Prepares a ordered List(ascending by user name) representation of users
	 * that are in the specified group.
	 * 
	 * @param group
	 *            Group to get the ordered List of participating users from
	 * @return List of participating Users from group. The List is in ascending
	 *         order by user name.
	 */
	public List<User> groupUserList(Groups group) {
		List<User> users = new ArrayList<User>();
		for (Grouping g : group.getGroupings()) {
			users.add(g.getUser());
		}
		Collections.sort(users, new UserNameComparator());
		return users;
	}

	/**
	 * Searches the database for all meetings that only have dates that are
	 * older than the field yesterday. Populates the field meetingsToRemove with
	 * the resulting meetings.
	 * 
	 * @param autoCleaning
	 *            True logs the results. False Throws a Facesmessage for the
	 *            webpage instead.
	 */
	public void searchOldMeetings(boolean autoCleaning) {
		List<Meeting> meetings = meetingService.getAll();
		meetingsToRemove = new ArrayList<Meeting>();
		boolean afterYesterday = false;
		DateTime date;
		yesterday = new DateTime();

		yesterday = yesterday.minusDays(1);

		for (int i = 0; i < meetings.size(); i++) {
			afterYesterday = false;
			for (TDate t : meetings.get(i).getDates()) {
				date = new DateTime(t.getDateTime());
				if (date.isAfter(yesterday)) {
					afterYesterday = true;
				}
			}
			if (!afterYesterday) {
				meetingsToRemove.add(meetings.get(i));
			}
		}
		if (autoCleaning) {
			LOGGER.log(Level.INFO, meetingsToRemove.size()
					+ " old meetings found");
		} else {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminDBManualMeetingCleanSearchText",
					String.valueOf(meetingsToRemove.size()));
		}
	}

	/**
	 * Deletes all meetings from the field meetingsToRemove from the database.
	 * 
	 * @param autoCleaning
	 *            True logs the results. False Throws a Facesmessage for the
	 *            webpage instead.
	 */
	public void cleanOldMeetings(boolean autoCleaning) {
		for (int i = 0; i < meetingsToRemove.size(); i++) {
			meetingService.removeMeeting(meetingsToRemove.get(i));
		}
		if (autoCleaning) {
			LOGGER.log(Level.INFO, meetingsToRemove.size()
					+ " old meetings removed");
		} else {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminDBManualMeetingCleanDeleteText",
					String.valueOf(meetingsToRemove.size()));
		}

		meetingsToRemove = new ArrayList<Meeting>();
	}

	/**
	 * Searches the database for all unregistered users that aren't either
	 * creator or participant of a currently running meeting. Populates
	 * the field usersToRemove with the resulting users that are currently
	 * useless.
	 * 
	 * @param autoCleaning
	 *            True logs the results. False Throws a Facesmessage for the
	 *            webpage instead.
	 */
	public void searchOldUsers(boolean autoCleaning) {
		usersToRemove = new ArrayList<User>();
		List<User> users = userService.getUsers(false);
		List<Meeting> meetings = meetingService.getAll();
		boolean toDelete = false;

		for (int i = 0; i < users.size(); i++) {
			User user = users.get(i);
			toDelete = true;
			for (int j = 0; j < meetings.size(); j++) {
				Meeting meeting = meetings.get(j);
				if (user.getUserId().equals(meeting.getCreator())) {
					toDelete = false;
					break;
				}

				for (User u1 : meeting.getUsers()) {
					if (user.getUserId().equals(u1.getUserId())) {
						toDelete = false;
					}
					if (!toDelete) {
						break;
					}
				}
			}

			if (toDelete) {
				usersToRemove.add(users.get(i));
			}
		}

		if (autoCleaning) {
			LOGGER.log(Level.INFO, usersToRemove.size() + " users found");
		} else {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminDBManualUserCleanSearchText",
					String.valueOf(usersToRemove.size()));
		}
	}

	/**
	 * Deletes all users the field usersToRemove from the database.
	 * 
	 * @param autoCleaning
	 *            True logs the results. False Throws a Facesmessage for the
	 *            webpage instead.
	 */
	public void cleanOldUsers(boolean autoCleaning) {
		for (int i = 0; i < usersToRemove.size(); i++) {
			userService.removeUser(usersToRemove.get(i));
		}

		if (autoCleaning) {
			LOGGER.log(Level.INFO, usersToRemove.size() + " users deleted");
		} else {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminDBManualUserCleanDeleteText",
					String.valueOf(usersToRemove.size()));
		}

		usersToRemove = new ArrayList<User>();
	}

	/**
	 * Initialize the userList representation for the bean with data from the
	 * database.
	 */
	private void initUserList() {
		userBeanList = getUserList();
	}

	/**
	 * Initialize the userList representation for the bean with data from the
	 * database.
	 */
	private void initGroupList() {
		groupBeanList = getGroupsList();
	}

	/**
	 * Initialize the adminsettings from the database
	 */
	private void initSettings() {
		Settings s = settingsService.findByName("adminCleaningInterval");
		try {
			if (s != null) {
				autoCleanDays = Integer.parseInt(s.getSettingsDetails());
			}
		} catch (NumberFormatException ex) {
			autoCleanDays = 1;
		}

		s = settingsService.findByName("adminCleaningStatus");
		if (s != null) {
			autoCleanStatus = s.getSettingsDetails();
		} else {
			autoCleanStatus = "stopped";
		}
		if (autoCleanStatus.equals("started")) {
			autoCleanService.startMonitoring(autoCleanDays);
		}

		s = settingsService.findByName("adminMailAddress");
		if (s != null) {
			mailSettingsAddress = s.getSettingsDetails();
		} else {
			mailSettingsAddress = "";
		}

		s = settingsService.findByName("adminMailSMTP");
		if (s != null) {
			mailSettingsSMTP = s.getSettingsDetails();
		} else {
			mailSettingsSMTP = "";
		}

		s = settingsService.findByName("adminMailSMTPServer");
		if (s != null) {
			mailSettingsSMTPServer = s.getSettingsDetails();
		} else {
			mailSettingsSMTPServer = "";
		}

		s = settingsService.findByName("adminMailUser");
		if (s != null) {
			mailSettingsUser = s.getSettingsDetails();
		} else {
			mailSettingsUser = "";
		}

		s = settingsService.findByName("adminMailNotification1");
		if (s != null) {
			try {
				mailNotificationHours1 = Integer.parseInt(s
						.getSettingsDetails());
			} catch (NumberFormatException ex) {
				mailNotificationHours1 = 1;
			}
		}

	}

	/**
	 * Fetches the admin account settings from the database.
	 */
	private void initAdminAccountSettings() {
		List<User> users = userService.getUsers(true);
		admin = null;

		for (int i = 0; i < users.size(); i++) {
			if (users.get(i).getRole().equals("admin")) {
				admin = users.get(i);
				break;
			}
		}

		if (admin != null) {
			adminAccountSettingsEMail = admin.getEmail();
		}
	}

	/**
	 * Updates the changes to the mailsettings into the database
	 */
	public void saveMailSettings() {
		boolean settingsChanged = false;
		Settings s = settingsService.findByName("adminMailAddress");
		if (s != null) {
			if (!mailSettingsAddress.equals(s.getSettingsDetails())) {
				s.setSettingsDetails(mailSettingsAddress);
				settingsService.updatesettings(s);
				settingsChanged = true;
			}
		}

		s = settingsService.findByName("adminMailSMTP");
		if (s != null) {
			if (!mailSettingsSMTP.equals(s.getSettingsDetails())) {
				s.setSettingsDetails(mailSettingsSMTP);
				settingsService.updatesettings(s);
				settingsChanged = true;
			}
		}

		s = settingsService.findByName("adminMailSMTPServer");
		if (s != null) {
			if (!mailSettingsSMTPServer.equals(s.getSettingsDetails())) {
				s.setSettingsDetails(mailSettingsSMTPServer);
				settingsService.updatesettings(s);
				settingsChanged = true;
			}
		}

		s = settingsService.findByName("adminMailUser");
		if (s != null) {
			if (!mailSettingsUser.equals(s.getSettingsDetails())) {
				s.setSettingsDetails(mailSettingsUser);
				settingsService.updatesettings(s);
				settingsChanged = true;
			}
		}

		s = settingsService.findByName("adminMailPassword");
		if (s != null) {
			if (mailSettingsPassword != null) {
				if (!mailSettingsPassword.equals(s.getSettingsDetails())) {
					s.setSettingsDetails(mailSettingsPassword);
					settingsService.updatesettings(s);
					settingsChanged = true;
				}
			}
		}

		if (settingsChanged) {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminMailSettingsChanged");
		}
	}

	/**
	 * Updates the changes to the mail reminder settings into the database
	 */
	public void saveMailReminderSettings() {
		boolean settingsChanged = false;
		int convert;

		Settings s = settingsService.findByName("adminMailNotification1");
		if (s != null) {
			try {
				convert = Integer.parseInt(s.getSettingsDetails());
				if (mailNotificationHours1 != convert) {
					s.setSettingsDetails(String.valueOf(mailNotificationHours1));
					settingsService.updatesettings(s);
					settingsChanged = true;
				}
			} catch (NumberFormatException ex) {

			}
		}

		if (settingsChanged) {
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminMailSettingsChanged");
		}
	}

	/**
	 * Updates the changes to the admin account settings into the database.
	 */
	public void saveAccountSettings() {
		boolean update = false;

		if (admin != null) {
			if (!admin.getEmail().equals(adminAccountSettingsEMail)) {
				admin.setEmail(adminAccountSettingsEMail);
				update = true;
			}

			if (adminAccountSettingsPassword != null) {
				String hashedPW = HashTool
						.createSHA256Hash(adminAccountSettingsPassword);

				if (!hashedPW.equals(admin.getPassword())) {
					admin.setPassword(hashedPW);
					update = true;
				}
			}
		}

		if (update) {
			userService.updateUser(admin);
			messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
					"adminMailSettingsChanged");
		}
	}

	/**
	 * Populates the field userCSVList with new users to add to the database
	 * from the uploaded .csv file via userCSVFile. Directly persists the users
	 * in the database to prevent errors coming from changes in the database
	 * between generating and saving.
	 */
	public void generateUserCSVList() {
		if (userCSVFile != null) {
			boolean wrongFile = false;
			if (!userCSVFile.getFileName().endsWith(".csv")) {
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"adminCSVFileWrongFormatText");
				wrongFile = true;
			}
			if (userCSVFile.getSize() > FILE_SIZE) {
				messageService.sendFacesMessage(FacesMessage.SEVERITY_ERROR,
						"adminCSVFileWrongSizeText");
				wrongFile = true;
			}
			if (!wrongFile) {
				userCSVList = new ArrayList<User>();
				BufferedReader br = null;
				String line = "";
				String cvsSplitBy = ",";
				List<User> registeredUsers = userService.getUsers(true);

				boolean regex;
				boolean existsAlready;
				String[] user;
				User u;
				Pattern p = Pattern
						.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
								+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)"
								+ "*(\\.[A-Za-z]{2,})$");
				Matcher m;

				try {

					br = new BufferedReader(new InputStreamReader(
							userCSVFile.getInputstream()));
					while ((line = br.readLine()) != null) {

						// use comma as separator
						user = line.split(cvsSplitBy);
						u = new User();
						if (user.length >= 4) {
							if (user[0] != null) {
								u.setUserName(user[0]);
							}
							if (user[1] != null) {
								u.setPassword(HashTool
										.createSHA256Hash(user[1]));
							}
							if (user[2] != null) {
								u.setEmail(user[2]);
							}
							if (user[3] != null) {
								u.setLocale(user[3]);
							}
							u.setRegistered("yes");
							u.setRole("user");

							m = p.matcher(u.getEmail());
							regex = m.matches();
							existsAlready = false;

							for (int i = 0; i < registeredUsers.size(); i++) {
								if (u.getEmail().equals(
										registeredUsers.get(i).getEmail())) {
									existsAlready = true;
								}
							}

							for (int i = 0; i < userCSVList.size(); i++) {
								if (u.getEmail().equals(
										userCSVList.get(i).getEmail())) {
									existsAlready = true;
								}
							}

							if (regex && !existsAlready) {
								userService.saveUser(u);
								userCSVList.add(u);

								if (user.length > CSV_REQUIRED) {
									// add to groups
									csvGroupingUpdateList = new ArrayList<Grouping>();
									for (int i = CSV_REQUIRED; i < user.length; i++) {
										for (int j = 0; j < groupBeanList
												.size(); j++) {
											if (user[i].equals(groupBeanList
													.get(j).getGroupsName())) {
												Grouping g = new Grouping();
												g.setGroups(groupBeanList
														.get(j));
												csvGroupingUpdateList.add(g);
											}
										}
									}

									User fromDB = null;
									List<User> regUsers = userService
											.getUsers(true);
									for (int i = 0; i < regUsers.size(); i++) {
										if (u.getUserName().equals(
												regUsers.get(i).getUserName())) {
											fromDB = regUsers.get(i);
										}
									}
									if (fromDB != null) {
										for (int i = 0; i < csvGroupingUpdateList
												.size(); i++) {
											Grouping groupingToSave = csvGroupingUpdateList
													.get(i);
											groupingToSave
													.setId(new GroupingId(
															groupingToSave
																	.getGroups()
																	.getGroupsId(),
															fromDB.getUserId()));
											groupingToSave.setValidated("yes");
											groupingToSave.setUser(fromDB);
											groupingService
													.saveGrouping(groupingToSave);
										}
									}
								}
							}
						}
					}

				} catch (FileNotFoundException e) {
					LOGGER.log(Level.SEVERE, e.toString(), e);
				} catch (IOException e) {
					LOGGER.log(Level.SEVERE, e.toString(), e);
				} finally {
					if (br != null) {
						try {
							br.close();
						} catch (IOException e) {
							LOGGER.log(Level.SEVERE, e.toString(), e);
						}
					}
				}

				if (userCSVList.isEmpty()) {
					messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
							"adminCSVFailText");
				}

				initUserList();
				messageService.sendFacesMessage(FacesMessage.SEVERITY_INFO,
						"adminCSVSaveText", String.valueOf(userCSVList.size()));
			}
		}
		userCSVFile = null;

	}

	/**
	 * Listens to the value of autoCleanStatus and starts/stops the automatic
	 * database cleaning job accordingly.
	 */
	public void onAutoCleanStatusChange() {
		if (autoCleanStatus.equals("started")) {
			LOGGER.log(Level.INFO,
					"Automatic database cleaning started(cleaning every "
							+ autoCleanDays + " days)");
			autoCleanService.startMonitoring(autoCleanDays);
			Settings s = settingsService.findByName("adminCleaningStatus");
			if (s != null) {
				s.setSettingsDetails("started");
				settingsService.updatesettings(s);
			}
			s = settingsService.findByName("adminCleaningInterval");
			if (s != null) {
				s.setSettingsDetails(String.valueOf(autoCleanDays));
				settingsService.updatesettings(s);
			}
		} else {
			if (autoCleanStatus.equals("stopped")) {
				LOGGER.log(Level.INFO, "Automatic database cleaning stopped");
				autoCleanService.stopMonitoring();
				Settings s = settingsService.findByName("adminCleaningStatus");
				if (s != null) {
					s.setSettingsDetails("stopped");
					settingsService.updatesettings(s);
				}
			}
		}

	}

	/**
	 * Searches the userBeanList for the username to the specified id.
	 * 
	 * @param id
	 *            User id to get the name from
	 * @return Username for the id, "" if not in list
	 */
	public String getUserNameFromId(int id) {
		String name = "";

		for (int i = 0; i < userBeanList.size(); i++) {
			if (id == userBeanList.get(i).getUserId()) {
				name = userBeanList.get(i).getUserName();
			}
		}

		return name;
	}

	/**
	 * Getter method for groupName.
	 * 
	 * @return Name of the group to be added by the admin
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * Setter method for groupName.
	 * 
	 * @param groupName
	 *            Name of the group to be added by the admin
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * Getter method for groupAdmin.
	 * 
	 * @return User to be added by the admin as the owner of the group
	 */
	public User getGroupAdmin() {
		return groupAdmin;
	}

	/**
	 * Setter method for groupAdmin.
	 * 
	 * @param groupAdmin
	 *            User to be added by the admin as the owner of the group
	 */
	public void setGroupAdmin(User groupAdmin) {
		this.groupAdmin = groupAdmin;
	}

	/**
	 * Getter method for groupDesciption.
	 * 
	 * @return Description of the group to be added by the admin
	 */
	public String getGroupDescription() {
		return groupDescription;
	}

	/**
	 * Setter method for groupDesciption.
	 * 
	 * @param groupDescription
	 *            Description of the group to be added by the admin
	 */
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}

	/**
	 * Getter method for userName.
	 * 
	 * @return Name of the user to be added by the admin
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * Setter method for userName.
	 * 
	 * @param userName
	 *            Name of the user to be added by the admin
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Getter method for userMail.
	 * 
	 * @return Mail of the user to be added by the admin
	 */
	public String getUserMail() {
		return userMail;
	}

	/**
	 * Setter method for userMail.
	 * 
	 * @param userMail
	 *            Mail of the user to be added by the admin
	 */
	public void setUserMail(String userMail) {
		this.userMail = userMail;
	}

	/**
	 * Getter method for userPassword.
	 * 
	 * @return Password of the user to be added by the admin
	 */
	public String getUserPassword() {
		return userPassword;
	}

	/**
	 * Setter method for userPassword.
	 * 
	 * @param userPassword
	 *            Password of the user to be added by the admin
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	/**
	 * Getter method for usersForGroupsAdding.
	 * 
	 * @return users to be eventually added to groupsForUsersAdding
	 */
	public List<User> getUsersForGroupsAdding() {
		return usersForGroupsAdding;
	}

	/**
	 * Setter method for usersForGroupAdding.
	 * 
	 * @param usersForGroupsAdding
	 *            users to be eventually added to groupsForUsersAdding
	 */
	public void setUsersForGroupsAdding(List<User> usersForGroupsAdding) {
		this.usersForGroupsAdding = usersForGroupsAdding;
	}

	/**
	 * Getter method for groupsForUsersAdding.
	 * 
	 * @return Groups that are eventually added to usersForGroupsAdding
	 */
	public List<Groups> getGroupsForUsersAdding() {
		return groupsForUsersAdding;
	}

	/**
	 * Setter method for groupsForUsersAdding.
	 * 
	 * @param groupsForUsersAdding
	 *            Groups that are eventually added to usersForGroupsAdding
	 */
	public void setGroupsForUsersAdding(List<Groups> groupsForUsersAdding) {
		this.groupsForUsersAdding = groupsForUsersAdding;
	}

	/**
	 * Getter method for meetingsToRemove.
	 * 
	 * @return List of meetings that are candidates to be removed from the
	 *         database by the admin
	 */
	public List<Meeting> getMeetingsToRemove() {
		return meetingsToRemove;
	}

	/**
	 * Setter method for meetingsToRemove.
	 * 
	 * @param meetingstoRemove
	 *            List of meetings that are candidates to be removed from the
	 *            database by the admin
	 */
	public void setMeetingsToRemove(List<Meeting> meetingstoRemove) {
		this.meetingsToRemove = meetingstoRemove;
	}

	/**
	 * Getter method for yesterDay.
	 * 
	 * @return Time representation of the current systemdate minus one day
	 */
	public DateTime getYesterday() {
		return yesterday;
	}

	/**
	 * Setter method for yesterDay.
	 * 
	 * @param yesterday
	 *            Time representation of the current systemdate minus one day
	 */
	public void setYesterday(DateTime yesterday) {
		this.yesterday = yesterday;
	}

	/**
	 * Getter method for usersToRemove.
	 * 
	 * @return List of users that are candidates to be removed from the database
	 *         by the admin
	 */
	public List<User> getUsersToRemove() {
		return usersToRemove;
	}

	/**
	 * Setter method for usersToRemove.
	 * 
	 * @param usersToRemove
	 *            List of users that are candidates to be removed from the
	 *            database by the admin
	 */
	public void setUsersToRemove(List<User> usersToRemove) {
		this.usersToRemove = usersToRemove;
	}

	/**
	 * Getter method for userCSVFile.
	 * 
	 * @return .csv file uploaded by the user
	 */
	public UploadedFile getUserCSVFile() {
		return userCSVFile;
	}

	/**
	 * Setter method for userCSVFile.
	 * 
	 * @param userCSVFile
	 *            .csv file uploaded by the user
	 */
	public void setUserCSVFile(UploadedFile userCSVFile) {
		this.userCSVFile = userCSVFile;
	}

	/**
	 * Getter method for userCSVList.
	 * 
	 * @return List with new users generated via userCSVFile
	 */
	public List<User> getUserCSVList() {
		return userCSVList;
	}

	/**
	 * Setter method for userCSVList.
	 * 
	 * @param userCSVList
	 *            List with new users generated via userCSVFile
	 */
	public void setUserCSVList(List<User> userCSVList) {
		this.userCSVList = userCSVList;
	}

	/**
	 * Getter method for autoCleanDays.
	 * 
	 * @return How often the database is cleaned in days
	 */
	public int getAutoCleanDays() {
		return autoCleanDays;
	}

	/**
	 * Setter method for autoCleanDays.
	 * 
	 * @param autoCleanDays
	 *            How often the database is cleaned in days
	 */
	public void setAutoCleanDays(int autoCleanDays) {
		this.autoCleanDays = autoCleanDays;
	}

	/**
	 * Getter method for autoCleanStatus.
	 * 
	 * @return Status of the automatic database cleaning job. Accepted Values
	 *         are "started" and "stopped"
	 */
	public String getAutoCleanStatus() {
		return autoCleanStatus;
	}

	/**
	 * Setter method for autoCleanStatus.
	 * 
	 * @param autoCleanStatus
	 *            Status of the automatic database cleaning job. Accepted Values
	 *            are "started" and "stopped"
	 */
	public void setAutoCleanStatus(String autoCleanStatus) {
		this.autoCleanStatus = autoCleanStatus;
	}

	/**
	 * Getter method for mailNotificationHours1.
	 * 
	 * @return Number of hours left on the survey before the mandatory first
	 *         reminder is sent
	 */
	public int getMailNotificationHours1() {
		return mailNotificationHours1;
	}

	/**
	 * Setter method for mailNotificationHours1.
	 * 
	 * @param mailNotificationHours1
	 *            Number of hours left on the survey before the mandatory first
	 *            reminder is sent
	 */
	public void setMailNotificationHours1(int mailNotificationHours1) {
		this.mailNotificationHours1 = mailNotificationHours1;
	}

	/**
	 * Getter method for userBeanList.
	 * 
	 * @return List representation of users in the database
	 */
	public List<User> getUserBeanList() {
		return userBeanList;
	}

	/**
	 * Setter method for userBeanList.
	 * 
	 * @param userBeanList
	 *            List representation of users in the database
	 */
	public void setUserBeanList(List<User> userBeanList) {
		this.userBeanList = userBeanList;
	}

	/**
	 * Getter method for groupBeanList.
	 * 
	 * @return List representation of groups in the database
	 */
	public List<Groups> getGroupBeanList() {
		return groupBeanList;
	}

	/**
	 * Setter method for groupBeanList.
	 * 
	 * @param groupBeanList
	 *            List representation of groups in the database
	 */
	public void setGroupBeanList(List<Groups> groupBeanList) {
		this.groupBeanList = groupBeanList;
	}

	/**
	 * Getter method for mailSettingsAddress.
	 * 
	 * @return E-Mail to send notifactions from
	 */
	public String getMailSettingsAddress() {
		return mailSettingsAddress;
	}

	/**
	 * Setter method for mailSettingsAddress.
	 * 
	 * @param mailSettingsAddress
	 *            E-Mail to send notifactions from
	 */
	public void setMailSettingsAddress(String mailSettingsAddress) {
		this.mailSettingsAddress = mailSettingsAddress;
	}

	/**
	 * Getter method for mailSettingsSMTP.
	 * 
	 * @return SMTP Port for mailSettingsAddress
	 */
	public String getMailSettingsSMTP() {
		return mailSettingsSMTP;
	}

	/**
	 * Setter method for mailSettingsSMTP.
	 * 
	 * @param mailSettingsSMTP
	 *            SMTP Port for mailSettingsAddress
	 */
	public void setMailSettingsSMTP(String mailSettingsSMTP) {
		this.mailSettingsSMTP = mailSettingsSMTP;
	}

	/**
	 * Getter method for mailSettingsSMTPServer.
	 * 
	 * @return SMTP Server for mailSettingsAddress
	 */
	public String getMailSettingsSMTPServer() {
		return mailSettingsSMTPServer;
	}

	/**
	 * Setter method for mailSettingsSMTPServer.
	 * 
	 * @param mailSettingsSMTPServer
	 *            SMTP Server for mailSettingsAddress.
	 */
	public void setMailSettingsSMTPServer(String mailSettingsSMTPServer) {
		this.mailSettingsSMTPServer = mailSettingsSMTPServer;
	}

	/**
	 * Getter method for mailSettingsUser.
	 * 
	 * @return Username for mailSettingsAddress
	 */
	public String getMailSettingsUser() {
		return mailSettingsUser;
	}

	/**
	 * Setter method for mailSettingsUser.
	 * 
	 * @param mailSettingsUser
	 *            Username for mailSettingsAddress
	 */
	public void setMailSettingsUser(String mailSettingsUser) {
		this.mailSettingsUser = mailSettingsUser;
	}

	/**
	 * Getter method for mailSettingsPassword.
	 * 
	 * @return Password for mailSettingsAddress
	 */
	public String getMailSettingsPassword() {
		return mailSettingsPassword;
	}

	/**
	 * Setter method for mailSettingsPassword.
	 * 
	 * @param mailSettingsPassword
	 *            Password for mailSettingsAddress
	 */
	public void setMailSettingsPassword(String mailSettingsPassword) {
		this.mailSettingsPassword = mailSettingsPassword;
	}

	/**
	 * Getter method for adminAccountSettingsEMail.
	 * 
	 * @return E-Mail for the admin account
	 */
	public String getAdminAccountSettingsEMail() {
		return adminAccountSettingsEMail;
	}

	/**
	 * Setter method for adminAccountSettingsEMail.
	 * 
	 * @param adminAccountSettingsEMail
	 *            E-Mail for the admin account.
	 */
	public void setAdminAccountSettingsEMail(String adminAccountSettingsEMail) {
		this.adminAccountSettingsEMail = adminAccountSettingsEMail;
	}

	/**
	 * Getter method for adminAccountSettingsPassword.
	 * 
	 * @return Password for the admin account.
	 */
	public String getAdminAccountSettingsPassword() {
		return adminAccountSettingsPassword;
	}

	/**
	 * Setter method for adminAccountSettingsPassword.
	 * 
	 * @param adminAccountSettingsPassword
	 *            Password for the admin account.
	 */
	public void setAdminAccountSettingsPassword(
			String adminAccountSettingsPassword) {
		this.adminAccountSettingsPassword = adminAccountSettingsPassword;
	}

	/**
	 * Getter method for userLang.
	 * 
	 * @return Language of the user to be added by the admin.
	 */
	public String getUserLang() {
		return userLang;
	}

	/**
	 * Setter method for userLang.
	 * 
	 * @param userLang
	 *            Language of the user to be added by the admin.
	 */
	public void setUserLang(String userLang) {
		this.userLang = userLang;
	}

	/**
	 * Getter method for availableLanguages.
	 * 
	 * @return Available language for translating the mails.
	 */
	public List<String> getAvailableLanguages() {
		return availableLanguages;
	}

	/**
	 * Setter method for availableLanguages.
	 * 
	 * @param availableLanguages
	 *            Available language for translating the mails.
	 */
	public void setAvailableLanguages(List<String> availableLanguages) {
		this.availableLanguages = availableLanguages;
	}

}

package login;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import bean.LoginBean;
import ejb.UserEJB;
import entity.User;

/**
 * Checks if the User is logged in and fills the LoginBean with Data if thats
 * the case.
 * 
 * @author OK
 *
 */
@WebFilter("/*")
public class UserFilter implements Filter {

	/**
     * Injection of the UserEJB for User related DB usage.
     */
	@Inject
	private UserEJB userService;

	/**
     * Injection of LoginBean
     */
	@Inject
	private LoginBean loginBean;

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
	 * javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		String remoteUser = request.getRemoteUser();

		if (remoteUser != null) {
			HttpSession session = request.getSession();

			if (session.getAttribute("user") == null) {
				User user = userService.findByEMail(remoteUser);
				session.setAttribute("user", user);
				loginBean.setUser(user);
				loginBean.setLoggedIn(true);
			}
		}

		chain.doFilter(req, res);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}
}
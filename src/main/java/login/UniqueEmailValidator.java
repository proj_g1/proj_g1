package login;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import ejb.UserEJB;


/**
 * Checks if a given email is already being used in the database.
 *
 * @author AH
 */

@ManagedBean
@RequestScoped
public class UniqueEmailValidator implements Validator {

	@EJB
	private UserEJB service;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		String email = value.toString();
		
		if (email == null || email.isEmpty()) 
			return; //let required="true" or @NotNull handle it
		
		if(service.doesEmailAlreadyExist(email))
			throw new ValidatorException(new FacesMessage("Email already in use, choose another"));
	}
}
package entity;

// Generated 08.07.2015 11:30:28 by Hibernate Tools 4.3.1

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Waitlist generated by hbm2java
 */
@Entity
@Table(name = "Waitlist", catalog = "jboss")
public class Waitlist implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private WaitlistId id;
	private TDate date;
	private User user;
	private java.util.Date jointime;

	public Waitlist() {
	}

	public Waitlist(WaitlistId id, TDate date, User user, java.util.Date jointime) {
		this.id = id;
		this.date = date;
		this.user = user;
		this.jointime = jointime;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "dateId", column = @Column(name = "date_id", nullable = false)),
			@AttributeOverride(name = "userId", column = @Column(name = "user_id", nullable = false)) })
	public WaitlistId getId() {
		return this.id;
	}

	public void setId(WaitlistId id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "date_id", nullable = false, insertable = false, updatable = false)
	public TDate getDate() {
		return this.date;
	}

	public void setDate(TDate date) {
		this.date = date;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "user_id", nullable = false, insertable = false, updatable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "jointime", nullable = false, length = 19)
	public java.util.Date getJointime() {
		return this.jointime;
	}

	public void setJointime(java.util.Date jointime) {
		this.jointime = jointime;
	}

}

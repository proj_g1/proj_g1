package util;

import java.util.Comparator;

import entity.TDate;

/**
 * Implements Comparator<TDate> to compare two TDate objects in respect to their
 * java.util.Date element.
 * Returns -1 if date1 is chronologically before date2
 * Returns 1 if date1 is chronologically after date2
 * and 0 if both are chronologically identical
 * 
 * @author Erik
 *
 */
public class TDateComparator implements Comparator<TDate> {
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(TDate date1, TDate date2) {
		if (date1.getDateTime().before(date2.getDateTime())) {
			return -1;
		} else if (date1.getDateTime().after(date2.getDateTime())) {
			return 1;
		}
		return 0;
	}
}
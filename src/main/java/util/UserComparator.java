package util;

import java.util.Comparator;

import entity.User;

/**
 * This class can be used to sort users by username
 * 
 * @author EW //
 */
public class UserComparator implements Comparator<User> {
	@Override
	public int compare(User user1, User user2) {
		if (user1.getUserName() == null)
			return 1;
		if (user2.getUserName() == null)
			return -1;
		return user1.getUserName().compareTo(user2.getUserName());
	}
}
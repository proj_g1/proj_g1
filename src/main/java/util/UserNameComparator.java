package util;

import java.util.Comparator;

import entity.User;

/**
 * Implements Comparator<User> to compare two User objects in respect to their
 * Database Name
 * Returns -1 if user1 name is lexically lesser than user2 name
 * Returns 1 if user1 name is lexically greater than user2 name
 * and 0 if both are the same.
 * 
 * @author OK
 *
 */
public class UserNameComparator implements Comparator<User> {
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(User user1, User user2) {
		return user1.getUserName().compareToIgnoreCase(user2.getUserName());
	}
}
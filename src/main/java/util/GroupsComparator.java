package util;

import java.util.Comparator;

import entity.Groups;

/**
 * Implements Comparator<Groups> to compare two Groups objects in respect to
 * their Database ID Returns -1 if group1 ID is lesser than group2 ID Returns 1
 * if group1 ID is greater than group2 ID and 0 if both are the same.
 * 
 * @author Oliver
 *
 */
public class GroupsComparator implements Comparator<Groups> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Groups group1, Groups group2) {
		if (group1.getGroupsId() < group2.getGroupsId()) {
			return -1;
		} else if (group1.getGroupsId() > group2.getGroupsId()) {
			return 1;
		}
		return 0;
	}
}
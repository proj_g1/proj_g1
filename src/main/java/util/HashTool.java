package util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Utility Class to Create HashStrings from specified String.
 * 
 * 
 * @author AH
 *
 */
public class HashTool {
	/**
	 * Logger.
	 */
	static final Logger LOGGER = Logger.getLogger(HashTool.class.getName());

	
	/**
	 * Creates a hex based SHA-256 string from the specified String toHash.
	 * 
	 * @param toHash
	 *            String to be encoded in SHA-256
	 * @return SHA-256 encoded hash String or empty String if something goes
	 *         horribly wrong
	 */
	public static String createSHA256Hash(String toHash) {
		String tmpPW = "";
		StringBuffer sb = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(toHash.getBytes("UTF-8"));
			byte[] passSHA256 = md.digest();
			sb = new StringBuffer();
			for (int i = 0; i < passSHA256.length; i++) {
				sb.append(Integer.toString((passSHA256[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			tmpPW = sb.toString();
			return tmpPW;
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			LOGGER.log(Level.SEVERE, e.toString(), e);
		}

		return tmpPW;
	}
}

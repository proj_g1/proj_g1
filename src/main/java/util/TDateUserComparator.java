package util;

import java.util.Comparator;

import entity.Datelist;
import entity.TDate;

/**
 * This comparator compares TDates by the number of userrs that have acceptet
 * the date
 * 
 * @author EW
 *
 */
public class TDateUserComparator implements Comparator<TDate> {
	@Override
	public int compare(TDate date1, TDate date2) {
		int participants1 = 0, participants2 = 0;
		for (Datelist datelist : date1.getDatelists()) {
			if (datelist.getWaiting() != null
					&& !datelist.getWaiting().equalsIgnoreCase("yes"))
				participants1++;
		}
		for (Datelist datelist : date2.getDatelists()) {
			if (datelist.getWaiting() != null
					&& !datelist.getWaiting().equalsIgnoreCase("yes"))
				participants2++;
		}

		if (participants1 < participants2) {
			return 1;
		} else if (participants1 > participants2) {
			return -1;
		}
		return 0;
	}
}

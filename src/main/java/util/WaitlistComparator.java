package util;

import java.util.Comparator;

import entity.Waitlist;

/**
 * This class can be used so sort Waitlists by jointime
 * 
 * @author EW
 *
 */
public class WaitlistComparator implements Comparator<Waitlist> {
	@Override
	public int compare(Waitlist o1, Waitlist o2) {
		if (o1.getJointime().before(o2.getJointime()))
			return -1;
		if (o1.getJointime().after(o2.getJointime()))
			return 1;
		return 0;
	}
}

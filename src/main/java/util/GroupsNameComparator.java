package util;

import java.util.Comparator;

import entity.Groups;

/**
 * Implements Comparator<Groups> to compare two Groups objects in respect to
 * their Database Name Returns -1 if group1 name is lexically lesser than group2
 * name Returns 1 if group1 name is lexically greater than group2 name and 0 if
 * both are the same.
 * 
 * @author OK
 *
 */
public class GroupsNameComparator implements Comparator<Groups> {
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Groups group1, Groups group2) {
		return group1.getGroupsName().compareToIgnoreCase(
				group2.getGroupsName());
	}
}
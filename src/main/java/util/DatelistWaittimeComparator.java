package util;

import java.util.Comparator;

import entity.Datelist;
/**
 * This class can be used so sort Waitlists by Waittime
 * 
 * @author EW
 *
 */

public class DatelistWaittimeComparator implements Comparator<Datelist> {

	@Override
	public int compare(Datelist datelist1, Datelist datelist2) {
		if (datelist1.getWaittime().before(datelist2.getWaittime()))
			return -1;
		if (datelist1.getWaittime().after(datelist2.getWaittime()))
			return 1;
		return 0;
	}

}
